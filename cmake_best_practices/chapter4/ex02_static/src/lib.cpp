// Greeter implementation

#include <chapter4/ex02/lib.hpp>
#include <iostream>

namespace chapter4
{

namespace ex02
{

void greeter::greet() { std::cout << "Hello, world!" << std::endl; }

} // namespace ex02


} // namespace chapter4
