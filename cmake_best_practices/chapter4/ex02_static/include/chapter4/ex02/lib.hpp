// Greate interface declaration

#pragma once

namespace chapter4
{

namespace ex02
{

/**
 * @brief The 'greeter' class interface
 */

class greeter
{
public:
    /**
     * @brief Greet the caller
     */
    void greet();
};

} // namespace ex02

} // namespace chapter4
