# 4. 打包、部署和安装

## 4.1. 使目标可安装

项目中支持部署的方法是可安装，这里的 ”可安装” 并不是指安装预先制作好的软件包，而是用户必须获取项目的源码，并从头开始构建。可安装的项目需要额外的构建系统，用于在系统上安装运行时或开发构件。构建系统将在这里执行安装操作，因为这里使用 CMake 来生成构建系统文件，所以 CMake 必须生成相关的安装代码。

### 4.1.1 `install()` 指令

`install(…)` 指令是一个内置的 CMake 命令，允许生成用于安装目标、文件、目录等的构建系统说明。 CMake 不会生成安装指令，除非明确地说明。因此，安装总是在开发者的控制中。

**安装 CMake 目标**

要使 CMake 目标可安装，必须用至少一个参数指定 `TARGETS` 参数。指令的签名如下所示:

```CMake
install(TARGETS <target>... [...])
```

`TARGETS` 参数表示 install 可以接受一组 CMake 目标，以便为其生成安装代码，只安装目标的输出构件。最常见的目标输出工件定义如下:

- ARCHIVE (静态库、 DLL 导入库和链接器导入文件):
  - 除了在 macOS 中标记为 `FRAMEWORK` 的目标
- LIBRARY (动态库):
  - 除了在 macOS 中标记为 `FRAMEWORK` 的目标
  - 除了 dll (Windows)
- RUNTIME (可执行文件和 dll):
  - 除了在 macOS 中标记为 MACOSX_BUNDLE 的目标

使目标可安装之后， CMake 将生成必要的安装代码，以安装输出的构件，这里先将可执行目标安装在一起。

- *[./ex01_exectuable/CMakeLists.txt](https://gitee.com/fyang0906/cmake-tutorial/blob/master/CMake-Best-Practices/chapter4/ex01_exectuable/CMakeLists.txt)*

```CMake
add_executable(ch4_ex01_executable)

target_sources(ch4_ex01_executable PRIVATE src/main.cpp)

target_compile_features(ch4_ex01_executable PRIVATE cxx_std_11)

install(TARGETS ch4_ex01_executable)
```

上述示例代码，定义了一个名为 `ch4_ex01_executable` 的可执行目标，并在随后的两行中填充了它的属性。最后一行 `install(…)` 是我们感兴趣的，是 `ch4_ex01_executable` 所需的安装代码。要检查 `ch4_ex01_executable` 是否可以安装，可以构建项目并通过 CLI 进行安装:

```shell
$ cmake -S . -B build
...
$ cmake --build build/
...
$ cmake --install ./build/ --prefix /tmp/install-test
-- Install configuration: ""
-- Installing: /tmp/install-test/bin/ch4_ex01_executable
```

```shell
$ ls -lRah /tmp/install-test
/tmp/install-test:
total 12K
drwxr-xr-x  3 fyang fyang 4.0K Mar 26 16:15 .
drwxrwxrwt 11 root  root  4.0K Mar 26 16:15 ..
drwxr-xr-x  2 fyang fyang 4.0K Mar 26 16:15 bin

/tmp/install-test/bin:
total 28K
drwxr-xr-x 2 fyang fyang 4.0K Mar 26 16:15 .
drwxr-xr-x 3 fyang fyang 4.0K Mar 26 16:15 ..
-rwxr-xr-x 1 fyang fyang  17K Mar 26 16:11 ch4_ex01_executable
```

> **Tips:**
>
> 不必为 `cmake --install` 指定 `--prefix` 参数，也可以使用 `CMAKE_INSTALL_PREFIX` 来修改非默认的安装路径。

从上面的输出，可以看到 `ch4_ex01_executable` 目标的输出构件 -- 安装了 `ch4_ex01_executable` 二进制文件。由于 `ch4_ex01_executable` 目标拥有的唯一输出工件，所以该目标确实已经可安装。

注意 `ch4_ex01_executable` 没有直接安装在 `/tmp/install-test` 目录中， `install` 将它放在 `bin` 子目录中，这是因为 CMake 知道什么样的工件应该放在哪里。传统的 UNIX 系统中，二进制文件存放在 `/usr/bin` 中，而库文件存放在 `/usr/lib` 中， CMake 知道 `add_executable()` 会生成一个可执行的二进制文件，所以将其放入 `bin` 子目录中。这些目录的默认值由 CMake 提供，具体取决于目标类型。提供默认安装路径信息的 CMake 模块称为 `GNUInstallDirs` 模块。 `GNUInstallDirs` 模块定义了各种 `CMAKE_INSTALL_` 的路径。默认安装目录如下表所示:

| 目标类型 | GNUInstallDirs 变量 | 默认位置 |
| ------ |  ------------------ | ------- |
| RUNTIME | ${CMAKE_INSTALL_BINDIR} | bin |
| LIBRARY | ${CMAKE_INSTALL_LIBDIR} | lib |
| ARCHIVE | ${CMAKE_INSTALL_LIBDIR} | lib |
| PRIVATE_HEADER | ${CMAKE_INSTALL_INCLUDEDIR} | include |
| PUBLIC_HEADER  | ${CMAKE_INSTALL_INCLUDEDIR} | include |

为了覆盖内置默认值，在 `install(…)` 指令中需要使用 `<TARGET_TYPE> DESTINATION` 参数。这里，我们尝试将默认的运行时安装目录改为 `qbin`，而不是 `bin`。只需要对 `install(…)` 指令做一个小修改:

```CMake
install(TARGETS ch4_ex01_executable
    RUNTIME DESTINATION qbin
)
```

进行修改后，可以重新运行配置、构建和安装命令。可以通过 `cmake --install` 检查输出来确认运行时目标已经更改。与第一次不同，可以观察到 `ch4_ex01_executable` 二进制文件放入了 `qbin`，而不是默认的 (`bin`) 目录中:

```shell
$ cmake --install ./build/ --prefix /tmp/install-test
-- Install configuration: ""
-- Installing: /tmp/install-test/qbin/ch4_ex01_executable
```

**安装 STATIC 库**

- *[./ex02_static/CMakeLists.txt](https://gitee.com/fyang0906/cmake-tutorial/blob/master/CMake-Best-Practices/chapter4/ex02_static/CMakeLists.txt)*

```CMake
add_library(ch4_ex02_static STATIC)

target_sources(ch4_ex02_static PRIVATE src/lib.cpp)

target_include_directories(ch4_ex02_static PUBLIC include)

target_compile_features(ch4_ex02_static PRIVATE cxx_std_11)
include(GNUInstallDirs)

install(TARGETS ch4_ex02_static)

install (
    DIRECTORY include/
    DESTINATION "${CMAKE_INSTALL_INCLUDEDIR}"
)
```

`install(...)` 指令中多了一个 `DIRECTORY` 参数，这是使静态库的头文件可安装。这样做的原因是 CMake 不会安装任何非输出工件，而 `STATIC` 库目标只生成一个二进制文件作为输出工件。头文件不是输出工件，应该单独安装。

> **Tips:**
>
> `DIRECTORY` 参数中末尾的斜杠会导致 CMake 复制文件夹的内容，而不是按名称复制文件夹。 CMake 处理尾随斜杠的方式与 Linux rsync 命令相同。

### 4.1.2 安装文件和目录

安装的东西并不总是目标输出构件的一部分。它们可能是目标的运行时依赖项，例如图片、源文件、脚本和配置文件。 CMake 提供了 `install(FILES…)` 和 `install(DIRECTORY…)` 指令来安装任何特定的文件或目录。

**安装文件**

`install(FILES…)` 指令接受一个或多个文件作为参数，还需要 `TYPE` 或 `DESTINATION` 参数，这两个参数用于确定指定文件的目标目录。 `TYPE` 用于指示哪些文件将使用该文件类型的默认路径作为安装目录，可以通过设置相关的 `GNUInstallDirs` 变量来重写默认值。下表显示了有效的 `TYPE` 值，以及它们的目录映射:

| 类型 | GUNInstallDirs变量 | 默认值 |
|   -          | - | - |
| BIN          | ${CMAKE_INSTALL_BINDIR}        | bin |
| LIN          | ${CMAKE_INSTALL_SBINDIR}       | sbin |
| LIN          | ${CMAKE_INSTALL_LIBDIR}        | lib |
| INCLUDE      | ${CMAKE_INSTALL_INCLUDEDIR}    | include |
| SYSCONF      | ${CMAKE_INSTALL_SYSCONFDIR}    | etc |
| SHAREDDSTATE | ${CMAKE_INSTALL_DHARESTATEDIR} | com |
| LOCALSTATE   | ${CMAKE_INSTALL_LOCALSTATEDIR} | var |
| RUNSTATE     | ${CMAKE_INSTALL_RUNSTATEDIR}   | \<LOCALSTATE dir>/run |
| DATA         | ${CMAKE_INSTALL_DATADIR}       | \<DATAROOT dir> |
| INFO         | ${CMAKE_INSTALL_INFODIR}       | \<DATAROOT dir>/info |
| LOCALE       | ${CMAKE_INSTALL_LOCALEDIR}     | \<DATAROOT dir>/locale |
| MAN          | ${CMAKE_INSTALL_MANDIR}        | \<DATAROOT dir>/man |
| DOC          | ${CMAKE_INSTALL_DOCDIR}        | \<DATAROOT dir>/doc |

若不想使用 `TYPE`，可以使用 `DESTINATION`。可以通过 `install(…)` 为指定文件提供自定义目标。

另一种形式的 `install(FILES...)` 是 `install(PROGRAMS...)`，这与 `install(FILES...)` 相同，但需要为安装文件设置 `OWNER_EXECUTE`，`GROUP_EXECUTE` 和
`WORLD_EXECUTE` 权限。这对最终用户执行的二进制文件或脚本文件是有意义的。

为了理解 `install(FILES|PROGRAMS…)`，来看一个示例，例 (chapter_4/ex03_file)。其包含三个文件: `chapter4_greeter_content`、 `chapter4_greeter.py` 和 `CMakeLists.txt`。首先，看看 `CMakeLists.txt:`

```CMake
install(FILES "${CMAKE_CURRENT_LIST_DIR}/chapter4_greeter_content" DESTINATION "${CMAKE_INSTALL_BINDIR}")
install(PROGRAMS "${CMAKE_CURRENT_LIST_DIR}/chapter4_greeter.py" DESTINATION "${CMAKE_INSTALL_BINDIR}" RENAME chapter4_greeter)
```
第一个 `install(…)` 中， CMake 在当前的 CMakeLists.txt(chapter4/ex03_file) 中安装了 `chapter4_greeter_content` 文件，在系统默认 `BIN` 目录下。第二个 `install(…)` 中， CMake 在默认 `BIN` 目录中安装 `chapter4_greeter.py`，名称为 `chapter4_greeter`。

> **Tips:** `RENAME` 参数只对单文件的 `install(…)` 有效。

有了这些 `install(…)` 指令，CMake 应该在 `${CMAKE_INSTALL_PREFIX}/bin` 目录下安装 `chapter4_greeter.py` 和 `chapter4_greeter_content`。通过 CLI 来构建和安装这个项目:

```CMake
$ cmake -S . -B build
...
$ cmake --build build/
...
$ cmake --install build/ --prefix /tmp/installtest/
-- Install configuration: ""
-- Installing: /tmp/installtest/bin/chapter4_greeter_content
-- Installing: /tmp/installtest/bin/chapter4_greeter
```

上面的输出确认了 CMake 为 `chapter4_greeter_content` 和 `chapter4_greeting.py` 文件生成了所需的安装代码。最后，因为使用了 `PROGRAMS` 参数来安装 `chapter4_greeter`，检查一下是否可以执行:

```shell
$ python3 /tmp/installtest/bin/chapter4_greeter
['Hello from installed file!']
```

**安装目录**

`install(DIRECTORY…)` 指令用于安装目录，目录的结构将按原样复制到目标。目录既可以作为一个整体安装，也可以有选择地安装。先从最基本的目录安装示例开始:

```CMake
install(DIRECTORY dir1 dir2 dir3 TYPE LOCALSTATE)
```

前面的示例将把 `dir1` 和 `dir2` 目录安装到 `${CMAKE_INSTALL_PREFIX}/var` 目录中，以及所有子文件夹和文件。有时，不能直接安装文件夹的全部内容。幸运的是， CMake 允许安装命令根据通配符模式和正则表达式包含或排除目录内容。这次我们有选择地安装 `dir1`、 `dir2` 和 `dir3`:

```CMake
include(GNUInstallDirs) # Defines the ${CMAKE_INSTALL_INCLUDEDIR} variable

install(DIRECTORY dir1 DESTINATION ${CMAKE_INSTALL_LOCALSTATEDIR} FILES_MATCHING PATTERN "*.x")
install(DIRECTORY dir2 DESTINATION ${CMAKE_INSTALL_LOCALSTATEDIR} FILES_MATCHING PATTERN "*.hpp" EXCLUDE PATTERN "*")
install(DIRECTORY dir3 DESTINATION ${CMAKE_INSTALL_LOCALSTATEDIR} PATTERN "bin" EXCLUDE)
```

前面的例子中，使用 `FILES_MATCHING` 参数来定义文件选择的条件。 `FILES_MATCHING` 后面可以跟 `PATTERN` 或 `REGEX` 参数。 `PATTERN` 可以定义通配符模式，而 `REGEX` 可以定义正则表达式。通常，这些表达式用于包含文件。若希望排除匹配条件的文件，可以将 `EXCLUDE` 参数附加到模式中。由于 `FILES_MATCHING` 参数，这些过滤器没有应用于子目录名。我们还在最后一个 `install(…)` 指令中使用了 `PATTERN`，并且没有添加 `FILES_MATCHING 前缀`，这可以过滤子目录而不是文件。这将只安装 `dir1` 中扩展名为 `.x` 的文件， `dir2` 中没有扩展名为 `.hpp` 的文件，以及 `dir3` 中除 `bin` 文件夹外的所有内容。

```shell
$ cmake -S . -B build
...
$ cmake --build build/
$ cmake --install ./build/ --prefix /tmp/installtest/
-- Install configuration: ""
-- Installing: /tmp/installtest/var/dir1
-- Installing: /tmp/installtest/var/dir1/asset1.x
-- Installing: /tmp/installtest/var/dir1/subdir
-- Installing: /tmp/installtest/var/dir1/subdir/asset5.x
-- Installing: /tmp/installtest/var/dir2
-- Installing: /tmp/installtest/var/dir2/chapter4_hello.dat
-- Installing: /tmp/installtest/var/dir3
-- Installing: /tmp/installtest/var/dir3/asset4
```

> **Tips:** FILES_MATCHING 不能在 PATTERN 或 REGEX 之后使用，反之亦然。

输出中，可以看到只有扩展名为 `.x` 的文件是从 `dir1` 中选取的，因为在第一个 `install(…)` 中使用 `FILES_MATCHING PATTERN *.x`，导致 `asset2` 文件没有被安装。另外， `dir2/chapter4_hello.dat` 文件已经安装，并且跳过 `dir2/chapter4_hello.hpp` 文件，这是因为在第二个 `install(…)` 中指定了 `FILES_MATCHING PATTERN *.hpp EXCLUDE PATTERN *`。最后，安装了 `dir3/asset4` 文件，并且完全跳过了 `dir3/bin` 目录，因为在最后一个 `install(…)` 中指定了 `PATTERN "bin" EXCLUDE` 参数。
通过 `install(DIRECTORY…)`，了解了 `install(…)` 的基本知识。接下来，继续看看 `install(…)` 的其他常见参数。

**install() 的其他常见参数**

`install()` 的第一个参数指示要安装什么，还有一些参数允许进行定制安装。

- **DESTINATION**

这个参数允许 `install(…)` 指定安装目录，目录可以是相对路径，也可以是绝对路径。相对路径是相对于 `CMAKE_INSTALL_PREFIX` 的，建议使用相对路径使安装可重定位。另外，使用相
对路径进行打包也很重要，因为 `cpack` 要求安装路径是相对路径。最好使用以相关 `GNUInstallDirs` 变量开始的路径，以便包维护人员在需要时覆盖安装目标。 `DESTINATION` 参数可以与 `TARGETS`，`FILES`， `IMPORTED_RUNTIME_ARTIFACTS`， `EXPORT` 和 `DIRECTORY` 安装类型一起使用。

- **PERMISSIONS**

此参数允许在支持的平台上更改已安装文件的权限。可用权限为: `OWNER_READ`、`OWNER_WRITE`、 `OWNER_EXECUTE`、 `GROUP_READ`、 `GROUP_WRITE`、 `GROUP_EXECUTE`、
`WORLD_READ`、 `WORLD_WRITE`、 `WORLD_EXECUTE`、 `SETUID` 和 `SETGID`。 `PERMISSIONS` 参数可以与 `TARGETS`, `FILES`, `IMPORTED_RUNTIME_ARTIFACTS`, `EXPORT` 和 `DIRECTORY` 安装类型一起使用。

- **CONFIGURATIONS**

允许指定构建配置时限制参数的应用。

- **OPTIONAL**

此参数使要安装的文件为可选文件，因此当文件不存在时，安装不会失败。可选参数可以与 `TARGETS`, `FILES`, `IMPORTED_RUNTIME_ARTIFACTS` 和 `DIRECTORY` 安装类型一起使用。

## 4.2 提供项目配置信息

若要导入的项目具有正确的配置文件，有很多导入库的方便方法。其中一个是使用 `find_package()`。若有使用者在工作中使用 CMake，只需要 `find_package(your_project_name)` 就可以使用你的代码。接下来，将学习如何生成所需的配置文件，使 `find_package()` 在项目中工作。

CMake 使用依赖的首选方式是通过包。包基于 CMake 的构建系统传递依赖信息，包可以是 `Config-file` 包、 `Find-module` 包或 `pkg-config` 包。所有的包类型都可以通过 `find_package()` 找到并使用。

### 4.2.1 进入 CMake 包世界 —— Config-file 包

`Config-file` 包是含包内容信息的配置文件。该信息指示包的内容位置，因此 CMake 读取该文件并使用该包。只使用包配置文件就可以使用该包。

有两种类型的配置文件 —— 包配置文件和可选的包版本文件，两个文件都必须有一个特定的命名。包配置文件可以命名为 `<ProjectName>Config.cmake` 或 `<projectname>-config.cmake`， 这取决于个人喜好。这两个符号将由 CMake 在 `find_package(ProjectName)/find_package(projectname)` 调用中选择。包配置文件的内容如下所示:

```CMake
set(Foo_INCLUDE_DIRS ${PREFIX}/include/foo-1.2)
set(Foo_LIBRARIES ${PREFIX}/lib/foo-1.2/libfoo.a)
```

这里， `${PREFIX}` 是项目的安装前缀，是一个变量，因为安装前缀可以根据系统的类型更改，也可以由用户修改。

与包配置文件类似，包版本文件可以命名为 `<ProjectName>ConfigVersion.cmake` 或 `<projectname>-config-version.cmake`。 CMake 期望包配置和包版本文件出现在 `find_package(…)` 的搜索路径中，可以在 CMake 的帮助下创建这些文件。搜索包时， `find_package(…)` 会查找 `<CMAKE_PREFIX_PATH>/cmake` 目录。示例中，将把 `Config-file` 包配置文件放到这个文件夹中。

要创建 `config-file` 包，需要学习一些东西，比如导出目标和 `CmakePackageConfigHelpers` 模块。为了了解它，这里需要一个实际的例子。这个例子位于 `chapter4/ex05_config_file_package` 文件夹。 `chapter4/ex05_config_file_package` 目录下的 [CMakeLists.txt]() 文件:

```CMake
include(GNUInstallDirs)
set(
    ch4_ex05_lib_INSTALL_CMAKEDIR cmake CACHE PATH
    "Installation directory for config-file package cmake files"
)

/*...*/
```

CMakeLists.txt 文件非常类似于 `chapter4/ex02_static`，并支持 `config-file` 打包。第一行 `include(GNUInstallDirs)` 用于包含 `GNUInstallDirs` 模块。这提供了 `CMAKE_INSTALL_INCLUDEDIR` 变量，将在稍后使用。`set(ch4_ex05_lib_INSTALL_CMAKEDIR…)` 是一个用户定义的变量，用于设置 `config-file` 打包配置文件的安装目录。这是一个相对路径，应该在 `install(…)` 指令中使用:

```CMake
/*… */

target_include_directories(
    ch4_ex05_lib PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
)

target_compile_features(ch4_ex05_lib PUBLIC cxx_std_11)

/*… */
```

`target_include_directories(…)` 与通常使用的方式不同。因为在将目标导入到另一个项目时，不存在构建时包含路径，其使用生成器表达式来区分构建时包含目录和安装时包含目录。

下面的一组命令使目标可安装:

```CMake
/*… */

install(
    TARGETS ch4_ex05_lib
    EXPORT ch4_ex05_lib_export
    INCLUDES DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)

install(
    DIRECTORY ${PROJECT_SOURCE_DIR}/include/
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
)

/*… */
```

`install(TARGETS...)` 也和平常有一点不同，包含 `EXPORT` 参数，用于从给定的 `install(…)` 目标创建一个导出名称。然后，可以使用此导出名称导出这些目标。使用 `INCLUDES DESTINATION` 参数指定的路径，将用于填充导出目标的 `INTERFACE_INCLUDE_DIRECTORIES` 属性，并自动使用安装前缀路径作为前缀。这里，`install(DIRECTORY…)` 用于安装目标的头文件，位于 `${PROJECT_SOURCE_DIR}/include/`，在 `${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_INCLUDEDIR}` 目录中。`${CMAKE_INSTALL_INCLUDEDIR}` 可以让使用者覆盖此安装的 `include` 目录。现在，可以根据前面例子中创建的导出名称创建一个导出文件:

```CMake
/*… */

install(
    EXPORT ch4_ex05_lib_export
    FILE ch4_ex05_lib-config.cmake
    NAMESPACE ch4_ex05_lib::
    DESTINATION ${ch4_ex05_lib_INSTALL_CMAKEDIR}
)

/*… */
```

`install(EXPORT…)` 是这个文件中最重要的一段代码，是执行实际目标导出的代码。其生成一个 CMake 文件，其中包含给定导出名称中的所有导出目标。 `EXPORT` 参数接受现有的导出名称来进行导出，它引用的是 `ch4_ex05_lib_export` 导出名称，我们在之前的 `install(TARGETS…)` 中创建的。 `FILE` 用于确定导出的文件名，并设置为 `ch4_ex05_lib-config.cmake`。`NAMESPACE` 用于为所有导出的目标添加命名空间前缀。这允许将所有导出的目标连接到通用的命名空间下，并避免与具有相似目标名称的包发生冲突。最后， `DESTINATION` 确定生成导出文件的安装路径。设置为
`${ch4_ex05_lib_INSTALL_CMAKEDIR}` 以便 `find_package()` 发现它。

> **Tips:** 除了导出的目标之外，没有提供额外的内容，所以导出文件的名称是 `ch4_ex05_lib-config.cmake`。它是此包所需的包配置文件名称。这样做是因为示例项目不需要满足任何依赖关系，可以按原样直接导入。若需要额外的操作，建议使用中间包配置文件来满足这些依赖关系，然后包含导出的文件。

使用 `install(EXPORT…)`，获得了 `ch4_ex05_lib-config.cmake` 文件。从而目标可以通过 `find_package(...)` 来使用，要实现对 `find_package(…)` 的完全支持，还需要一个步骤，
即获取 `ch4_ex05_lib-config-version.cmake` 文件:

```CMake
/*… */

include(CMakePackageConfigHelpers)

write_basic_package_version_file(
    "ch4_ex05_lib-config-version.cmake"
    
    # Package compatibility strategy. SameMajorVersion is essentially 'semantic versioning'.
    COMPATIBILITY SameMajorVersion
)

install(
    FILES "${CMAKE_CURRENT_BINARY_DIR}/ch4_ex05_lib-config-version.
    cmake"
    DESTINATION "${ch4_ex05_lib_INSTALL_CMAKEDIR}"
)

/* end of the file *
```

最后几行中，可以找到生成和安装 `ch4_ex05_lib-config-version.cmake` 文件所需的代码。使用 `include(CMakePackageConfigHelpers)`，导入 `CMakePackageConfigHelpers` 模块。这个模块提供了 `write_basic_package_version_file(…)` 函数，用于根据给定的参数自动生成包版本文件。第一个位置参数是输出的文件名。 `VERSION` 用于以 `major.minor.patch` 形式指定生成包的版本，允许 `write_basic_package_version_file` 从项目版本自动获取。 `COMPATIBILITY` 可以根据版本的值指定兼容性策略。 `SameMajorVersion` 表示此包与具有此包相同主版本值的任何版本兼容，其他可能的值为 `AnyNewerVersion`、 `SameMinorVersion` 和 `ExactVersion`。

测试包配置:

```shell
$ cmake -S . -B build
...
$ cmake --build build/
...
$ cmake --install ./build/ --prefix /tmp/install-test
-- Install configuration: ""
-- Installing: /tmp/install-test/lib/libch4_ex05_lib.a
-- Installing: /tmp/install-test/include
-- Installing: /tmp/install-test/include/chapter4
-- Installing: /tmp/install-test/include/chapter4/ex05
-- Installing: /tmp/install-test/include/chapter4/ex05/lib.hpp
-- Installing: /tmp/install-test/cmake/ch4_ex05_lib-config.cmake
-- Installing: /tmp/install-test/cmake/ch4_ex05_lib-config-noconfig.cmake
-- Installing: /tmp/install-test/cmake/ch4_ex05_lib-config-version.cmake
```

可以看到包配置文件已经成功安装在 `/tmp/install-test/cmake` 目录下。接下来查看，`chapter4/ex05_consumer` 示例中的 CMakeLists.txt 文件:

```CMake
if (NOT PROJECT_IS_TOP_LEVEL)
    message(FATAL_ERROR "The chapter-4, ex05_consumer project is intended to be a standalone, top-level project. Do not vendor this directory")
endif()

find_package(ch4_ex05_lib 1 CONFIG REQUIRED)

add_executable(ch4_ex05_consumer src/main.cpp)

target_compile_features(ch4_ex05_consumer PRIVATE cxx_std_11)

target_link_libraries(ch4_ex05_consumer PRIVATE ch4_ex05_lib::ch4_ex05_lib)
```

前几行中，可以看到关于项目是否是顶层项目的验证。这个示例是一个外部应用程序，所以不应该是根示例项目的一部分。因此，可以保证用包导出的目标，而不是根项目的目标，根项目也不包括 `ex05_consumer` 文件夹。接下来，有一个 `find_package(…)`，其中 `ch4_ex05_lib` 作为包名给出。还明确要求包的主版本为 `1`; `find_package(…)` 只考虑 `CONFIG` 包和 `find_package(…)` 中指定的包是必需的。在随后的行中，一个常规的可执行文件定义为 `ch4_ex05_consumer`，它在 `ch4_ex05_lib` 命名空间 (`ch4_ex05_lib::ch4_ex05_lib`) 中链接到 `ch4_ex05_lib`。`ch4_ex05_lib::ch4_ex05_lib` 是我们在包中定义的实际目标。来看看源文件 `src/main.cpp`:

```CMake
#include <chapter4/ex05/lib.hpp>

int main(void) {
    chapter4::ex05::greeter g;
    g.greet();
}
```

这是一个包含 `chapter4/ex05/lib.hpp` 的简单应用程序，创建了一个 `greeter` 类的实例，并调用了 `greet()` 函数。试着编译并运行这个应用程序:

```shell
$ cd ex05_consumer/
$ cmake -S . -B build -DCMAKE_PREFIX_PATH:STRING=/tmp/install-test
...
$ cmake --build build
...
$ ./build/ch4_ex05_consumer 
Hello, world!
```

已经使用自定义前缀 `(/tmp/install-test)` 安装了包，所以可以通过设置 `CMAKE_PREFIX_PATH` 变量来表明这一点。这会导致 `find_package(…)` 也会在 `/tmp/install-test` 中搜索包。对于默认的前缀安装，不需要此参数设置。若一切顺利的话，我们应该看到输出为 `Hello, world!`。这样，使用者就可以使用我们的配置文件。

