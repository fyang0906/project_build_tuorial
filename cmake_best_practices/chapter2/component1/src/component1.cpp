/**
 * component-1 implementation
*/

#include <framework/components/component1.hpp>

#include <iostream>

namespace framework {
namespace components {

component1::component1() {
    std::cout << "component1 created" << std::endl;
}

component1::~component1() {
    std::cout << "component1 destroyed" << std::endl;
}

bool component1::do_stuff() const { return false; }

int component1::do_other_stuff(int param) { return param; }

} // namespace components
} // namespace framework
