/**
 * Abstrace component interface description
*/
#pragma once

namespace framework {
namespace components {

/**
 * @brief Component interface
*/
class component_interface {
public:

    /**
    * @brief Destructor
    */
    virtual ~component_interface() = default;

    /**
     * @brief Do some work
     *
     * @return true on succeess, false on failure
     */
    virtual bool do_stuff() const = 0;

    /**
     * @brief Do other stuff
     * @param pragma Arguement
     *
     * @return int stuffs completed
    */
    virtual int do_other_stuff(int pragma) = 0;
}; // class component_interface

} // namespace components
} // namespace framework