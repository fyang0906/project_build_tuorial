# 2. CMake 的最佳使用方式

- 通过命令行使用 CMake
- 使用 cmake-gui 和 ccmake
- IDE 和编辑器集成 (Visual Studio, Visual Studio Code 和 Qt Creator)

## 2.1 通过命令行使用 CMake

- 查看 CMake 版本

```bash
$ cmake --version
cmake version 3.28.2

CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

> **注意：** 如果没有安装 CMake ，在 Ubuntu 系统中可以使用命令 `sudo apt install cmake` 进行安装。
> 其他环境安装 CMake 请参考 [CMake 官方文档](https://cmake.org/download/)。
>
> 安装 CMake 之后，应该安装构建系统和编译器。对于类似 Debian 的操作系统 (例如， Debian 和 Ubuntu)，
> 这可以通过 `sudo apt install build-essential` 命令轻松完成，包含对 gcc、g++ 和 make 的安装。

**了解 CMake 命令行的基本操作**

- 配置
- 构建
- 安装

**通过命令行配置项目**

要通过命令行配置 CMake 项目，可以使用 `CMake -G "Unix Makefiles" -S -B <output_directory>` 的方式。
`-S` 参数用于指定要配置的 CMake 项目，而 `-B` 参数用于指定配置输出目录。
最后，`-G` 参数指定将用于生成构建系统的生成器，配置过程的结果将写入 `<output_directory>`。

进入 CMake-Best-Practices/chapter2 目录， 使用 `cmake -G "Unix Makefiles" -S . -B ./build`:

```shell
$ cmake -G "Unix Makefiles" -S . -B ./build/
-- The CXX compiler identification is GNU 11.4.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.7s)
-- Generating done (0.0s)
-- Build files have been written to: /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build
```

> 使用 `"Unix Makefiles"` (`-G "Unix Makefiles"`) 生成器在当前目录 ( `-S .` ) 生成 CMake 项目的构建系统 ( `-B ./build` ) 目录。
> CMake 将设置源文件夹为当前文件夹中的项目。当省略构建类型时， CMake 使用 Debug 构建类型 (项目默认的 `CMAKE_BUILD_TYPE` )。

**修改构建类型**

CMake 不假定构建类型。 为了设置构建类型， 必须在配置阶段提供一个名为 `CMAKE_BUILD_TYPE` 的变量，变量必须以 `-D` 作为前缀。
要获得 Release 版本，而不是 Debug 版本，需要在配置命令中添加 `CMAKE_BUILD_TYPE` 变量，该命令为: `cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release -S . -B ./build`。

```shell
$ cmake -G "Unix Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release -S . -B ./build/
-- The CXX compiler identification is GNU 11.4.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.8s)
-- Generating done (0.0s)
-- Build files have been written to: /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build
```

**更换生成器类型**

根据不同的环境， CMake 会在默认情况下尝试选择合适的生成器。要显式指定生成器， `-G` 参
数必须提供有效的生成器名称。例如，若想使用 Ninja 作为构建系统，而不是 make，可以这样:

```shell
$ cmake -G "Ninja" -DCMAKE_BUILD_TYPE:STRING=Debug -S . -B ./build/
CMake Error: CMake was unable to find a build program corresponding to "Ninja".  CMAKE_MAKE_PROGRAM is not set.  You probably need to select a different build tool.
CMake Error: CMAKE_CXX_COMPILER not set, after EnableLanguage
-- Configuring incomplete, errors occurred!
```

> **注意：** 上述错误是因为未安装 ninja 构建工具，Ubuntu 中使用 `sudo apt install ninja-build` 命令安装

```shell
$ sudo apt install ninja-build
```

```shell
$ cmake -G "Ninja" -DCMAKE_BUILD_TYPE:STRING=Debug -S . -B ./build/
-- The CXX compiler identification is GNU 11.4.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.6s)
-- Generating done (0.0s)
-- Build files have been written to: /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build
$ cd build/
$ ls
CMakeCache.txt  CMakeFiles  build.ninja  cmake_install.cmake  component1  component2  component_interface
```

这将导致 CMake 生成 Ninja 的构建文件，而不是生成 makefile 文件。

为了查看环境中可用的生成器类型，可使用 `cmake --help` 。

**修改编译器**

CMake 中，编译器可以通过 `CMAKE_<LANG>_COMPILER` 来指定。为了改变编译器，必须在配置命令中提供 `CMAKE_<LANG>_COMPILER`。

对于 C/C++ 项目，通常修改的是 `CMAKE_C_COMPILER` (C 编译器) 和 `CMAKE_CXX_COMPILER` (C++ 编译器)。编译器标志由 `CMAKE_<LANG>_FLAGS` 控制。此变量可用于保存与配置无关的编译器标志。

例如，尝试使用 `arm-linux-gnueabihf-g++-11` 作为 C++ 编译器，但它不是默认编译器:

> **Tips：**
>
> 如果所使用的Linux环境没有安装 `arm-linux-gnueabihf-g++-11` 编译器，（Ubuntu）请使用命令：
> `sudo apt install g++-arm-linux-gnueabihf` 进行安装。其他Linux发行版请自行查阅资料安装 arm 编译器。

```shell
$ cmake -G "Unix Makefiles" -DCMAKE_CXX_COMPILER=/usr/bin/arm-linux-gnueabihf-g++-11 -S . -B ./build/
-- The CXX compiler identification is GNU 11.4.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/arm-linux-gnueabihf-g++-11 - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (1.5s)
-- Generating done (0.0s)
-- Build files have been written to: /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build
```

**向编译器传递编译标志**

为了演示如何指定编译器标志，假设需要启用警告视为错误。这在 gcc 工具链中分别用 `-Wall` 和 `-Werror` 编译器标志控制。因此，需要将这些标志传递给 C++ 编译器:

```shell
$ cmake -G "Unix Makefiles" -DCMAKE_CXX_FLAGS:STRING="-Wall -Werror" -S . -B ./build/
```

下面的例子中，将标志 (`-Wall` 和 `-Werror`) 传递给了编译器:

```shell
$ cmake -G "Unix Makefiles" -DCMAKE_CXX_FLAGS:STRING="-Wall -Werror" -S . -B ./build/
$ cmake --build ./build/ --clean-first --target ch2_framework_component1  --verbose | grep "\-Wall"
cd /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build/component1 && /usr/bin/c++  -I/home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component1/include -I/home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component_interface/include -Wall -Werror -MD -MT component1/CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o -MF CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o.d -o CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o -c /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component1/src/component1.cpp
```

构建标志可以为每个构建类型定制，方法是在它们后面加上大写的构建类型字符串。有四个变量用于四个不同的构建类型，它们对于根据编译器标志指定构建类型非常有用。这些变量中指定的标志只在配置构建类型匹配时有效：

1. `CMAKE_<LANG>_FLAGS_DEBUG`
2. `CMAKE_<LANG>_FLAGS_RELEASE`
3. `CMAKE_<LANG>_FLAGS_RELWITHDEBINFO`
4. `CMAKE_<LANG>_FLAGS_MINSIZEREL`

若希望只在 Release 构建中将警告视为错误，可以使用构建类型特定的编译器标志。下面的示例，演示了如何使用特定于构建类型的编译器标志:

```shell
cmake -G "Unix Makefiles" -DCMAKE_CXX_FLAGS:STRING="-Wall -Werror" -DCMAKE_CXX_FLAGS_RELEASE:STRING="-O3" -DCMAKE_BUILD_TYPE:STRING=Debug -S . -B ./build
```

前面的命令中有一个 CMAKE_CXX_FLAGS_RELEASE 参数，只有当构建类型为 Release 时，这个变量中的内容才会传递给编译器。由于构建类型指定为 Debug，可以看到传递给编译器的标志中没有-O3 标志:

```shell
$ cmake -G "Unix Makefiles" -DCMAKE_CXX_FLAGS:STRING="-Wall -Werror" -DCMAKE_CXX_FLAGS_RELEASE:STRING="-O3" -DCMAKE_BUILD_TYPE:STRING=Debug -S . -B ./build
-- The CXX compiler identification is GNU 11.4.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.6s)
-- Generating done (0.0s)
CMake Warning:
  Manually-specified variables were not used by the project:

    CMAKE_CXX_FLAGS_RELEASE


-- Build files have been written to: /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build
```

```shell
$ cmake --build ./build/ --clean-first --target ch2_framework_component1  --verbose | grep "\-Wall"
cd /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build/component1 && /usr/bin/c++  -I/home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component1/include -I/home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component_interface/include -Wall -Werror -g -MD -MT component1/CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o -MF CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o.d -o CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o -c /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component1/src/component1.cpp
```

CMake 发出一个关于指定，但未使用变量的警告， `CMAKE_CXX_FLAGS_RELEASE`。这确认了 `CMAKE_CXX_FLAGS_RELEASE` 没有在 `Debug` 构建类型中使用。当构建类型指定为 `Release` 时，可以看到 `-O3` 标志:


```shell
$ cmake -G "Unix Makefiles" -DCMAKE_CXX_FLAGS:STRING="-Wall -Werror" -DCMAKE_CXX_FLAGS_RELEASE:STRING="-O3" -DCMAKE_BUILD_TYPE:STRING=Release -S . -B ./build
-- The CXX compiler identification is GNU 11.4.0
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done (0.6s)
-- Generating done (0.0s)
-- Build files have been written to: /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build
```

```shell
$ cmake --build ./build/ --clean-first --target ch2_framework_component1  --verbose | grep "\-Wall"
cd /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build/component1 && /usr/bin/c++  -I/home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component1/include -I/home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component_interface/include -Wall -Werror -O3 -MD -MT component1/CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o -MF CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o.d -o CMakeFiles/ch2_framework_component1.dir/src/component1.cpp.o -c /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/component1/src/component1.cpp
```

**缓存变量列表**

可以通过 `cmake -L ./build/` 列出所有缓存的变量。默认情况下，这不会显示与每个变量关联的高级变量和帮助字符串。也可以使用 `cmake -LAH ./build/` 来显示它们。

```shell
$ cmake -LAH ./build/
-- Configuring done (0.0s)
-- Generating done (0.0s)
-- Build files have been written to: /home/fyang/code/cmake-tutorial/CMake-Best-Practices/chapter2/build
-- Cache values
// Whether to include driver application in build. Default: True
CHAPTER2_BUILD_DRIVER_APPLICATION:BOOL=OFF

// Path to a program.
CMAKE_ADDR2LINE:FILEPATH=/usr/bin/addr2line

// Path to a program.
CMAKE_AR:FILEPATH=/usr/bin/ar

// No help, variable specified on the command line.
CMAKE_BUILD_TYPE:STRING=Release

// Enable/Disable color output during build.
CMAKE_COLOR_MAKEFILE:BOOL=ON

// CXX compiler
CMAKE_CXX_COMPILER:FILEPATH=/usr/bin/c++

// A wrapper around 'ar' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_CXX_COMPILER_AR:FILEPATH=/usr/bin/gcc-ar-11

// A wrapper around 'ranlib' adding the appropriate '--plugin' option for the GCC compiler
CMAKE_CXX_COMPILER_RANLIB:FILEPATH=/usr/bin/gcc-ranlib-11

// No help, variable specified on the command line.
CMAKE_CXX_FLAGS:STRING=-Wall -Werror

// Flags used by the CXX compiler during DEBUG builds.
CMAKE_CXX_FLAGS_DEBUG:STRING=-g

// Flags used by the CXX compiler during MINSIZEREL builds.
CMAKE_CXX_FLAGS_MINSIZEREL:STRING=-Os -DNDEBUG

// No help, variable specified on the command line.
CMAKE_CXX_FLAGS_RELEASE:STRING=-O3

// Flags used by the CXX compiler during RELWITHDEBINFO builds.
CMAKE_CXX_FLAGS_RELWITHDEBINFO:STRING=-O2 -g -DNDEBUG

// Path to a program.
CMAKE_DLLTOOL:FILEPATH=CMAKE_DLLTOOL-NOTFOUND

// Flags used by the linker during all build types.
CMAKE_EXE_LINKER_FLAGS:STRING=

// Flags used by the linker during DEBUG builds.
CMAKE_EXE_LINKER_FLAGS_DEBUG:STRING=

// Flags used by the linker during MINSIZEREL builds.
CMAKE_EXE_LINKER_FLAGS_MINSIZEREL:STRING=

// Flags used by the linker during RELEASE builds.
CMAKE_EXE_LINKER_FLAGS_RELEASE:STRING=

// Flags used by the linker during RELWITHDEBINFO builds.
CMAKE_EXE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

// Enable/Disable output of compile commands during generation.
CMAKE_EXPORT_COMPILE_COMMANDS:BOOL=

// Install path prefix, prepended onto install directories.
CMAKE_INSTALL_PREFIX:PATH=/usr/local

// Path to a program.
CMAKE_LINKER:FILEPATH=/usr/bin/ld

// Path to a program.
CMAKE_MAKE_PROGRAM:FILEPATH=/usr/bin/gmake

// Flags used by the linker during the creation of modules during all build types.
CMAKE_MODULE_LINKER_FLAGS:STRING=

// Flags used by the linker during the creation of modules during DEBUG builds.
CMAKE_MODULE_LINKER_FLAGS_DEBUG:STRING=

// Flags used by the linker during the creation of modules during MINSIZEREL builds.
CMAKE_MODULE_LINKER_FLAGS_MINSIZEREL:STRING=

// Flags used by the linker during the creation of modules during RELEASE builds.
CMAKE_MODULE_LINKER_FLAGS_RELEASE:STRING=

// Flags used by the linker during the creation of modules during RELWITHDEBINFO builds.
CMAKE_MODULE_LINKER_FLAGS_RELWITHDEBINFO:STRING=

// Path to a program.
CMAKE_NM:FILEPATH=/usr/bin/nm

// Path to a program.
CMAKE_OBJCOPY:FILEPATH=/usr/bin/objcopy

// Path to a program.
CMAKE_OBJDUMP:FILEPATH=/usr/bin/objdump

// Path to a program.
CMAKE_RANLIB:FILEPATH=/usr/bin/ranlib

// Path to a program.
CMAKE_READELF:FILEPATH=/usr/bin/readelf

// Flags used by the linker during the creation of shared libraries during all build types.
CMAKE_SHARED_LINKER_FLAGS:STRING=

// Flags used by the linker during the creation of shared libraries during DEBUG builds.
CMAKE_SHARED_LINKER_FLAGS_DEBUG:STRING=

// Flags used by the linker during the creation of shared libraries during MINSIZEREL builds.
CMAKE_SHARED_LINKER_FLAGS_MINSIZEREL:STRING=

// Flags used by the linker during the creation of shared libraries during RELEASE builds.
CMAKE_SHARED_LINKER_FLAGS_RELEASE:STRING=

// Flags used by the linker during the creation of shared libraries during RELWITHDEBINFO builds.
CMAKE_SHARED_LINKER_FLAGS_RELWITHDEBINFO:STRING=

// If set, runtime paths are not added when installing shared libraries, but are added when building.
CMAKE_SKIP_INSTALL_RPATH:BOOL=NO

// If set, runtime paths are not added when using shared libraries.
CMAKE_SKIP_RPATH:BOOL=NO

// Flags used by the linker during the creation of static libraries during all build types.
CMAKE_STATIC_LINKER_FLAGS:STRING=

// Flags used by the linker during the creation of static libraries during DEBUG builds.
CMAKE_STATIC_LINKER_FLAGS_DEBUG:STRING=

// Flags used by the linker during the creation of static libraries during MINSIZEREL builds.
CMAKE_STATIC_LINKER_FLAGS_MINSIZEREL:STRING=

// Flags used by the linker during the creation of static libraries during RELEASE builds.
CMAKE_STATIC_LINKER_FLAGS_RELEASE:STRING=

// Flags used by the linker during the creation of static libraries during RELWITHDEBINFO builds.
CMAKE_STATIC_LINKER_FLAGS_RELWITHDEBINFO:STRING=

// Path to a program.
CMAKE_STRIP:FILEPATH=/usr/bin/strip

// Path to a program.
CMAKE_TAPI:FILEPATH=CMAKE_TAPI-NOTFOUND

// If this value is on, makefiles will be generated without the .SILENT directive, and all commands will be echoed to the console during the make.  This is useful for debugging only. With Visual Studio IDE projects all commands are done without /nologo.
CMAKE_VERBOSE_MAKEFILE:BOOL=FALSE
```

**通过 CLI 构建配置好的项目**

要生成已配置的工程，需要使用 `cmake --build ./build` 在构建文件夹中配置的 CMake 项目。
也可以使用 `cd build && make` 进行构建。使用 `cmake --build` 的好处是，省去了调用特定于构建系统的命令。在构建 CI 管道或构建脚本时，可以在不更改构建命令的情况下，更改构建系统生成器。

可以看到 `cmake --build ./build` 的输出示例如下所示:

```shell
$ cmake --build ./build/
[ 50%] Built target ch2_framework_component1
[ 75%] Building CXX object component2/CMakeFiles/ch2_framework_component2.dir/src/component2.cpp.o
[100%] Linking CXX shared library libch2_framework_component2.so
[100%] Built target ch2_framework_component2
```

**并行构建**

还可以在发出生成命令时，自定义生成时间详细信息。要指定并行构建的任务数，可以在 `cmake --build` 中追加`--parallel <job_count>`。要并行构建，请使用 `cmake --build ./build --parallel 2`，其中数字 2 指定了任务数。对于构建系统，建议的任务数最多为每个硬件线程一个作业。多核系统中，还建议至少使用比可用硬件线程数少一个的线程，以避免在构建过程中影响系统的响应能力。

> **Note:**
>
> 通常可以在每个硬件线程中使用多个任务，从而获得更快的构建时间，因为构建过程主要受 I/O 限制，但实际情况可能有所不同，需要进行实验和观察。此外，某些构建系统 (如 Ninja) 会尝试利用系统中可用的尽可能多的硬件线程，若目标是使用系统中的所有硬件线程，那么为此类构建系统指定任务数就多余了。可以在 Linux 环境中，使用 `nproc` 命令来获取硬件线程计数。


在期望在不同环境中调用的命令 (如 CI/CD 脚本和构建脚本) 中，不使用环境相关变量的固定
值，这是一个很好的实践。下面是一个使用 nproc 动态确定并行任务数量的构建命令示例:

```shell
cmake --build ./build/ --parallel $(($(nproc)-1))
```

观察不同的任务数如何影响构建时间，将使用时间工具来测试每次命令调用的时间。

对于一个任务 (--parallel 1)，构建时间如下所示:

```shell
$ /usr/bin/time --format='%C took %e seconds' cmake --build build --clean-first --parallel 1 1> /dev/null
cmake --build build --clean-first --parallel 1 took 0.50 seconds
```

两个任务 (--parallel 2) 的构建时间结果如下:

```shell
$ /usr/bin/time --format='%C took %e seconds' cmake --build build --clean-first --parallel 2 1> /dev/null
cmake --build build --clean-first --parallel 2 took 0.39 seconds
```

三个任务 (--parallel 3) 的构建时间结果如下:

```shell
$ /usr/bin/time --format='%C took %e seconds' cmake --build build --clean-first --parallel 3 1> /dev/null
cmake --build build --clean-first --parallel 3 took 0.37 seconds
```

四个任务 (--parallel 4) 的构建时间结果如下:

```shell
$ /usr/bin/time --format='%C took %e seconds' cmake --build build --clean-first --parallel 4 1> /dev/null
cmake --build build --clean-first --parallel 4 took 0.37 seconds
```

可以看到 3 个作业到 4 个任务时间几乎无差异，这意味着已经达到了这个项目的构建并行性的极限，增加更多的工作将不会在构建时间上实现显著的差异。

**只构建特定的目标**

通常， CMake 将构建已配置的所有可用目标。由于构建所有目标并不总是理想的， CMake 允许通过 `--target` 子选项构建目标的子集，子选项可以指定多次:

```shell
cmake --build ./build/ --target "ch2_framework_component1" --target "ch2_framework_component2"
```

该命令将构建范围限制为 ch2_framework_component1 和 ch2_framework_component2 目标。若这些目标也依赖于其他目标，它们也会构建。

**构建之前删除以前的构建**

若想要运行一个干净的构建，首先就要删除之前的构建。为此，可以使用 `--clean-first` 子选项。此子选项将调用一个特殊的目标，该目标清除由构建过程生成的所有构件 (例如，调用 `make clean`)。下面是一个示例，说明如何对名为 build 的构建文件夹执行此操作:

```shell
cmake --build ./build/ --clean-first
```

**调试构建过程**

正如在前面的向编译器传递标志一节中所做的那样，可能希望检查构建过程中使用哪些参数调用了哪些命令。 `--verbose` 指示 CMake 以 verbose 模式调用所有构建命令，前提是该命令支持 verbose
模式。这使我们能够轻松地调试编译和链接错误。

要在 verbose 模式下构建名为 build 的文件夹，可以使用:

```shell
cmake --build ./build/ --verbose
```

**将命令行参数传递给构建工具**

若需要将参数传递给底层构建工具，可以在命令的末尾加上 `--`，并输入给定的参数

```shell
cmake --build ./build/ -- --trace
```

`--trace` 将直接传递给构建工具，例子中就是 make。这将使 make 为构建每个文件的编译和连接输出跟踪信息。

**通过 CLI 安装项目**

CMake 允许在环境中进行安装， CMake 代码必须使用 `install()` 指令来指定当调用 `cmake --install`(或构建系统等效) 时安装什么。 chapter_2 的内容已经以这样的方式配置用于说明指令。

`cmake --install` 需要一个已经配置和生成的项目，执行 `cmake --install <project_binary_dir>` 来安装工程。我们的例子中， `build` 作为项目二进制目录，所以 `<project_binary_dir>` 将为 `build`。

```shell
sudo cmake --install build
```

不同环境的默认安装目录不同。对于类 Unix 环境，默认为 `/usr/local`，而在 Windows 环境中，默认为 `C:/Program Files`。

**Tip:**

> 在尝试安装项目之前，必须已经构建了项目。为了能够成功安装项目，必须拥有适当的权限来写入安装目标目录。

**修改默认安装路径**

要更改默认安装目录，可以指定的 `--prefix` 参数，如下所示

```shell
sudo cmake --install build --prefix /tmp/example
```

将安装目录指定为 `/tmp/example`，使用 `cmake --install` 后， `/tmp/example` 目录下的内容如
下所示:

```shell
$ sudo cmake --install build --prefix /tmp/example
-- Install configuration: "Release"
-- Installing: /tmp/example/include
-- Installing: /tmp/example/include/framework
-- Installing: /tmp/example/include/framework/components
-- Installing: /tmp/example/include/framework/components/component_interface.hpp
-- Installing: /tmp/example/lib/libch2_framework_component1.a
-- Up-to-date: /tmp/example/include
-- Up-to-date: /tmp/example/include/framework
-- Up-to-date: /tmp/example/include/framework/components
-- Installing: /tmp/example/include/framework/components/component1.hpp
-- Installing: /tmp/example/lib/libch2_framework_component2.so
-- Up-to-date: /tmp/example/include
-- Up-to-date: /tmp/example/include/framework
-- Up-to-date: /tmp/example/include/framework/components
-- Installing: /tmp/example/include/framework/components/component2.hpp
```

```shell
$ ls -lRah /tmp/example/
/tmp/example/:
total 16K
drwxr-xr-x  4 root root 4.0K Mar 18 17:01 .
drwxrwxrwt 11 root root 4.0K Mar 18 17:01 ..
drwxr-xr-x  3 root root 4.0K Mar 18 17:01 include
drwxr-xr-x  2 root root 4.0K Mar 18 17:01 lib

/tmp/example/include:
total 12K
drwxr-xr-x 3 root root 4.0K Mar 18 17:01 .
drwxr-xr-x 4 root root 4.0K Mar 18 17:01 ..
drwxr-xr-x 3 root root 4.0K Mar 18 17:01 framework

/tmp/example/include/framework:
total 12K
drwxr-xr-x 3 root root 4.0K Mar 18 17:01 .
drwxr-xr-x 3 root root 4.0K Mar 18 17:01 ..
drwxr-xr-x 2 root root 4.0K Mar 18 17:01 components

/tmp/example/include/framework/components:
total 20K
drwxr-xr-x 2 root root 4.0K Mar 18 17:01 .
drwxr-xr-x 3 root root 4.0K Mar 18 17:01 ..
-rw-r--r-- 1 root root  831 Jan 31 14:58 component1.hpp
-rw-r--r-- 1 root root  829 Jan 31 15:21 component2.hpp
-rw-r--r-- 1 root root  660 Jan 31 14:51 component_interface.hpp

/tmp/example/lib:
total 40K
drwxr-xr-x 2 root root 4.0K Mar 18 17:01 .
drwxr-xr-x 4 root root 4.0K Mar 18 17:01 ..
-rw-r--r-- 1 root root 8.4K Mar 18 16:46 libch2_framework_component1.a
-rw-r--r-- 1 root root  18K Mar 18 16:46 libch2_framework_component2.so
```

可以看到，安装根目录成功更改为 `/tmp/example`。

**安装时瘦身二进制文件**

软件世界中，构建工件通常与一些额外的信息捆绑在一起，例如：调试所需的符号表。这些信息在最终产品时可能不是必需的，并且可能会极大地增加二进制大小。若希望减少最终产品的存储空间，瘦身二进制文件可能是一个不错的选择。瘦身的另一个好处是，这会使逆向工程二进制文件变得更加困难，因为从二进制文件中剥离了基本的符号信息。

CMake 的 `--install` 允许在安装操作时瘦身二进制文件。可以通过在 `--install` 中使用 `--strip` 选
项来启用:

```shell
cmake --install build --strip
```

下面的示例中，可以观察未瘦身和瘦身的二进制文件之间的大小差异。注意，瘦身静态库有一些限制， CMake 默认情况下不会执行:

```shell
$ ls -lrah /usr/local/lib/ | grep libch
-rw-r--r--  1 root root  18K Mar 18 16:46 libch2_framework_component2.so
-rw-r--r--  1 root root 8.4K Mar 18 16:46 libch2_framework_component1.a
```

使用瘦身的 (`cmake --install build --strip`) 二进制文件，大小差异如下所示:

```shell
$ sudo cmake --install build --strip
...

$ ls -lrah /usr/local/lib/ | grep libch
-rw-r--r--  1 root root  15K Mar 18 17:19 libch2_framework_component2.so
-rw-r--r--  1 root root 8.4K Mar 18 16:46 libch2_framework_component1.a
```

## 2.2 CMake-GUI 和 ccmake 的高级配置

**如何使用 ccmake**

ccmake 是 CMake 的一个基于终端的图形用户界面 (GUI)，允许用户编辑缓存的 CMake 变量。不叫它 GUI，术语终端用户界面 (TUI) 可能更适合，因为没有传统的 Shell UI 元素，窗口和按钮使用文本接口在终端中呈现。ccmake 是 CMake 的一部分，所以除了 CMake 之外，不需要额外的安装。使用 ccmake 与在 CLI 中使用 CMake 相同，只是缺少了构建和安装步骤。主要的区别是 ccmake 是基于终端的图形界面，用于交互式编辑缓存的 CMake 变量。 ccmake 的状态栏将显示每个设置及其可能值的描述。

使用 ccmake 配置项目时，与之前在通过 CLI 配置项目一样:

```shell
ccmake -G "Unix Makefiles" -S . -B build/
```

![ccmake_1.png](./image/ccmake_1.png)

执行该命令后，将出现基于终端的用户界面。初始页面是可以编辑 CMake 变量的页面，空缓存意味着没有预先配置和 CMake 缓存文件 (CMakeCache.txt) 目前是空的。为了编辑变量，必须首先配置项目。要进行配置，请按下键盘上的 c 键，如 Keys: 部分所示。

按下 c 键后，将执行 CMake 配置步骤，（按 l 进入日志输出界面，关闭日志输出屏幕并返回到主屏幕，请按 e），输出配置信息:

![ccmake_2.png](./image/ccmake_2.png)

注意 `EMPTY CACHE` 会替换成 CMakeCache.txt 文件中的变量名称。要选择一个变量，请使用键盘上的向上和向下箭头键。选中的变量将以白色高亮显示。

前面的屏幕截图中，选择了 `CMAKE_BUILD_TYPE`。在右边，显示了 CMake 变量的当前值。对于 `CMAKE_BUILD_TYPE`，现在是空的。变量值旁边的星号表示该变量的值刚刚更改了之前的配置，可以按回车键进行编辑，也可以按 D 键删除。下图显示了修改变量后 ccmake 主界面的样子:

![ccmake_3.png](./image/ccmake_3.png)

设置 `CMAKE_BUILD_TYPE` 为 Release 并再次配置:

![ccmake_4.png](./image/ccmake_4.png)

![ccmake_5.png](./image/ccmake_5.png)

可以观察到构建类型现在设置为 Release。返回到上一个屏幕，按下 g(生成) 按钮保存更改。可以按 q(退出而不生成) 按钮丢弃更改。

要编辑其他变量，例如： `CMAKE_CXX_COMPILER` 和 `CMAKE_CXX_FLAGS`，高级模式应该打开。默认情况下，可以通过 `mark_as_advanced()` 将这些变量标记为高级变量; 默认情况下，在图形界面中是隐藏的，主界面按 “t” 键可切换到高级模式。

![ccmake_6.png](./image/ccmake_6.png)

激活高级模式后，选项将变得可见。可以观察和修改它们的值，就像普通变量一样。以前隐藏的名为 `CHAPTER2_BUILD_DRIVER_APPLICATION` 现在出现了，这是一个用户定义的 CMake 变量。该变量的定义如下:

```cmake
# Option to exclude driver application from build.
set(CHAPTER2_BUILD_DRIVER_APPLICATION
    TRUE
    CACHE BOOL "Whether to include driver application in build. Default: True"
)
# Hide this option from GUI's by default.
mark_as_advanced(CHAPTER2_BUILD_DRIVER_APPLICATION)
```

`CHAPTER2_BUILD_DRIVER_APPLICATION` 是一个布尔类型的缓存变量，默认值为 `true`。它标记为“高级”，这就是为什么它在非高级模式中不显示的原因。

**使用 cmake-gui**

若认为 CLI 反直觉，或更喜欢 GUI 而不是 CLI，那么 CMake 也有跨平台的 GUI。与 ccmake 相比， cmake-gui 提供了更多的功能，比如：环境编辑器和正则表达式资源管理器。

CMake GUI 是默认 CMake 安装的一部分; 除了 CMake 之外，不需要额外安装，主要目的是允许用户配置 CMake 项目。要启动 cmake-gui，请在终端中输入 cmake-gui。 Windows 下，也可以从开始菜单中找到它。若这些方法都不起作用，进入 CMake 的安装路径，它应该在 bin 目录下。

**Note：**

> 若在 Windows 环境中启动 cmake-gui，并且打算使用 Visual Studio 提供的工具链，从 IDE 的适当的本地工具命令提示符启动 cmake-gui。若有多个版本的 IDE，请确保正在使用正确的 Native Tools 命令提示符。否则， CMake 可能无法发现所需的工具，如编译器，或可能发现不正确的工具。更多信息请参考 https://docs.microsoft.com/en-us/visualstudio/ide/reference/command-prompt-powershell?view=vs-2019。

![cmake-gui_1.png](./image/cmake-gui_1.png)

CMake GUI 的主屏幕包含以下内容:
- 源码路径字段
- 输出路径字段
- 配置和生成按钮
- 缓存变量列表
要开始配置项目，请单击 Browse Source… 按钮，选择项目的根目录，通过单击 Browse Build… 按钮为项目选择一个输出目录。此路径将是所选生成器生成的输出文件的路径。设置源和输出路径后，单击 Configure 开始配置所选项目。 CMake GUI 会让你选择使用的生成器、平台选择 (若生成器支持的话)、工具集和编译器等细节，如下图所示:

![cmake-gui_2.png](./image/cmake-gui_2.png)

根据环境填写这些信息之后，单击 Finish 继续。 CMake GUI 将开始使用给定的详细信息配置项目，并在日志部分报告输出。配置成功后，也会在缓存变量列表部分看到缓存变量:

![cmake-gui_3.png](./image/cmake-gui_3.png)

若一切正常，请按 Generate 按钮生成所选构建系统所需的构建文件。

有时，项目可能需要调整一些缓存变量，或者需要使用不同的构建类型。要更改缓存变量，请单击所需缓存变量的值。根据变量类型的不同，可能会显示复选框，而不是字符串。若需要的变量在列表中不可见，则可能是高级变量，只有选中窗口上的 advanced 复选框时才可见。还可以使用搜索框更容易地定位变量，还可以在 cmake-gui 的高级模式下看到:

![cmake-gui_4.png](./image/cmake-gui_4.png)

调整缓存值之后，单击 Configure，然后单击 Generate 应用更改。

已经介绍了 cmake-gui 的最基本特性。处理其他杂项之前，若需要重新加载缓存值或删除缓存并从头开始，可以在文件菜单中找到“Reload Cache”和“Delete Cache”菜单项。

**修改环境变量**

CMake GUI 提供了一个方便的环境变量编辑器，允许对环境变量进行 CRUD 操作。要访问它，只需单击主屏幕上的 Environment… 按钮。点击后，会弹出“Environment Editor”窗口:

![cmake-gui_5.png](./image/cmake-gui_5.png)

Environment Editor 窗口包含当前环境中存在的环境变量列表。要编辑环境变量，双击表中所需环境变量的值字段。该窗口还允许通过添加条目和删除条目按钮添加和删除信息。

![cmake-gui_6.png](./image/cmake-gui_6.png)

![cmake-gui_7.png](./image/cmake-gui_7.png)

这个隐藏的工具可以使用 CMake 的 regex 引擎调试正则表达式，其位于“工具”菜单中，名称为 Regular Expressions Explorer…，使用起来非常简单:

1. 在正则表达式字段中输入表达式。
   该工具将检查表达式是否有效。若有效，屏幕上的有效文本将是绿色的。若 CMake 的 regex 引擎不喜欢你给出的表达式，它会变成红色。
2. 在 Input Text 字段中输入测试字符串。正则表达式将与此文本匹配。
3. 若有匹配，窗口上的 match 将从红色变为绿色。匹配的字符串将在完全匹配字段中输出。
4. 匹配时，捕获组将被分配给匹配 1、匹配 2 和匹配 N(有的话)。

## 2.3 Visual Studio、 Visual Studio Code 和 Qt Creator 中使用 CMake

作为软件开发中的常用工具， CMake 可以与各种 IDE 和源代码编辑器集成。使用 IDE 或编辑器的同时，这样的集成对用户来说可能会更方便。下面将介绍 CMake 如何与一些主流 IDE 和编辑器的集成。

**Visual Studio**

与其他流行的 IDE 不同， Visual Studio 直到 2017 年才原生支持 CMake。那一年，微软决定引入了对处理 CMake 项目的内置支持，并发布了 Visual Studio 2017。从那时起，内置支持 CMake 就成为了 Visual Studio IDE 的一个特性。开始前，请安装 Visual Studio 2017 或更高版本，对于老版本的 Visual Studio 没有这个特性。我们的例子中，将使用 Visual Studio 2022 社区版。

![VisualStudio_1.png](./image/VisualStudio_1.png)

在“创建新项目”窗口上，双击项目模板列表中的 CMake project。可以使用位于列表顶部的搜
索栏来筛选项目模板:

![VisualStudio_2.png](./image/VisualStudio_2.png)

单击 Next 之后，将出现项目配置窗口。可以给 CMake 项目起一个名字，并选择将新项目放在哪里。示例中，将使用默认项目名称 CMakeProject1。

![VisualStudio_3.png](./image/VisualStudio_3.png)

填写详细信息后，单击 Create 创建新的 CMake 项目。生成的项目将包含一个 CMakeLists.txt 文件，一个 C++ 源文件和一个 C++ 头文件，以所选的项目名称命名。新建的项目布局如下图所示:

![VisualStudio_4.png](./image/VisualStudio_4.png)

- 打开已有的 CMake 项目

打开现有的 CMake 项目， File | Open | CMake... 并选择待打开项目的顶层 CMakeLists.txt 文件。下图显示了“打开”菜单的样子:

![VisualStudio_5.png](./image/VisualStudio_5.png)

- 配置和构建 CMake 项目

要在 Visual Studio 中构建 CMake 项目，请先转到 Project | Configure。这将调用 CMake 配置步骤并生成所需的构建系统文件。配置完成后，单击 Build | Build All 以生成项目。也可以通过 F7 快捷键来触发 Build All。注意，当保存 CMakeLists.txt 文件时， Visual Studio 将自动重新配置，该文件是项目的一部分。

- 使用 CMake 目标执行通用操作

Visual Studio 使用启动目标的概念来执行目标所需的操作，比如构建、调试和启动。要将 CMake目标设置为启动目标，请使用工具栏上的“选择启动目标”下拉框。 Visual Studio 将自动在配置中使用 CMake 目标填充这个下拉框。

![VisualStudio_6.png](./image/VisualStudio_6.png)

设置启动目标后，可以使用诸如调试、构建或启动等操作:
- 要调试，首先单击“Debug | Startup Target”，然后单击“Debug | Start Debugging”或使用 F5 快
捷键。
- 不调试的情况下启动，单击“Start without debug”或使用 Ctrl + F5 快捷键。
- 要构建，单击 Build，或单击 Build | Build <target>，或使用 Ctrl + B 键盘快捷键。


