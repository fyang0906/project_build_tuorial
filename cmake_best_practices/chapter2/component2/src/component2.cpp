/**
 * component-2 implementation
*/

#include <framework/components/component2.hpp>

#include <cmath>
#include <iostream>

namespace framework {
namespace components {

component2::component2() {
    std::cout << "component2 created" << std::endl;
}

component2::~component2() {
    std::cout << "component2 destroyed" << std::endl;
}

bool component2::do_stuff() const { return true; }

int component2::do_other_stuff(int param) { return std::sqrt(param) * 2; }
} // namespace components
} // namespace framework