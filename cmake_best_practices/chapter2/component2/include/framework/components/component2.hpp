/**
 * component-2 interface description
*/

#pragma once

#include <framework/components/component_interface.hpp>

namespace framework {
namespace components {

/**
 * @brief component 1 interface
*/
class component2 : public component_interface {
public:

    /**
     * @brief Construct a new component 2 object
    */
    component2();

    /**
     * @brief Destroy the component 2 object
    */
    virtual ~component2() override;

    /**
     * @brief do_stuff
     * 
     * @return true on secess, false on failure
    */
    virtual bool do_stuff() const override;

    /**
     * @brief do_other_stuff
     * 
     * @param param Argument
     * 
     * @return int stuffs completed
    */
    virtual int do_other_stuff(int param) override;
}; // class component2

} // namespace components
} // namespace framework
