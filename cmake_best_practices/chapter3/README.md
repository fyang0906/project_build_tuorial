# 3. 创建 CMake 项目

## 3.1. 创建项目

虽然 CMake 可以用于任何文件结构，但有一些关于如何组织文件的良好实践。

```shell

├── CMakeLists.txt
├── builld
├── include/project
└── src
```

最小的项目结构中有三个文件夹和一个文件。

- **build:** 放置构建文件和二进制文件的文件夹。
- **include/project_name:** 此文件夹包含从项目外部公开访问的所有头文件，包含 `<project_name/somefile.h>` 使它更容易看出头文件来自哪个库。
- **src:** 此文件夹包含所有私有的源文件和头文件。
- **CMakeLists.txt:** 这是主 CMake 文件。

构建文件夹可以放置在任何地方，放在项目根目录最方便，但强烈建议不要选择任何非空文件夹作为构建文件夹。特别是将构建好的文件放入 `include` 或 `src` 中，这是一种糟糕的实践。其他文件夹，如 `test` 或 `doc`，在组织测试项目和文档页面时就很方便。

**嵌套项目**

将项目相互嵌套时，每个项目都应该映射上面的文件结构， 并且应该编写每个 `CMakeLists.txt` 以便子项目可以独立构建。每个子项目的 `CMakeLists.txt` 文件应该指定 `cmake_minimum_required`，以及可选的项目定义。

```shell
├── CMakeLists.txt
├── build
├── include/project_name
├── src
└── subproject
    ├── CMakeLists.txt
    ├── include
    │ └── subproject
    └── src
```

文件夹结构在子项目文件夹中重复。坚持这样的文件夹结构，并使子项目能够独立构建，这样就更容易移植项目。其还允许只构建项目的一部分，这对于构建时间可能相当长的大型项目非常有用。

## 3.2. 创建“hello world”可执行文件

首先，从一个简单的 hello world C++ 程序创建一个简单的可执行文件。下面的 C++ 程序将打印出 Welcome to CMake Best Practices:

```C++
#include <iostream>

int main(int, char **)
{
    std::cout << "Welcome to CMake Best Practices\n";
    return 0;
}
```

```CMake
# CMakeLists creating a simple executable

cmake_minimum_required(VERSION 3.21)

project(
    hello_world_standalone
    VERSION 1.0
    DESCRIPTION "A simple C++ project"
    HOMEPAGE_URL https://gitee.com/fyang0906/cmake-tutorial/tree/master/CMake-Best-Practices/chapter3
    LANGUAGES CXX
)

# Create a target to build an executable
add_executable(hello_world_standalone)

# Add source files to 'hello_world' target
target_sources(hello_world_standalone PRIVATE src/main.cpp)
```

第一行， `cmake_minimum_required(VERSION 3.21)`，期望看到的 CMake 的版本，以及 CMake 将启用哪些特性。将 `cmake_minimum_required` 指令放在每个 CMakeLists.txt 文件的顶部是一个很好的做法。

使用 `project()` 指令设置项目。第一个参数是项目的名称——我们的例子中为 “hello_world_standalone”。接下来，版本设置为 1.0。下面是一个简短的描述和主页的 URL。最后， `LANGUAGES CXX` 属性指定正在构建一个 C++ 项目。除了项目名称之外，所有参数都可选。

调用 `add_executable(hello_world_standalone)` 指令，会创建一个名为 `hello_world_standalone` 的目标。这也将是可执行的文件名。

现在已经创建了目标，使用 `target_sources` 完成了向目标添加 C++ 源文件。 `hello_world_standalone` 是目标名，在 `add_executable` 中指定。 `PRIVATE` 定义源仅用于构建此目标，而不用于依赖的目标。在范围说明符之后，有一个相对于当前 CMakeLists.txt 文件路径的源文件列表。如果需要，当前处理的 CMakeLists.txt 文件的位置可以通过 `CMAKE_CURRENT_SOURCE_DIR` 得到。

源码可以直接添加到 `add_executable`，也可以单独使用 `target_sources`，将它们与 `target_sources` 一起添加。通过使用 `PRIVATE`、 `PUBLIC` 或 `INTERFACE`，可以显式地定义在何处使用源码。但是，指定 `PRIVATE` 以外的内容只对库目标有意义。

经常看到的一种常见模式是用项目名称来命名项目的可执行文件:

```CMake
project(hello_world
...
)
add_executable(${PROJECT_NAME})
```

乍一看这似乎很方便，但不推荐这样做。项目名称和目标具有不同的语义含义，因此应该将其独立对待，因此应该避免使用  `PROJECT_NAME` 作为目标的名称。

## 3.3. 创建库

创建库的工作方式与创建可执行文件的方式类似，但因为库目标通常是由其他目标使用的，要么在同一个项目中，要么由其他项目使用。因为库通常有一个内部和公开可见的 API，所以在向项目添加文件时必须考虑到这一点。

下面是一个简单的库项目的 CMakeLists.txt :

```CMake
cmake_minimum_required(VERSION 3.21)

project(
    ch3.hello_lib
    VERSION 1.0
    DESCRIPTION
        "A sample C++ project to demonstrate creating executables and libraries in CMake"
    LANGUAGES CXX
)

add_library(hello)

target_sources(
    hello
    PRIVATE src/hello.cpp src/internal.cpp
)

target_compile_features(
    hello
    PUBLIC cxx_std_17
)

target_include_directories(
    hello
    PRIVATE src/hello
    PUBLIC include
)
```

同样，该文件以设置 `cmake_minimum_required` 和项目信息开始。接下来，使用 `add_library` 创建库的目标——本例中，库的类型没有确定。可以传递 `STATIC` 或 `SHARED` 来显式确定库的类型，这里可以省略设置该类型，我们允许库的使用者选择如何构建和链接。通常，静态库很容易处理。

若省略了库的类型，则 `BUILD_SHARED_LIBS` 将决定库是默认构建为动态库还是静态库。这个变量不应该在项目的 CMake 文件中设置，应该由构建者传递。

使用 `target_sources` 为库添加源文件。第一个参数是目标名称，后面跟 `PRIVATE`、 `PUBLIC` 或 `INTERFACE` 关键字分隔相应源文件。实践中，源文件使用 `PRIVATE` 添加， `PRIVATE` 和 `PUBLIC` 关键字指定在何处使用源代码进行编译。 `PRIVATE` 指定的源文件将只在目标 `hello` 中使用。若使用 `PUBLIC`，那么源文件也会将附加到 `hello` 和依赖 `hello` 的目标上，这通常不是我们想要的结果。 `INTERFACE` 关键字说明源文件不会添加到 `hello` 目标中，而是会添加到依赖到 `hello` 的目标上。通常，为目标指定为 `PRIVATE` 的内容都可以视为构建需求。最后，包含目录使用 `target_include_directories` 设置。该指令指定的文件夹内的所有文件都可以使用 `#include <file.hpp>`(带尖括号) 来访问。

`PRIVATE` 包含不会包含目标属性中的路径，也就是 `INTERFACE_INCLUDE_DIRECTORIES`。当目标依赖于该库时， CMake 将读取该属性以确定哪些包含目录可见。

由于标准库的 C++ 代码使用了与现代版本 C++ 绑定的特性，如 C++11/14/17/20 或 C++23，必须设置 `cxx_std_17` 属性。用于编译库本身并需要其接口，因此将其设置为 `PUBLIC`。只有在头文件包含需要特定标准的代码时，才需要将其设置为 `PUBLIC` 或 `INTERFACE`。若只有内部代码依赖于某个标准，则首选将其设置为 `PRIVATE`。通常，尽量将公共 C++ 标准设置为最低的可用标准，也可以只启用现代 C++ 标准的某些特性。

完整的可用编译特性列表可参阅 https://cmake.org/cmake/help/latest/prop_gbl/CMAKE_CXX_KNOWN_FEATURES.html。

### 3.3.1. 命名库

当使用 `add_library(<name>)` 创建库时，库名称在项目中必须全局唯一。默认情况下，库的实际文件名是根据平台上的约定构造的，例如 `lib<name>`。在 Linux 上为 `<name>.lib`，在 Windows上为 `< 名称 >.dll`。通过设置目标的 `OUTPUT_NAME` 属性，可以更改文件的名称。可以在下面的例子中看到，输出文件的名称已经从 `ch3_hello` 改为 `hello`:

```CMake
add_library(ch3_hello)


set_target_properties(
    ch3_hello
    PROPERTIES OUTPUT_NAME hello
)
```

避免使用 `lib` 前缀或后缀的库名称，因为 CMake 可能会在文件名后面或前面添加适当的字符串。当然，这取决于平台。

动态库的常用命名约定是在文件名中添加版本以指定构建版本和 API 版本，通过指定 `VERSION` 和 `SOVERSION` 属性， CMake 将在构建和安装库时创建必要的文件名和符号链接:

```CMake
set_target_properties(
    hello
    PROPERTIES VERSION ${PROJECT_VERSION}  # Contains 1.2.3
               SOVERSION ${PROJECT_VERSION_MAJOR} # Contains only 1
)
```

> Linux 上， `libch3_hello_shared.so.1.0.0` 的文件名带有来自 `libch3_hello_shared` 的符号链接。所以，`libch3_hello_shared.so.1` 指向实际的库文件。

![lib.so_1.png](image/lib.so_1.png)


项目中经常看到的另一种约定，是为各种构建配置的文件名添加不同的后缀。 CMake 通过设置 `CMAKE_<CONFIG>_POSTFIX` 全局变量或添加 `<CONFIG>_POSTFIX` 属性来处理这个问题。若设置了此变量，后缀将自动添加到非可执行目标。与大多数全局变量一样，应该通过命令行传递给 CMake，或者作为预置，而不是硬编码在 CMakeLists.txt 文件中。

- 设置 `CMAKE_<CONFIG>_POSTFIX` 全局变量

```CMake
# set the postfix "d" for the resulting .os or .dll files when building the library in debug mode
set(CMAKE_DEBUG_POSTFIX
    d
)
```
- 使用 CMake 构建 Debug 版本库文件 `libch3_hello_sharedd.so`

```shell
$ cmake -DCMAKE_BUILD_TYPE:STRING=Debug -S . -B build

...

$ cmake --build build/
```

![lib.so_2.png](./image/lib.so_2.png)

调试库的后缀也可以显式地设置为单个目标，如下面的例子所示:

```CMake
set_target_properties(
    ch3_hello_shared
    PROPERTIES DEBUG_POSTFIX d
)
```

这将使库文件和符号链接命名为 `libch3_hello_sharedd`。由于在 CMake 中链接库是针对目标而不是文件名进行的，因此会自动选择正确的文件名。然而，链接动态库时要注意的是符号可见性。

### 3.3.2. 动态库中的符号可见性

要链接到动态库，链接器必须知道哪些符号可以从库外部使用。这些符号可以是类、函数、类型等，使它们可见的过程称为导出。

指定符号可见性时，编译器有不同的方式和默认行为，这使得以独立于平台的方式指定符号可见性有点麻烦。从默认的编译器可见性开始; gcc 和 clang 假设所有的符号都是可见的，而 Visual Studio 编译器默认情况下会隐藏所有的符号，除非显式导出。设置 `CMAKE_WINDOWS_EXPORT_ALL_SYMBOLS`，可以改变 MSVC 的默认行为，这是一种暴力的解决方法，只有当库的所有符号都应该导出时才能使用。

虽然将所有的符号设置为公共可见是确保链接容易的一种简单方法，但也有一些缺点：

- 通过导出所有内容，就无法阻止依赖目标使用内部代码。
- 外部代码使用每个符号，链接器不能丢弃死代码，因此库体积往往会膨胀。若库中包含模板，则尤其如此，因为模板往往会大幅增加符号的数量。
- 由于导出了每个符号，关于哪些应该隐藏或内部符号的唯一线索必须来自文档。
- 暴露库的内部符号会暴露本应隐藏的东西。

> - 所有符号可见
>
> 将动态库中的所有符号设置为可见时要小心，特别是在关心安全问题或二进制文件的大小很重要时。

**更改默认可见性**

要更改符号的默认可见性，请将 `<LANG>_VISIBILITY_PRESET` 属性设置为 `HIDDEN`。此属性可以全局设置，也可以针对单个库目标设置。 `<LANG>` 会替换为编写库的语言，例如： `CXX` 替换为 C++， C 替换为 C。若所有要导出的符号都是隐藏符号，必须在代码中特别标记。最常见的方法是指定一个预处理器定义来确定一个符号是否可见:

```C++
class HELLO_EXPORT Hello {
    ...
};
```

`HELLO_EXPORT` 将包含这样的信息: 当编译库时，该符号是否导出，或者当对库进行链接时，它是否应该导入。 GCC 和 Clang 使用 `__attribute__(…)` 关键字来确定此行为，而在 Windows 上使用 `_declspec(…)`。编写以跨平台方式处理此问题的头文件并不是一项轻松的任务，特别是若还要考虑库可能构建为静态库和对象库。幸运的是， CMake 提供了 `generate_export_header` 宏，由
`GenerateExportHeader` 模块导入。

下面的例子中， `hello` 库的符号默认设置为隐藏。然后，通过使用 `generate_export_header` 宏再次单独启用。另外，本例将 `VISIBILITY_INLINES_HIDDEN` 属性设置为 `TRUE`，通过隐藏内联类成员函数进一步减少导出的符号表。并不严格要求设置内联的可见性，但通常在设置了默认可见性后才会这样做:

```CMake
add_library(hello SHARED)

set_property(TARGET hello PROPERTY CXX_VISIBILITY_PRESET "hidden")
set_property(TARGET hello PROPERTY VISIBILITY_INLINES_HIDDEN TRUE)

include(GenerateExportHeader)

generate_export_header(hello EXPORT_FILE_NAME export/hello/ export_hello.hpp)
target_include_directories(hello PUBLIC "${CMAKE_CURRENT_BINARY_DIR}/export")
```

`generate_export_header` 的调用在 `CMAKE_CURRENT_BINARY_DIR/export/hello` 目录中创建了一个名为 `export_hello.hpp` 的文件，该文件可以包含在库的文件中。将这些生成的文件放在构建目录的子文件夹中是一个好做法，这样只将目录的一部分添加到包含路径中。生成文件的 `include` 结构应该与库的其他部分的 `include` 结构匹配。在这个例子中，通过 `#include <hello a_public_header.h>` 来包含所有公共头文件，那么导出头文件也应该放在名为 `hello` 的文件夹中。生成的文件也必须添加到安装说明中。此外，要创建导出文件，必须将用于导出符号的必要编译器标志设置为目标。

因为生成的头文件必须包含在声明要导出的类、函数和类型的文件中，所以 `CMAKE_CURRENT_BINARY_DIR/export/` 添加到 `target_include_directories` 中。 注意，必须是 `PUBLIC`，这样依赖库才可以找到该文件。

关于设置符号可见性的其他信息，可以查阅官方 CMake 文档 https://cmake.org/cmake/help/latest/module/GenerateExportHeader.html。


### 3.3.3 接口或纯头文件库

纯头文件的库有点特殊，因为不需要编译; 相反，可以导出它们的头文件，以便直接包含在其他库中。大多数情况下，头文件库的工作方式与普通库类似，但是头文件使用 `INTERFACE`，而非 `PUBLIC`。

由于仅包含头文件的库不需要编译，因此不会向目标添加源文件。下面的代码创建了一个小的纯头文件库:

```CMake
cmake_minimum_required(VERSION 3.21)

project(
    ch3_hello_header_only
    VERSION 1.0
    DESCRIPTION "chapter 3 header only example"
    LANGUAGES CXX
)

# Create a library target
add_library(ch3_hello_header_only INTERFACE)

# expose the include directories
target_include_directories(ch3_hello_header_only INTERFACE include/)

# expose the minium C++ standard needed to compile this library
target_compile_features(ch3_hello_header_only INTERFACE cxx_std_17)
```

> **注意：**
>
> CMake 3.19 版本之前， INTERFACE 库不能使用 target_sources。现在，纯头文件库可以不列出源文件

**对象库——仅供内部使用**

有时，可能想要分离代码，以便部分代码可以重用，而不需要创建完整的库。当想在可执行测试和单元测试中使用某些代码时，通常的做法是不需要重新编译所有代码两次。为此， CMake 提供了对象库，其中的源代码是编译的，但不进行归档或链接。通过 `add_library(MyLibrary object)` 创建对象库。

自 CMake 3.12 起，这些对象可以像普通库一样使用，只需将它们添加到 `target_link_libraries` 函数中。 3.12 版本之前，对象库需要添加生成器表达式，也就是 `$<TARGET_OBJECTS:MyLibrary>`。这将在生成构建系统期间扩展为一个对象列表。这种方式现在还可以用，但不推荐这样做，因为这很快就变得不可维护，特别是在一个项目中有多个对象库的情况下。

**何时使用对象库**

对象库可以在不公开模块的情况下加快代码的构建和模块化。

使用对象库，可以覆盖所有不同类型的库。编写和维护库本身很有趣，但除非将其集成到更大的项目中，否则它们什么也做不了。

## 3.4. 将它们结合在一起 -- 使用库

我们已经创建了三个不同的库 -- 一个要静态或动态库，一个接口或头文件库，以及一个预编译但没有链接的对象库。

所以， 可以把 `add_library` 放在同一个 CMakeLists.txt 文件中，或者使用 `add_subdirectory` 将其整合起来。两者都是有效的选项，并取决于项目的设置方式。

下面的例子中，假设在 `hello_lib`、 `hello_header_only` 和 `hello_object` 目录中已经用 CMakeLists.txt
文件定义了三个库。可以使用 `add_subdirectory` 包含这些库。这里，创建了一个名为 `chapter3` 的新目标，它是可执行文件。然后，将这些库用 `target_link_libraries` 添加到可执行文件中:

```CMake
# Add subdirectories with examples
add_subdirectory(hello_world_standalone)
add_subdirectory(hello_shared_lib)
add_subdirectory(hello_static_lib)
add_subdirectory(hello_header_only)
add_subdirectory(hello_object_lib)

# add an example executable
add_executable(chapter3)

# add source to the example executable
target_sources(chapter3 PRIVATE src/main.cpp)

# link libraries "hello" and "hello_header_only" to the example executable 
# the libraries are described in the subdirectories
target_link_libraries(chapter3 PRIVATE ch3_hello_header_only ch3_hello_shared 
                      ch3_hello_object)
```

`target_link_libraries` 的目标也可以是另一个库。同样，库的链接说明符，可以是以下任意一个:
- **PRIVATE:** 用于链接库，但不是公共接口的一部分。只有在构建目标时才需要链接库。
- **INTERFACE:** 没有链接到库，但是公共接口的一部分。当在其他地方使用目标时，链接库是必需的。这通常仅限头文件库时使用。
- **PUBLIC:** 链接到库，是公共接口的一部分。因此，该库既是构建依赖项，也是使用依赖项。


### 3.4.1 设置编译器和链接器选项

C++ 编译器有很多选项来设置一些常见的标志，从外部设置预处理器定义也是一种常见的做法。 CMake 中，这些是使用 `target_compile_options` 传递，使用 `target_link_options` 更改链接器行为，但编译器和链接器可能有不同的设置标志的方法。例如，在 GCC 和 Clang 中，选项用减号 (`-`) 传递，而 Microsoft 编译器将斜杠 (`/`) 作为选项的前缀。但是通过生成器表达式，可以很容易地在 CMake 中处理这个问题:

```CMake
target_compile_options(
    hello
    PRIVATE $<$<CXX_COMPILER_ID:MSVC>:/SomeOption>
    $<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:-someOption>
)
```

`$<$<CXX_COMPILER_ID:MSVC>:/SomeOption>` 是一个嵌套的生成器表达式，由内而外求值。生成器表达式在生成阶段进 行计算。首先，当 C++ 编译器等于 MSVC 时，`$<CXX_COMPILER_ID:MSVC>` 为 `true`。若是这种情况，那么外部表达式将返回 `/SomeOption`，然后传递给编译器。若内部表达式的计算结果为 `false`，则不传递。

`$<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:-someOption>` 的工作原理类似，但不是只检查单个值，而是传递一个包含 GNU， Clang， AppleClang 的列表。若 `CXX_COMPILER_ID` 匹配其中任何一个，内部表达式计算为 `true`， `-someOption` 会传递给编译器。

将编译器或链接器选项传递为 `PRIVATE`，将其标记为与库接口不需要的此目标的构建需求。若使用 `PUBLIC`，那么编译选项也成为构建需求，所有依赖于原始目标的目标将使用相同的编译选项。将编译器选项暴露给依赖的目标是需要谨慎做的事情。若编译器选项只用于使用目标而不用于构建目标，则可以使用关键字 `INTERFACE`。在构建纯头文件库时，这是最常见的情况。

编译器选项的特殊情况是编译定义，其会传递给底层程序。这通过 `target_compile_definitions` 进行传递。

**调试编译器选项**

要查看所有编译选项，可以查看生成的构建文件，例如 Makefiles 或 Visual Studio 项目。更方便的方法是让 CMake 将所有编译命令导出为 JSON。

通过使用 `CMAKE_EXPORT_COMPILE_COMMANDS`， 将生成一个名为 `compile_commands.json` 的文件，其包含用于编译的完整命令。启用此选项并运行 CMake 将产生如下结果:

### 3.4.2. 库别名

库别名是在不创建新的构建目标的情况下引用库的一种方法，有时称为命名空间。常见的模式是为从项目中安装的每个库以 `MyProject::library` 的形式创建一个库别名，可以用于对多个目标进行语义分组。有助于避免命名方面的冲突，特别是当项目包含公共目标时，比如名为 `utils` 的库、 `helper` 和类似的库。在相同的命名空间下收集相同项目的所有目标是一个很好的实践。在链接来自其他项目的库时，包含名称空间可以避免意外包含错误的库。本例中所有的库目标都将使用一个命名空间
作为别名来分组它们，以便它们可以使用命名空间引用:

```CMake
add_library(Chapter3::hello ALIAS hello)

...

target_link_libraries(SomeLibrary PRIVATE Chapter3::hello)
```

除了帮助确定目标的来源， CMake 使用命名空间来识别导入的目标，并提供更好的诊断消息。

**使用命名空间**

作为一种良好的实践，始终使用名称空间对目标进行别名，并使用”命名空间::前缀”进行引用。

命名空间是组织构建构件的一种很好的方式。但有时这还不够，我们希望看到更大的视野，了解“什么正在使用什么”，以及哪个工件依赖于哪个库。 CMake 可以帮助创建提供这样深刻见解的依赖图。

