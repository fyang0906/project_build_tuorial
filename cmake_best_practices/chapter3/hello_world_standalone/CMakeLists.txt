# CMakeLists creating a simple executable

cmake_minimum_required(VERSION 3.21)

project(
    hello_world_standalone
    VERSION 1.0
    DESCRIPTION "A simple C++ project"
    HOMEPAGE_URL https://gitee.com/fyang0906/cmake-tutorial/tree/master/CMake-Best-Practices/chapter3
    LANGUAGES CXX
)

# Create a target to build an executable
add_executable(hello_world_standalone)

# Add source files to 'hello_world' target
target_sources(hello_world_standalone PRIVATE src/main.cpp)
