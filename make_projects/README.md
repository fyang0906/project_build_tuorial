
# Makefile Project Practice

**Ubuntu开发环境**

- 安装 gcc 和 g++

```shell
apt install g++
```
- 调试工具

```shell
apt install gbd
```
- make

```shell
apt install make
```

## 1. 动手编写第一个 makefile 编译 C++ 多文件项目

**最简单的 Makefile：[1-first_make_0](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/1-first_make_0)**

- 目录结构

```shell
.
├── first_make.cpp
├── makefile
├── xdata.cpp
└── xdata.h
```

> *1-first_make_0/xdata.h*

```C++
// xdata.h

#ifndef XDATA_H
#define XDATA_H

class XData
{
public:
    XData();
};

#endif
```

> *1-first_make_0/xdata.cpp*

```C++
#include <iostream>
#include "xdata.h"

using namespace std;

XData::XData()
{
    cout << "Create XData" << endl;
}
```

> *1-first_make_0/first_make.cpp*

```C++
#include <iostream>
#include <thread>
#include "xdata.h"

using namespace std;

void ThreadMain()
{
    cout << "Thread Main" << endl;
}

int main(int argc, char *argv[])
{
    thread th(ThreadMain);

    cout << "test make" << endl;

    th.join();

    XData d;

    return 0;
}
```

> *1-first_make_0/makefile*

```makefile
first_make.out : first_make.cpp xdata.cpp
	g++ first_make.cpp xdata.cpp -o first_make.out -lpthread
```

> **注意：** makefile 文件缩进格式为 `TAB`，不是空格


## 2. g++ 分步编译从源码到可执行程序

**GCC（g++）**

- 预处理（Preprocessing）
- 编译（Compilation）
- 汇编（Assembly）
- 链接（Linking）

**gcc [选项] <文件名>**

- 基本使用格式
- 常用选项及含义

| 选项 | 含义 |
| -   | -    |
| `-o file` | 将经过 gcc 处理过的结果存为文件（file），这个结果文件可以是预处理文件、汇编文件、目标文件或者最终的可执行文件。假设被处理的源文件为 `source.suffix`，如果这个选项被省略了，那么生成的可执行文件默认名称为 `a.out`；目标文件默认名为 `source.o`；汇编文件默认名为 `source.s`；生成的预处理文件则发送到标准输出设备 |
| `-c` | 仅对源文件进行编译，不链接生成可执行文件。在对源文件进行查错时，或只需产生目标文件时可以使用该选项。 |
| `-g [gdb]` | 在可执行文件中加入调试信息，方便进行程序的调试。如果使用中括号中的选项，表示加入 gdb 扩展的调试信息，方便使用gdb来进行调试 |
| `-O [0、1、2、3]` | 对生成的代码使用优化，中括号中的部分为优化级别，缺省的情况为2级优化，0为不进行优化。注意，采用更高级的优化并不一定得到效率更高的代码。 |
| `-Dname[=definition]` | 将名为 name 的宏定义为 definition，如果中括号中的部分缺省，则宏被定义为1 |
| `-Idir` | 在编译源程序时增加一个搜索头文件的额外目录 -- `dir`，即 `include` 增加一个搜索的额外目录 |
| `-Ldir` | 在编译源文件时增加一个搜索库文件的额外目录 -- `dir` |
| `-llibrary` | 在编译链接文件时增加一个额外的库，库名为 `liblibrary.so` |
| `-w` | 禁止所有警告 |
| `-Wwarning` | 允许产生 warning 类型的警告，warning 可以是：main、unused 等很多取值，最常用是 `-Wall`，表示产生所有警告。如果 warning 取值为 error，其含义是将所有警告作为错误（error），即出现警告就停止编译。 |

**gcc 文件扩展名规范**

| 扩展名 | 类型 | 可进行的操作方式 |
| -     | -   | -             |
| .c    | c语言原程序 | 预处理、编译、汇编、链接  |
| .c, .cc, .cp, .cpp, .c++, .cxx | c++ 语言源程序  | 预处理、编译、汇编、链接 |
| .i    | 预处理后的c语言源程序      | 编译、汇编、链接 |
| .ii   | 预处理后的c++语言源程序    | 编译、汇编、链接 |
| .s    | 预处理后的汇编程序         | 汇编、链接 |
| .S    | 未预处理的汇编程序         | 预处理、汇编、链接 |
| .h    | 头文件                   | 不进行任何操作   |
| .c    | 目标文件                 | 链接      |

**g++ 分步编译：[2-test_gcc](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/2-test_gcc)**

- 目录结构

```shell
.
├── test
├── test.h
├── test.ii
├── test.s
└── test_gcc.cpp
```

1. 预处理：*2-test_gcc/test.ii*

```shell
g++ -E test.cpp > test.ii
```

1. 编译：*2-test_gcc/test.s*

```shell
g++ -S test.ii
```

3. 汇编：*test.o*

```shell
g++ -c test.s
```

4. 链接：*test*

```shell
g++ test.o -o test
```

5. 运行

```shell
• ./test
```

## 3. makefile 运行流程分析并使用变量改写项目

![Makefile_Workflow_1.png](./image/Makefile_Workflow_1.png)

**Makefile 文件主要包含了5部分内容：**

1. 显示规则：说明了如何生成一个或多个目标文件。由 Makefile 文件的创建者指出，包括要生成的文件、文件的依赖文件、生成的命令
2. 隐式规则：由于 make 有自动推导的功能，所以隐式的规则可以比较粗糙地简略书写 Makefile 文件，这是 Make 所支持的
3. 变量定义：在 Makefile 文件中要定义一系列的变量，变量一般都是字符串，这与C语言中的宏有些类似。当 Makefile 文件执行时，其中的变量都会扩展到相应的引用位置上
4. 文件指示：其包括3个部分，一个是在 Makefile 文件中引用另一个 Makefile 文件；另一个是指根据某些情况指定 Makefile 文件中的有效部分；还有就是定义一个多行的命令
5. 注释：Makefile 文件中只有行注释，其注释用 `#` 字符。如果要在 Makefile 文件中使用 `#` 字符，可以用反斜杠进行转义，如：`\#`

**Makefile 中常见预定义变量**

| 命令格式 | 含义 |
| -      | -                                |
| AR     | 库文件维护程序的名称，默认值为 `ar`   |
| AS     | 汇编程序的名称，默认值为 `as`        |
| CC     | C 编译器的名称，默认值为 `cc`        |
| CPP    | C 与编译器的名称，默认值为 `$(CC)-E` |
| CXX    | C++ 编译器的名称，默认值为 `g++`     |
| FC     | FORTRAN 编译器的名称，默认值为 `f77` |
| RM     | 文件删除程序的名称，默认值为 `rm -rf` |
| ARFLAGS | 库文件维护程序的选项，无默认值       |
| ASFLAGS | 汇编程序的选项，无默认值            |
| CFLAGS  | C 编译器的选项，无默认值            |
| CPPFLAGS | C 预编译器的选项，无默认值         |
| CXXFLAGS | C++ 编译器的选项，无默认值         |
| FFLAGS   | FORTRAN 编译器的选项，无默认值     |

**Makefile 变量的使用**

| 命令格式 | 含义 |
| -       | -   |
| `$*`    | 不包含扩展名的目标文件名称                                     |
| `$+`    | 所有的依赖文件，以空格分开，并以出现的先后顺序，可能包含重复的依赖文件 |
| `$<`    | 第一个依赖文件的名称                                          |
| `$?`    | 所有时间戳比目标文件晚的依赖文件，并以空格分开                     |
| `$@`    | 目标文件的完整名称                                            |
| `$^`    | 所有不重复的依赖文件，以空格分开                                |
| `$%`    | 如果目标是归档成员，则该变量表示目标的归档成员名称                  |

> *[3-first_make_1](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/3-first_make_1)*

- 目录结构

```shell
.
├── first_make.cpp
├── makefile
├── xdata.cpp
└── xdata.h
```

> *3-first_make_1/makefile*

```Makefile
# first_make
# $^ 依赖 不重复
# $@ 目标
# @ 不显示命令执行  - 失败不停止

TARGET = first_make.out
LIBS = -lpthread

$(TARGET): first_make.cpp xdata.cpp
	@#-@rm test
	@echo "begin build $(TARGET)"
	@$(CXX) $^ -o $@ $(LIBS)
	@echo "build success!"
```

- 编译：

```shell
☁  1-first_make_1 [master] ⚡  make
begin build first_make.out
build success!
```

- 运行：

```shell
☁  1-first_make_1 [master] ⚡  ./first_make.out 
Thread Main
test make
Create XData
```
## 4. makefile 自动推导目标代码配置和伪目标 `clean` 清理

**自动推导**

> *[4-first_make_2](https://gitee.com/fyang0906/project_build_tuorial/tree/master/makefile/src/4-first_make_2)*

- 目录结构

```shell
.
├── first_make.cpp
├── makefile
├── xdata.cpp
└── xdata.h
```

> *4-first_make_2/makefile*

```Makefile
# first_make
# $^ 依赖 不重复
# $@ 目标
# @ 不显示命令执行  - 失败不停止

TARGET = first_make.out
LIBS = -lpthread

$(TARGET): first_make.o xdata.o
	@#-@rm test
	@echo "begin build $(TARGET)"
	$(CXX) $^ -o $@ $(LIBS)
	@echo "build success!"
```

- 编译：

```shell
☁  1-first_make_2 [master] ⚡  make        
g++    -c -o first_make.o first_make.cpp
g++    -c -o xdata.o xdata.cpp
begin build first_make.out
g++ first_make.o xdata.o -o first_make.out -lpthread
build success!
```

**自动推导可能会存在的问题：头文件的引用**

在 `1-first_make_2/first_make.cpp` 中引用 `2-test_gcc/test.h`

> *[2-test_gcc/test.h](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/2-test_gcc/test.h)*

```C++
// test_gcc

#define CONF_PATH "/usr/local/xcj"

```

> *[4-first_make_2/first_make.cpp](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/4-first_make_2/first_make.cpp)*

```C++
#include <iostream>
#include <thread>
#include "xdata.h"
#include "test.h"

using namespace std;

void ThreadMain()
{
    cout << "Thread Main" << endl;
}

int main(int argc, char *argv[])
{
    thread th(ThreadMain);

    cout << "test make" << endl;

    th.join();

    XData d;

    return 0;
}
```

- 编译：`make`

```shell
☁  1-first_make_2 [master] ⚡  make        
g++    -c -o first_make.o first_make.cpp
first_make.cpp:4:10: fatal error: test.h: No such file or directory
    4 | #include "test.h"
      |          ^~~~~~~~
compilation terminated.
make: *** [<builtin>: first_make.o] Error 1
```

如上所述，编译报错找不到 `test.h`，手动命令行指令指定头文件使用路径

```shell
☁  1-first_make_2 [master] ⚡  g++ -c -o first_make.o first_make.cpp -I ../2-test_gcc 
☁  1-first_make_2 [master] ⚡  ls
first_make.cpp  first_make.o  makefile  xdata.cpp  xdata.h
```

可以看到编译生成了 `first_make.o`

**修改 Makefile 增加头文件引用**

> *[4-first_make_3/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/4-first_make_3/makefile)*

```Makefile
# first_make
# $^ 依赖 不重复
# $@ 目标
# @ 不显示命令执行  - 失败不停止

TARGET = first_make.out
LIBS = -lpthread
OBJS = first_make.o xdata.o
CXXFLAGS = -I ../2-test_gcc

$(TARGET): $(OBJS)
	@#-@rm test
	@echo "begin build $(TARGET)"
	$(CXX) $^ -o $@ $(LIBS)
	@echo "build success!"
```

- 编译：`make`

```shell
☁  1-first_make_3 [master] ⚡  make         
g++ -I ../2-test_gcc   -c -o first_make.o first_make.cpp
g++ -I ../2-test_gcc   -c -o xdata.o xdata.cpp
begin build first_make.out
g++ first_make.o xdata.o -o first_make.out -lpthread
build success!
```

**clean 清理**

> *[4-first_make_4/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/4-first_make_4/makefile)*

```Makefile
# first_make
# $^ 依赖 不重复
# $@ 目标
# @ 不显示命令执行  - 失败不停止

TARGET = first_make.out
LIBS = -lpthread
OBJS = first_make.o xdata.o
CXXFLAGS = -I ../2-test_gcc

$(TARGET): $(OBJS)
	@#-@rm test
	@echo "begin build $(TARGET)"
	@$(CXX) $^ -o $@ $(LIBS)
	@echo "build success!"

clean:
	$(RM) $(OBJS) $(TARGET)
.PHONY: clean *clean
```

> **Tips：** `.PHONY` 伪目标，避免当前目录下存在目标同名的文件（如：目标 `clean` 命令的同名文件 clean）或在不同Makefile版本中存在的问题

- 编译：`make`

```shell
☁  1-first_make_4 [master] ⚡  make             
g++ -I ../2-test_gcc   -c -o first_make.o first_make.cpp
g++ -I ../2-test_gcc   -c -o xdata.o xdata.cpp
begin build first_make.out
build success!
```

- 清理：`make clean`

```shell
☁  1-first_make_4 [master] ⚡  make clean
rm -f first_make.o xdata.o first_make.out
```

## 5. 使用 makefile 编译动态链接库并编写测试项目

- `-fPIC` 编译选项
  - `-fPIC` if supported for the target machine, emit position-independent
    - code, suitable for dynamic linking, even if branches need large displacements. 产生位置无关代码（PIC），一般创建共享库时用到。在x86上，PIC的代码的符号引用都是通过ebx进行操作的。
  - `-shared`

- `g++ -shared -fPIC mylib.cpp -o libmylib.so`
- `g++ test.cpp -lmylib -L/root/cpp`

```shell
# !/bin/sh

LD_LIBRARY_PATH=../xthread
export LD_LIBRARY_PATH
./xserver
```

**项目示例： [5-makefile_demo](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/5-makefile_demo)**

- 目录结构

```shell
.
├── xserver
│   ├── makefile
│   ├── run.sh
│   └── xserver.cpp
└── xthread
    ├── makefile
    ├── xthread.cpp
    └── xthread.h
```

> *xthread/xthread.h*

```C++
#ifndef XTHREAD_H
#define XTHREAD_H

#include <thread>

class XThread
{
public:
    virtual void Start();
    virtual void Wait();

private:
    virtual void Main() = 0;
    std::thread th_;
};

#endif
```

> *xthread/xthread.cpp*

```C++
#include <iostream>
#include "xthread.h"

using namespace std;

void XThread::Start()
{
    cout << "Start Thread" << endl;
    th_ = std::thread(&XThread::Main, this);
}

void XThread::Wait()
{
    cout << "begin wait Thread" << endl;
    th_.join();
    cout << "end wait Thread" << endl;
}
```

> *xthread/makefile*

```Makefile
TARGET = libxthread.so
OBJS = xthread.o
LDFLAGS = -shared

$(TARGET) : $(OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译：`make`

```shell
g++    -c -o xthread.o xthread.cpp
g++ -shared xthread.o -o libxthread.so
/usr/bin/ld: xthread.o: warning: relocation against `_ZTVNSt6thread6_StateE@@GLIBCXX_3.4.22' in read-only section `.text._ZNSt6thread6_StateC2Ev[_ZNSt6thread6_StateC5Ev]'
/usr/bin/ld: xthread.o: relocation R_X86_64_PC32 against symbol `_ZSt4cout@@GLIBCXX_3.4' can not be used when making a shared object; recompile with -fPIC
/usr/bin/ld: final link failed: bad value
collect2: error: ld returned 1 exit status
make: *** [makefile:7: libxthread.so] Error 1
```

如上输出，链接失败，编译动态库的时候要加上 `-fPIC` 选项

> *xthread/makefile*

```Makefile
TARGET = libxthread.so
OBJS = xthread.o
LDFLAGS = -shared
CXXFLAGS = -fPIC

$(TARGET) : $(OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 清理：`make clean`

```shell
☁  xthread [master] ⚡  make clean 
rm -f libxthread.so xthread.o
```

- 编译：`make`

```shell
☁  xthread [master] ⚡  make      
g++ -fPIC   -c -o xthread.o xthread.cpp
g++ -shared xthread.o -o libxthread.so
```

- 使用 `ldd` 命令查看生成的 `libxthread.so` 

```shell
☁  xthread [master] ⚡  ldd libxthread.so 
        linux-vdso.so.1 (0x00007ffcbcb87000)
        libstdc++.so.6 => /lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f056ecdf000)
        libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f056ecbf000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f056ea96000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f056e9af000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f056ef1b000)
```

> **Tips：** 如上所述编译完成了动态库 `xxx.so` 文件，当我们对外发布动态库时，只需要发布 `xx.h` 和 `xx.so` 文件


**创建测试项目： [5-makefile_demo/xserver](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/5-makefile_demo/xserver)**

- 目录结构

```shell
.
├── makefile
├── run.sh
└── xserver.cpp
```

> *xserver/xserver.cpp*

```C++
#include <iostream>
#include "xthread.h"

using namespace std;

class XTask : public XThread
{
public:
    void Main() override
    {
        cout << "XTask main" << endl;
    }
};

int main(int argc, char *argv[])
{
    cout << "XServer" << endl;

    return 0;
}
```

> *xserver/makefile*

```Makefile
TARGET = xserver.out
OBJS = xserver.o

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make      
g++    -c -o xserver.o xserver.cpp
xserver.cpp:2:10: fatal error: xthread.h: No such file or directory
    2 | #include "xthread.h"
      |          ^~~~~~~~~~~
compilation terminated.
make: *** [<builtin>: xserver.o] Error 1
```

如上输出，编译时找不到 `xthread.h`，修改 makefile 添加头文件引用 `CXXFLAGS = -I../xthread`

> *xserver/makefile*

```Makefile
TARGET = xserver.out
OBJS = xserver.o
CXXFLAGS = -I../xthread

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 清理：`make clean`

```shell
☁  xserver [master] ⚡  make clean
rm -f xserver.out xserver.o
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make
g++ -I../xthread   -c -o xserver.o xserver.cpp
g++ xserver.o -o xserver.out
```

> **Tips：** 这里编译过了是因为，在 `xserver.cpp` 中只引用了 `xthread.h` 并对 `XThread` 类的成员函数 `Main()` 做了重载，但并未访问 `Main()`；修改 `xserver.cpp`，使用 `XTask` 类并访问其成员

> *xserver/xserver.cpp*

```C++
#include <iostream>
#include "xthread.h"

using namespace std;

class XTask : public XThread
{
public:
    void Main() override
    {
        cout << "XTask main" << endl;
    }
};

int main(int argc, char *argv[])
{
    cout << "XServer" << endl;
    XTask task;
    task.Start();
    task.Wait();

    return 0;
}
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make      
g++ -I../xthread   -c -o xserver.o xserver.cpp
g++ xserver.o -o xserver.out
/usr/bin/ld: xserver.o: warning: relocation against `_ZTV7XThread' in read-only section `.text._ZN7XThreadD2Ev[_ZN7XThreadD5Ev]'
/usr/bin/ld: xserver.o: in function `main':
xserver.cpp:(.text+0x62): undefined reference to `XThread::Start()'
/usr/bin/ld: xserver.cpp:(.text+0x6e): undefined reference to `XThread::Wait()'
/usr/bin/ld: xserver.o: in function `XThread::XThread()':
xserver.cpp:(.text._ZN7XThreadC2Ev[_ZN7XThreadC5Ev]+0x13): undefined reference to `vtable for XThread'
/usr/bin/ld: xserver.o: in function `XThread::~XThread()':
xserver.cpp:(.text._ZN7XThreadD2Ev[_ZN7XThreadD5Ev]+0x13): undefined reference to `vtable for XThread'
/usr/bin/ld: xserver.o:(.data.rel.ro._ZTV5XTask[_ZTV5XTask]+0x10): undefined reference to `XThread::Start()'
/usr/bin/ld: xserver.o:(.data.rel.ro._ZTV5XTask[_ZTV5XTask]+0x18): undefined reference to `XThread::Wait()'
/usr/bin/ld: xserver.o:(.data.rel.ro._ZTI5XTask[_ZTI5XTask]+0x10): undefined reference to `typeinfo for XThread'
/usr/bin/ld: warning: creating DT_TEXTREL in a PIE
collect2: error: ld returned 1 exit status
make: *** [makefile:8: xserver] Error 1
```

> 如上输出信息为链接错误，找不到 `XThread` 类的成员函数，修改 makefile 引用对应的库
> - `LDFLAGS = -L../xthread`
> - `LIBS = -lxthread -lpthread` -- `-l <库文件名>` 注意此时库文件名要去掉 `libxthread.so` 的， `lib` 和 `.os` 文件后缀（即：`-lxthread`）

> *xserver/makefile*

```Makefile
TARGET = xserver.out
OBJS = xserver.o
CXXFLAGS = -I../xthread
LDFLAGS = -L../xthread
LIBS = -lxthread -lpthread 

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LIBS)

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```


- 清理：`make clean`

```shell
☁  xserver [master] ⚡  make clean
rm -f xserver.out xserver.o
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make
g++ -I../xthread   -c -o xserver.o xserver.cpp
g++ xserver.o -o xserver.out -L../xthread -lxthread -lpthread 
```

- 执行：`./xserver.out`

```shell
☁  xserver [master] ⚡  ./xserver.out
./xserver: error while loading shared libraries: libxthread.so: cannot open shared object file: No such file or directory
```

如上输出，找不到对应的动态链接库， 因为 Linux 中程序运行时系统默认会在 `/lib` 和 `/usr/lib` 这两个目录下搜索需要的动态链接库，这里我们使用设置环境变量 `LD_LIBRARY_PATH` 的方式指定额外的动态库搜索路径，多个路径间用冒号分隔。

- 创建执行脚本：`xserver/run.sh`

```shell
# !/bin/sh

LD_LIBRARY_PATH=../xthread
export LD_LIBRARY_PATH
./xserver.out
```

- 修改 `run.sh` 文件的权限为可执行：`sudo chmod +x run.sh`
- 运行：`./run.sh`

```shell
☁  xserver [master] ⚡  ./run.sh 
XServer
Start Thread
begin wait Thread
XTask main
end wait Thread
```

## 6. 使用 makefile 编译静态库并通过 `ifeq` 语句实现静态库和动态库切换

**静态库**

- 静态库：`ar -crv libmylib.a mylib.o`
- `[c]`  - do not warn if the library had to be created 不显示创建
- `[v]`  - be verbose 显示过程
- `r`    - replace existing or insert new file(s) into the archive 创建静态库

**项目示例： [6-makefile_demo](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/6-makefile_demo)**

- 目录结构

```shell
.
├── xcom
│   ├── makefile
│   ├── xcom.cpp
│   └── xcom.h
├── xserver
│   ├── makefile
│   ├── run.sh
│   └── xserver.cpp
└── xthread
    ├── makefile
    ├── xthread.cpp
    └── xthread.h
```

> *xcom/xcom.h*

```C++
#ifndef XCOM_H
#define XCOM_H

class XCom
{
public:
    XCom();
};

#endif
```

> *xcom/xcom.cpp*

```C++
#include "xcom.h"
#include <iostream>

using namespace std;

XCom::XCom()
{
    cout << "Create XCom" << endl;
}
```

> *xcom/makefile*

```Makefile
TARGET = libxcom.a
OBJS = xcom.o
$(TARGET) : $(OBJS)
	$(AR) -cvr $@ $^

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译：`make`

```shell
☁  xcom [master] ⚡  make
g++    -c -o xcom.o xcom.cpp
ar -cvr libxcom.a xcom.o
a - xcom.o
```

可以看到在当前目录（xcom）下生成了 `libxcom.a`

**在 xserve 中使用 xcom 静态库**

> *xserver/xserver.cpp*

```C++
#include <iostream>
#include "xthread.h"
#include "xcom.h"

using namespace std;

class XTask : public XThread
{
public:
    void Main() override
    {
        cout << "XTask main" << endl;
    }
};

int main(int argc, char *argv[])
{
    cout << "XServer" << endl;
    XCom xcom;
    XTask task;
    task.Start();
    task.Wait();

    return 0;
}
```

> *xserver/makefile*

```Makefile
TARGET = xserver.out
OBJS = xserver.o
CXXFLAGS = -I../xthread
LDFLAGS = -L../xthread
LIBS = -lxthread -lpthread

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LIBS)

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make      
g++ -I../xthread   -c -o xserver.o xserver.cpp
xserver.cpp:3:10: fatal error: xcom.h: No such file or directory
    3 | #include "xcom.h"
      |          ^~~~~~~~
compilation terminated.
make: *** [<builtin>: xserver.o] Error 1
```

找不到 `xcom.h`，修改 makefile 增加 `xcom.h` 文件引用：`CXXFLAGS = -I../xthread -I../xcom`

> *xserver/makefile*

```Makefile
TARGET = xserver.out
OBJS = xserver.o
CXXFLAGS = -I../xthread -I../xcom
LDFLAGS = -L../xthread
LIBS = -lxthread -lpthread

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LIBS)

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make
g++ -I../xthread -I../xcom   -c -o xserver.o xserver.cpp
g++ xserver.o -o xserver.out -L../xthread -lxthread -lpthread
/usr/bin/ld: xserver.o: in function `main':
xserver.cpp:(.text+0x56): undefined reference to `XCom::XCom()'
collect2: error: ld returned 1 exit status
make: *** [makefile:8: xserver.out] Error 1
```

链接错误，找不到 `XCom::XCom()` 的定义，修改 makefile 增加 `libxcom.a` 库文件的引用：`LIBS = -lxthread -lpthread -lxcom`

> *xserver/makefile*

```Makefile
TARGET = xserver.out
OBJS = xserver.o
CXXFLAGS = -I../xthread -I../xcom
LDFLAGS = -L../xthread
LIBS = -lxthread -lpthread -lxcom

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LIBS)

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make
g++ xserver.o -o xserver.out -L../xthread -lxthread -lpthread -lxcom
/usr/bin/ld: cannot find -lxcom: No such file or directory
collect2: error: ld returned 1 exit status
make: *** [makefile:8: xserver.out] Error 1
```

链接时找不到 `xcom`， 修改 makefile 增加 `libxcom.a` 库文件链接路径：`LDFLAGS = -L../xthread -L../xcom`

> *xserver/makefile*

```makefile
TARGET = xserver.out
OBJS = xserver.o
CXXFLAGS = -I../xthread -I../xcom
LDFLAGS = -L../xthread -L../xcom
LIBS = -lxthread -lpthread -lxcom

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LIBS)

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译：`make`

```shell
☁  xserver [master] ⚡  make
g++ xserver.o -o xserver.out -L../xthread -L../xcom -lxthread -lpthread -lxcom
```

- 运行：`run.sh`

```shell
☁  xserver [master] ⚡  ./run.sh 
XServer
Create XCom
Start Thread
begin wait Thread
XTask main
end wait Thread
```

> **Tips：** 静态库 `libxcom.a` 会在编译时编译进可执行文件 `xserver.out` 中，所以在程序运行时不会动态加载

- **问题：** 如何将静态库换成动态库？

make 可以在命令中设置变量的值，例如使用 clang 编译 xcom ：`make CXX=clang`

```shell
☁  xcom [master] ⚡  make CXX=clang
clang    -c -o xcom.o xcom.cpp
ar -cvr libxcom.a xcom.o
a - xcom.o
```

- **示例：** `make STATIC=1` 时将 xcom 编译成静态库，`make STATIC=0` 时将 xcom 编译成动态库

> *xcom/makefile*

```makefile
TARGET = libxcom
OBJS = xcom.o

ifeq ($(STATIC), 1)
# 静态库
TARGET := $(TARGET).a

$(TARGET) : $(OBJS)
	$(AR) -cvr $@ $^
else
# 动态库
TARGET := $(TARGET).so
LDFLAGS = -shared
CXXFLAGS = -fPIC

$(TARGET) : $(OBJS)
	$(CXX) $(LDFLAGS) $^ -o $@
endif

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY: clean
```

- 编译动态库 libxcom.so ：`make STATIC=0`

```shell
☁  xcom [master] ⚡  make STATIC=0      
g++ -fPIC   -c -o xcom.o xcom.cpp
g++ -shared xcom.o -o libxcom.so
```

- 编译静态库 libxcom.a ：`make STATIC=1`

```shell
☁  xcom [master] ⚡  make STATIC=1
ar -cvr libxcom.a xcom.o
a - xcom.o
```

- 在 xserver 中使用 `libxcom.so`
  - 编译：`make`

```shell
☁  xserver [master] ⚡  make 
g++ -I../xthread -I../xcom   -c -o xserver.o xserver.cpp
g++ xserver.o -o xserver.out -L../xthread -L../xcom -lxthread -lpthread -lxcom
```

- 运行：`run.sh`

```shell
☁  xserver [master] ⚡  ./run.sh 
./xserver.out: error while loading shared libraries: libxcom.so: cannot open shared object file: No such file or directory
```

```shell
☁  xserver [master] ⚡  ldd xserver.out
        linux-vdso.so.1 (0x00007ffd453d6000)
        libxthread.so => not found
        libxcom.so => not found
        libstdc++.so.6 => /lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f04688ef000)
        libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f04688cf000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f04686a6000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f04685bd000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f0468b27000)
```

如上输出信息可知，当 xcom 的静态库和动态库同时存在时 xserver 运行时默认加载动态库，且 xserver 运行时找不到 xcom 的动态库 `libxcom.so` 的路径，因为 Linux 系统中程序运行时默认会在 `/lib` 和 `/usr/lib` 这两个目录下搜索需要的动态链接库，所以这里我们修改 xserver 的运行脚本设置环境变量 `LD_LIBRARY_PATH` 的方式指定额外的动态库搜索路径，多个路径间用冒号分隔

> *xserver/run.sh*

```shell
# !/bin/sh

LD_LIBRARY_PATH=../xthread:../xcom:
export LD_LIBRARY_PATH
./xserver.out
```

- 运行：`run.sh`

```shell
☁  xserver [master] ⚡  ./run.sh       
XServer
Create XCom
Start Thread
begin wait Thread
XTask main
end wait Thread
```

**删除 xcom 中的动态库，清除并重新编译 xserver，使用 xcom 的静态库**

- 清除 xcom 中的动态库：`make STATIC=0 clean`

```shell
☁  xcom [master] ⚡  make STATIC=0 clean 
rm -f libxcom.so xcom.o
```

- 清除 xserver

```shell
☁  xserver [master] ⚡  make clean 
rm -f xserver.out xserver.o
```

- 编译 xserver 使用 xcom 静态库

```shell
☁  xserver [master] ⚡  make
g++ -I../xthread -I../xcom   -c -o xserver.o xserver.cpp
g++ xserver.o -o xserver.out -L../xthread -L../xcom -lxthread -lpthread -lxcom
```

```shell
☁  xserver [master] ⚡  ldd xserver.out                                                                                                     
        linux-vdso.so.1 (0x00007ffd349c9000)
        libxthread.so => not found
        libstdc++.so.6 => /lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007f0025607000)
        libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007f00255e7000)
        libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007f00253be000)
        libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007f00252d7000)
        /lib64/ld-linux-x86-64.so.2 (0x00007f002583f000)
```

> 如上 `ldd xserver.out` 输出信息可知，xserver.out 未使用 xcom 的动态库，使用 xcom 的静态库

- 运行 xserver

```shell
☁  xserver [master] ⚡  ./run.sh 
XServer
Create XCom
Start Thread
begin wait Thread
XTask main
end wait Thread
```

## 7. makefile 函数使用 `wildcard` 自动添加目录下源码生成 `.o`

**Makefile 函数**

- `wildcard`
  - 展开已经存在的、使用空格分开的、匹配此模式的所有文件列表
  - `SRC=$(wildcard *.cpp *.cc)`
- `patsubst`
  - 格式：`$(patsubst<pattern>,<replacement>,<text>)`
  - 名称：模式字符串替换函数 -- `patsubst`
  - `TMP=$(patsubst %.cpp,%.o,$(SRC))`

> **项目示例：[7-makefile_demo/test_make_func](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/7-makefile_demo/test_make_func)**

- 目录结构

```shell
.
├── main.cpp
├── makefile
├── test.cpp
├── xdata.cpp
└── xdata.h
```

> *test_make_func/xdata.h*

```C++
// xdata.h

#ifndef XDATA_H
#define XDATA_H

class XData
{
public:
    XData();
};

#endif
```

> *test_make_func/xdata.cpp*

```C++
#include <iostream>
#include "xdata.h"

using namespace std;

XData::XData()
{
    cout << "Create XData" << endl;
}
```

> *test_make_func/test.cpp*

```C++

#include <iostream>

void Test()
{
    std::cout << "Test" << std::endl;
}
```

> *test_make_func/main.cpp*

```C++
#include <iostream>
#include "xdata.h"

using namespace std;

int main(int argc, char *argv[])
{
    extern void Test();
    Test();
    XData d;
    cout << "test make function" << endl;
    return 0;
}
```

> *test_make_func/makefile*

```makefile
TARGET=test_make_func.out
SRC := $(wildcard *.cpp)
OBJS = $(patsubst %.cpp,%.o,$(SRC))

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@
all:
	@echo $(SRC)

clean :
	$(RM) $(TARGET) $(OBJS)

.PHONY : clean
```

- `make all`:

```shell
☁  test_make_func [master] ⚡  make all
main.cpp test.cpp xdata.cpp
```

如上输出可以看出，`SRC := $(wildcard *.cpp)` 函数将获取当前目录下所有 `.cpp` 文件，并赋值给 `SRC`

- 编译：`make`

```shell
☁  test_make_func [master] ⚡  make
g++    -c -o main.o main.cpp
g++    -c -o test.o test.cpp
g++    -c -o xdata.o xdata.cpp
g++ main.o test.o xdata.o -o test_make_func.out
```

- 运行：`./test_make_func.out`

```shell
☁  test_make_func [master] ⚡  ./test_make_func.out 
Test
Create XData
test make function
```

## 8. `include` 外部文件定义统一的 makefile 头文件

> 定义统一的 Makefile 头文件 `makefile.mk` 并在 `test_include` 和 `first_make` 项目中引用该头文件，当有新的项目增加时，只需要引用 Makefile 头文件，针对项目做一些简单的配置修改即可

**项目示例：[8-makefile_demo](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/8-makefile_demo)**

- 目录结构

```shell
.
├── first_make
│   ├── first_make.cpp
│   ├── makefile
│   ├── makefile.mk
│   ├── xdata.cpp
│   └── xdata.h
├── makefile.mk
├── test_gcc
│   ├── test.h
│   └── test_gcc.cpp
└── test_include
    ├── makefile
    ├── test_include.cpp
    ├── testc.c
    └── testcpp.cc
```

> *8-makefile_demo/makefile.mk*

```Makefile
# makefile.mk

ifndef TARGET
TARGET := test.out
endif

CXXFLAGS := -I../../include -std=c++17 # g++ -C 编译 自动推导
LDFLAGS := -L../xcom -L../xthread      # 链接 可用于自动推导
LDLIBS := -lpthread                    # 链接库 用于自动推导
SRCS := $(wildcard *.cpp *.cc *.c)    # test_include.cpp testcpp.cc testc.c
OBJS := $(patsubst %.cpp,%.o,$(SRCS)) # test_include.o testcpp.cc testc.c
OBJS := $(patsubst %.cc,%.o,$(OBJS))  # test_include.o testcpp.o testc.c
OBJS := $(patsubst %.c,%.o,$(OBJS))   # test_include.o testcpp.o testc.o

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LDLIBS)

clean:
	$(RM) $(OBJS) $(TARGET)
.PHONY: clean # 伪目标 没有对应的文件
```

> *8-makefile_demo/test_incldue/testc.c*

```C
#include <stdio.h>

void TestC()
{
    printf("TestC\n");
}
```

> *8-makefile_demo/test_incldue/testcpp.cc*

```C++
#include <iostream>

void TestCpp()
{
    std::cout << "TestCpp" << std::endl;
}
```

> *8-makefile_demo/test_incldue/test_include.cpp*

```C++
#include <iostream>

extern void TestCpp();
extern "C" void TestC();

using namespace std;

int main(int argc, char *argv[])
{
    TestCpp();
    TestC();

    return 0;
}
```

> *8-makefile_demo/test_incldue/makefile*

```Makefile
# test_include.cpp testcpp.cc testc.c
# clean

TARGET := test_include.out
include ../makefile.mk
CXXFLAGS := $(CXXFLAGS) -I../test_gcc
```

- 编译：`make`

```shell
☁  test_include [master] ⚡  make 
g++ -I../../include -std=c++17  -I../test_gcc   -c -o test_include.o test_include.cpp
g++ -I../../include -std=c++17  -I../test_gcc   -c -o testcpp.o testcpp.cc
cc    -c -o testc.o testc.c
g++ test_include.o testcpp.o testc.o -o test_include.out -L../xcom -L../xthread       -lpthread
```

- 运行：`./test_include.out`

```shell
☁  test_include [master] ⚡  ./test_include.out 
TestCpp
TestC
```

- 清理：`make clean`

```shell
☁  test_include [master] ⚡  make clean
rm -f test_include.o testcpp.o testc.o    test_include.out
```

- 引入新项目 `first_make` 和 `fiest_make` 的依赖项目 `teat_gcc`；并在 `first_make` 中创建 `makefile.mk` 引用顶层 makefile

> *8-makefile_demo/first_make/makefile.mk*

```Makefile
include ../makefile.mk
CXXFLAGS := $(CXXFLAGS) -I../test_gcc
```

- 编译：`make -f makefile.mk`

```shell
☁  first_make [master] ⚡  make -f makefile.mk 
g++ -I../../include -std=c++17  -I../test_gcc   -c -o first_make.o first_make.cpp
g++ -I../../include -std=c++17  -I../test_gcc   -c -o xdata.o xdata.cpp
g++ first_make.o xdata.o -o test.out -L../xcom -L../xthread       -lpthread
```

- 运行：`./test.out`

```shell
☁  first_make [master] ⚡  ./test.out 
test make
Thread Main
Create XData
```

- 清理：`make -f makefile.mk clean`

```shell
☁  first_make [master] ⚡  make -f makefile.mk clean
rm -f first_make.o xdata.o    test.out
```

## 9. makefile 获取 shell 结果实现目录创建和根目录生成目标名称

**执行shell脚本**

- 获取当前路径自动推导目标名称：`pwd` -- `TARGET:=$(notdir $(shell pwd))`
- 目录判断是否存在自动创建目录：
  - `exist=$(shell if[! -d $(INCLUDE_PATH)]; then mkdir $(INCLUDE_PATH); fi;)`
  - `$(exist)`
  - `@-` 不显示命令，执行失败不停止

**项目示例：[9-makefile_demo](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/9-makefile_demo)**

- 目录结构

```shell
.
├── first_make
│   ├── first_make.cpp
│   ├── makefile
│   ├── makefile.mk
│   ├── xdata.cpp
│   └── xdata.h
├── makefile.mk
├── test.mk
├── test_gcc
│   ├── test.h
│   └── test_gcc.cpp
└── test_include
    ├── makefile
    ├── test_include.cpp
    ├── testc.c
    └── testcpp.cc
```

> 测试代码示例：*test.mk*

```Makefile
#test.mk

LDIR = $(shell pwd)  #/root/makefile/src
LSS = $(shell ls)
TMP = $(shell echo 111>222)
OUT := out
INIT = $(shell if [ ! -d $(OUT) ]; then mkdir $(OUT); fi;)
# INIT := $(shell mkdir $(OUT))
test:
	@-echo $(INIT)
	@echo $(LDIR)
	@echo $(LSS)
	@-echo $(TMP)
```

- 编译：`make -f test.mk`

```shell
☁  test_include [master] ⚡  make 
☁  9-makefile_demo [master] ⚡  make -f test.mk 

/mnt/e/wkspace/cmake-tutorial/makefile/src/9-makefile_demo
first_make makefile.mk out test.mk test_gcc test_include

```

- 查看生成文件：`ls`

```shell
☁  9-makefile_demo [master] ⚡  ls
222  first_make  makefile.mk  out  test.mk  test_gcc  test_include
```

如上输出信息可知，`test.mk` 中使用shell生成了 `out` 目录和 `222`

**修改 makefile.mk 中目标名 `TARGET` 使其根据目录生成目标名**

> *9-makefile_demo/makefile.mk*

```Makefile
# makefile.mk

ifndef TARGET
# /root/makefile/src/9-makefile_demo/test_include
# notdir
TARGET := $(notdir $(shell pwd)) # test_include
endif

CXXFLAGS := -I../../include -std=c++17 # g++ -C 编译 自动推导
LDFLAGS := -L../xcom -L../xthread      # 链接 可用于自动推导
LDLIBS := -lpthread                    # 链接库 用于自动推导
SRCS := $(wildcard *.cpp *.cc *.c)    # test_include.cpp testcpp.cc testc.c
OBJS := $(patsubst %.cpp,%.o,$(SRCS)) # test_include.o testcpp.cc testc.c
OBJS := $(patsubst %.cc,%.o,$(OBJS))  # test_include.o testcpp.o testc.c
OBJS := $(patsubst %.c,%.o,$(OBJS))   # test_include.o testcpp.o testc.o

$(TARGET) : $(OBJS)
	$(CXX) $^ -o $@ $(LDFLAGS) $(LDLIBS)

clean:
	$(RM) $(OBJS) $(TARGET)
.PHONY: clean # 伪目标 没有对应的文件
```

> *9-makefile_demo/test_include/makefile*

```Makefile
# test_include.cpp testcpp.cc testc.c
# clean

# TARGET := test_include.out
include ../makefile.mk
CXXFLAGS := $(CXXFLAGS) -I../test_gcc
```

- 编译：`make`

```shell
☁  test_include [master] ⚡  make
g++ -I../../include -std=c++17  -I../test_gcc   -c -o test_include.o test_include.cpp
g++ -I../../include -std=c++17  -I../test_gcc   -c -o testcpp.o testcpp.cc
cc    -c -o testc.o testc.c
g++ test_include.o testcpp.o testc.o -o test_include -L../xcom -L../xthread       -lpthread
```

- 查看生成文件：`ls`

```shell
☁  test_include [master] ⚡  ls
makefile  test_include  test_include.cpp  test_include.o  testc.c  testc.o  testcpp.cc  testcpp.o
```

如上输出信息可知，编译生成可执行文件 `test_include` 就是其项目 `test_include` 的目录名

## 10. 嵌套 make 同时编译和清理多个项目

**同时编译多个项目**

- 嵌套执行make
  - `TARGETS=xserver xthread xcom`
  - `all:$(TARGETS)`
  - `.PHONY: all $(TARGETS)`
- 多目标按次序编译：`make -C $(LDIR)/$@/ -f $(LDIR)/$@/makefile`
- 自定义函数和 `forearch` 实现多目标同时 `clean` 清理
  - `make -C $@ -f $@/makefile clean`
  - `CLEANTARGETS:=$(forearch n,$(TARGETS),$(shell pwd)/$(n))`

**项目示例：[10-makefile_demo](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/10-makefile_demo)**

- 目录结构

```shell
.
├── makefile
├── xcom
│   ├── makefile
│   ├── xcom.cpp
│   └── xcom.h
├── xserver
│   ├── makefile
│   ├── run.sh
│   └── xserver.cpp
└── xthread
    ├── makefile
    ├── xthread.cpp
    └── xthread.h
```

> *10-makefile_demo/makefile*

```Makefile
# makefile
TARGET=xcom xthread xserver
$(TARGET):
	$(MAKE) -f $@/makefile
```

- 编译：`make`

```shell
☁  10-makefile_demo [master] ⚡  make
make: 'xcom' is up to date.
```

如上输出信息，`xcom` 是最新的，这是因为没有做依赖项；make 将 `xcom` 当做文件来处理，认为它是最新的，我们在这里将它设置为伪目标：

> *10-makefile_demo/makefile*

```Makefile
# makefile
TARGET=xcom xthread xserver
$(TARGET):
	$(MAKE) -f $@/makefile

.PHONY: $(TARGET)
```

- 编译：`make`

```shell
☁  10-makefile_demo [master] ⚡  make
make -f xcom/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo'
make[1]: *** No rule to make target 'xcom.o', needed by 'libxcom.so'.  Stop.
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo'
make: *** [makefile:6: xcom] Error 2
```

如上输出信息，生成 `libxcom` 时找不到 `xcom.o`，是由于 `xcom/makefile` 中 `OBJS` 对应的依赖是自动推导的，因为工作路径不对，所以这里我们修改 makefile 添加 `-C` 指定 makefile 的工作路径

> *10-makefile_demo/makefile*

```Makefile
# makefile
TARGET=xcom xthread xserver
$(TARGET):
	$(MAKE) -C $@/ -f $@/makefile

.PHONY: $(TARGET)
```

- 编译：`make`

```shell
☁  10-makefile_demo [master] ⚡  make
make -C xcom/ -f xcom/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
make[1]: xcom/makefile: No such file or directory
make[1]: *** No rule to make target 'xcom/makefile'.  Stop.
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
make: *** [makefile:6: xcom] Error 2
```

如上输出，还是找不到 `xcom/makefile`，也就是说相对路径在当前执行环境中找不到 `xcom/makefile`；修改 makefile 设定绝对路径

> *10-makefile_demo/makefile*

```Makefile
# makefile
TARGET=xcom xthread xserver
LDIR=$(shell pwd)
# all:$(TARGET)
$(TARGET):
	$(MAKE) -C $(LDIR)/$@/ -f $(LDIR)/$@/makefile

.PHONY: $(TARGET)
```

- 编译：`make`

```shell
☁  10-makefile_demo [master] ⚡  make
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom/ -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
make[1]: 'libxcom.so' is up to date.
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
```

如上输出信息，`xcom` 编译成功，但 `TARGET=xcom xthread xserver` 为3个目标（xcom xthread xserver），`xthread`、`xserver` 并未参与编译。修改 makefile 添加目标 `all` 并依赖目标 `TARGET`，为防止同名文件（all）也将其设置为伪目标

> *10-makefile_demo/makefile*

```Makefile
# makefile
TARGET=xcom xthread xserver
LDIR=$(shell pwd)
all:$(TARGET)
$(TARGET):
	$(MAKE) -C $(LDIR)/$@/ -f $(LDIR)/$@/makefile

.PHONY: $(TARGET) all
```

- 编译：`make`

```shell
☁  10-makefile_demo [master] ⚡  make
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom/ -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
make[1]: 'libxcom.so' is up to date.
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread/ -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread'
make[1]: 'libxthread.so' is up to date.
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread'
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver/ -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver'
make[1]: 'xserver.out' is up to date.
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver'
```

如上输出信息，两个库 `xcom` 和 `xthread` 一个可执行文件 `xserver` 全都编译完成，这里可以看到 `xserver` 是在最后去编译的，是因为 `xserver` 依赖 `xcom` 和 `xthread`，所以编译时应该按次序来编译

**清理多个项目**

> *10-makefile_demo/makefile*

```Makefile
# makefile
TARGET=xcom xthread xserver
LDIR=$(shell pwd)
CTARGET=$(foreach n,$(TARGET),$(LDIR)/$n) #/src/10-makefile_demo/xcom /src/10-makefile_demo/xthread
all:$(TARGET)
$(TARGET):
	$(MAKE) -C $(LDIR)/$@/ -f $(LDIR)/$@/makefile

clean:$(CTARGET)
$(CTARGET):
	$(MAKE) -C $@ -f $@/makefile clean

.PHONY: $(TARGET) all clean $(CTARGET)
```

- 编译：`make`

```shell
☁  10-makefile_demo [master] ⚡  make      
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom/ -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
g++ -fPIC   -c -o xcom.o xcom.cpp
g++ -shared xcom.o -o libxcom.so
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread/ -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread'
g++ -fPIC   -c -o xthread.o xthread.cpp
g++ -shared xthread.o -o libxthread.so
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread'
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver/ -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver/makefile
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver'
g++ -I../xthread -I../xcom   -c -o xserver.o xserver.cpp
g++ xserver.o -o xserver.out -L../xthread -L../xcom -lxthread -lpthread -lxcom
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver'
```

- 清理：`make clean`

```shell
☁  10-makefile_demo [master] ⚡  make clean
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom/makefile clean
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
rm -f libxcom.so xcom.o
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xcom'
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread/makefile clean
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread'
rm -f libxthread.so xthread.o
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xthread'
make -C /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver -f /mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver/makefile clean
make[1]: Entering directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver'
rm -f xserver.out xserver.o
make[1]: Leaving directory '/mnt/e/wkspace/cmake-tutorial/makefile/src/10-makefile_demo/xserver'
```

## 11. 自定义 makefile 函数调用实现中的 install

**项目示例：[11-makefile_demo/test_install](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/11-makefile_demo/test_install)**

- 目录结构

```shell
.
├── makefile
└── test_install.cpp
```

> *test_install.cpp*

```C++
#include <iostream>
#include <thread>

using namespace std;

int main(int argc, char *argv[])
{
    int sec = 1;
    if (argc > 1)
    {
        sec = atoi(argv[1]);
    }

    for (int i = 0;;i++)
    {
        cout << "test install " << i << endl;
        this_thread::sleep_for(chrono::seconds(sec));
    }

    return 0;
}
```

> *makefile*

```makefile
# test_install/makefile

TARGET = test_install
OBJS = test_install.o
CC := g++
# make install OUT=./out
OUT = ./out

define Install # $(1) TARGET $(2) OUT
	@echo "begin install "$(1)
	mkdir -p $(2)
	cp $(1) $(2)
	@echo $(1) "install success!!!"
endef

$(TARGET) : $(OBJS)

install : $(TARGET)
	$(call Install, $(TARGET), $(OUT)/bin/)

#	@echo "begin install "$(TARGET)
#	mkdir -p $(OUT)/bin/
#	cp $(TARGET) $(OUT)/bin/
#	@echo $(TARGET) "install success!!!"

clean:
	$(RM) $(TARGET) $(OBJS)

.PHONY : install clean
```

- 编译安装：`make install`

```shell
☁  test_install [master] ⚡  make install
begin install  test_install
mkdir -p  ./out/bin/
cp  test_install  ./out/bin/
test_install install success!!!
```

- 编译并安装到指定目录：`make install OUT=./out1`

```shell
☁  test_install [master] ⚡  make install OUT=./out1    
g++    -c -o test_install.o test_install.cpp
g++   test_install.o   -o test_install
begin install  test_install
mkdir -p  ./out1/bin/
cp  test_install  ./out1/bin/
test_install install success!!!
```

## 12. 使用 makefile 生成后台启动和停止脚本并安装

**项目示例：[12-makefile_demo/test_install](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/12-makefile_demo/test_install)**

- 目录结构

```shell
.
├── makefile
└── test_install.cpp
```

```makefile
# test_install/makefile

TARGET = test_install
OBJS = test_install.o
CC := g++
# make install OUT=./out
OUT = /usr

# start_test_install
# nohup test_install 5 &

# stop_test_install
# killall test_install

STARTSH = start_$(TARGET)
STOPSH = stop_$(TARGET)
define Install # $(1) TARGET $(2) OUT $(3) para1 $(4) para2...
	@echo "begin install "$(1)
	mkdir -p $(2)
	cp $(1) $(2)
	
	@echo "begin make start shell"
	echo "nohup $(1) $(3) $(4) $(5) $(6) &" > $(STARTSH)
	chmod +x $(STARTSH)
	cp $(STARTSH) $(2)
	@echo "end make start shell"
	
	@echo "begin make stop shell"
	echo killall $(1) > $(STOPSH)
	cp $(STOPSH) $(2)
	@echo "end make stop shell"
	
	@echo $(1) "install success!!!"
endef

$(TARGET) : $(OBJS)

install : $(TARGET)
	$(call Install, $(TARGET), $(OUT)/bin/, 5)

#	@echo "begin install "$(TARGET)
#	mkdir -p $(OUT)/bin/
#	cp $(TARGET) $(OUT)/bin/
#	@echo $(TARGET) "install success!!!"

clean:
	$(RM) $(TARGET) $(OBJS) $(STARTSH) $(STOPSH)

.PHONY : install clean
```

- 编译安装：`sudo make install`

```shell
☁  test_install [master] ⚡  sudo make install
g++    -c -o test_install.o test_install.cpp
g++   test_install.o   -o test_install
begin install  test_install
mkdir -p  /usr/bin/
cp  test_install  /usr/bin/
begin make start shell
echo "nohup  test_install  5    &" > start_test_install
chmod +x start_test_install
cp start_test_install  /usr/bin/
end make start shell
begin make stop shell
echo killall  test_install > stop_test_install
cp stop_test_install  /usr/bin/
end make stop shell
test_install install success!!!
```

- 后台启动运行：`start_test_install`

```shell
☁  test_install [master] ⚡  start_test_install
nohup: appending output to 'nohup.out'
```

- 查看后台运行程序 `ps -ef|grep test`

```shell
☁  test_install [master] ⚡  ps -ef|grep test   
fyang       3735     730  0 09:19 pts/2    00:00:00 test_install 5
fyang       3751     732  0 09:19 pts/2    00:00:00 grep --color=auto --exclude-dir=.bzr --exclude-dir=CVS --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn --exclude-dir=.idea --exclude-dir=.tox test
```

- 停止运行：`stop_test_install`

```shell
☁  test_install [master] ⚡  ps -ef|grep test  
fyang       3788     732  0 09:21 pts/2    00:00:00 grep --color=auto --exclude-dir=.bzr --exclude-dir=CVS --exclude-dir=.git --exclude-dir=.hg --exclude-dir=.svn --exclude-dir=.idea --exclude-dir=.tox test
```

## 13. 项目演示 xserver 完成执行程序-静态库-动态库公用 makefile

**项目示例：[13-makefile_demo](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_projects/src/13-makefile_demo)**

- 目录结构：

```shell
.
├── makefile
├── makefile.mk
├── xcom
│   ├── makefile
│   ├── xcom.cpp
│   └── xcom.h
├── xserver
│   ├── makefile
│   └── xserver.cpp
└── xthread
    ├── makefile
    ├── xthread.cpp
    └── xthread.h
```

> *[xcom/xcom.h](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xcom/xcom.h)*

```C++
#ifndef XCOM_H
#define XCOM_H

class XCom
{
public:
    XCom();
};

#endif
```

> *[xcom/xcom.cpp](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xcom/xcom.cpp)*

```C++
#include "xcom.h"
#include <iostream>

using namespace std;

XCom::XCom()
{
    cout << "Create XCom" << endl;
}
```

> *[xcom/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xcom/makefile)*

```Makefile
# xcom makefile

ifeq ($(STATIC), 1)
LIBTYPE = .a
else
LIBTYPE = .so
endif

include ../makefile.mk
```

> *[xthread/xthread.h](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xthread/xthread.h)*

```C++
#ifndef XTHREAD_H
#define XTHREAD_H
#include <thread>


class XThread
{
public:
    virtual void Start();
    virtual void Wait();

private:
    virtual void Main() = 0;
    std::thread th_;
};

#endif
```

> *[xthread/xthread.cpp](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xthread/xthread.cpp)*

```C++
#include "xthread.h"
#include <iostream>

using namespace std;

void XThread::Start()
{
    cout << "Start Thread" << endl;
    th_ = std::thread(&XThread::Main, this);
}
void XThread::Wait()
{
    cout << "begin Wait Thread" << endl;
    th_.join();
    cout << "end Wait Thread" << endl;
}
```

> *[xthread/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xthread/makefile)*

```Makefile
# xthread makefile
LIBTYPE = .so
include ../makefile.mk
```

> *[xserver/xserver.cpp](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xserver/xserver.cpp)*

```C++
#include <iostream>
#include "xthread.h"
#include "xcom.h"


using namespace std;
/*
LD_LIBRARY_PATH=./;
export LD_LIBRARY_PATH
*/
class XTask : public XThread
{
public:
    XTask(int s) : sec_(s) {}
    void Main() override
    {
        cout << "XTask main" << endl;
        for (int i = 0;; i++)
        {
            cout << "test task " << i << endl;
            this_thread::sleep_for(chrono::seconds(sec_));
        }
    }

private:
    int sec_ = 3;
};


int main(int argc, char *argv[])
{
    cout << "XServer" << endl;
    int sec = 1;
    if (argc > 1)
        sec = atoi(argv[1]);
    XCom com;
    XTask task(sec);
    task.Start();
    task.Wait();
    return 0;
}
```

> *[xserver/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/xserver/makefile)*


```Makefile
CXXFLAGS := -I ../xthread -I ../xcom
LDFLAGS := -L ../xthread -L ../xcom
LDLIBS := -lxthread -lxcom
include ../makefile.mk # 统一的makefile头
```

> *[13-makefile_demo/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/makefile)*

```Makefile
# makefile

TARGET = xcom xthread xserver
LDIR = $(shell pwd)
CTARGET = $(foreach n, $(TARGET), $(LDIR)/$n)  # /make/src/xcom /make/src/xthread

all : $(TARGET)
$(TARGET) :
	$(MAKE) -C $(LDIR)/$@/ -f $(LDIR)/$@/makefile

clean : $(CTARGET)
$(CTARGET) :
	$(MAKE) -C $@ -f $@/makefile clean

.PHONY : $(TARGET) all clean $(CTARGET)
```

> *[13-makefile_demo/makefile.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_projects/src/13-makefile_demo/makefile.mk)*

```Makefile
# makefile.mk

ifndef TARGET
TARGET := $(notdir $(shell pwd)) # test_include
endif

CXXFLAGS := $(CXXFLAGS) -I ../../include -std=c++17 # g++ -c 编译自动推导
LDFLAGS := $(LDFLAGS) # 链接 可用于自动推导
LDLIBS := $(LDLIBS) -lpthread # 链接库 用于自动推导

SRCS := $(wildcard *.cpp *.cc *.c)
OBJS := $(patsubst %.cpp,%.o,$(SRCS))
OBJS := $(patsubst %.cc,%.o,$(OBJS))
OBJS := $(patsubst %.c,%.o,$(OBJS))

# 区分动态库 静态库和可执行程序
ifeq ($(LIBTYPE),.so) # 动态库 $(strip $(TARGEET)) 去掉前后空格\t
	TARGET := lib$(strip $(TARGET)).so
	LDLIBS := $(LDLIBS) -shared
	CXXFLAGS := $(CXXFLAGS) -fPIC
endif

ifeq ($(LIBTYPE), .a) # 静态库
	TARGET := lib$(strip $(TARGET)).a
endif

#目标生成
$(TARGET):$(OBJS)
ifeq ($(LIBTYPE),.a) #静态库 
	$(AR) -cvr $@ $^
else
	$(CXX) $(LDFLAGS) $^ -o $@ $(LDLIBS)
endif

# rm -r test.o test
# 目标清理
clean:
	$(RM) $(OBJS) $(TARGET)

.PHONY: clean # 伪目标 没有对应的文件
```

- 编译 `xcom` 默认生成动态库 `libxcom.so`：`make`

```shell
☁  xcom [master] ⚡  make      
g++  -I ../../include -std=c++17  -fPIC   -c -o xcom.o xcom.cpp
g++   xcom.o -o libxcom.so  -lpthread  -shared
```
- 编译 `xcom` 生成静态库 `libxcom.a`：`make STATIC=1`

```shell
☁  xcom [master] ⚡  make STATIC=1
g++  -I ../../include -std=c++17    -c -o xcom.o xcom.cpp
ar -cvr libxcom.a xcom.o
a - xcom.o
```

- 编译 `xthread` ：`make`

```shell
☁  xthread [master] ⚡  make         
g++  -I ../../include -std=c++17  -fPIC   -c -o xthread.o xthread.cpp
g++   xthread.o -o libxthread.so  -lpthread  -shared
```

## 14. xserver 项目 makefile 实战自动生成 install 和 uninstall





















