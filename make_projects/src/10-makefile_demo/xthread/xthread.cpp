#include <iostream>
#include "xthread.h"

using namespace std;

void XThread::Start()
{
    cout << "Start Thread" << endl;
    th_ = std::thread(&XThread::Main, this);
}

void XThread::Wait()
{
    cout << "begin wait Thread" << endl;
    th_.join();
    cout << "end wait Thread" << endl;
}