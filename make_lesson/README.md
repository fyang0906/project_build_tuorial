# 玩转 Makefile

> ***参考资料：[GNU make](https://www.gnu.org/software/make/manual/make.html)***
>
> ***参考资料：[跟我一起写 makefile](https://gitee.com/fyang0906/project_build_tuorial/tree/master/ebook)***
>
> ***参考资料：[驾驭makefile](https://gitee.com/fyang0906/project_build_tuorial/tree/master/ebook)***

-----



[TOC]



-----



## 1. make 和 makefile

**`make` 是一个应用程序**

- 解析源程序之间的依赖关系
- 根据依赖关系自动维护编译工作
- 执行宿主操作系统中的各种命令

**`makefile` 是一个描述文件**

- 定义一系列的规则来指定源文件编译的先后顺序
- 拥有特定的语法规则，支持函数定义和函数调用
- 能够直接集成操作系统中的各种命令

**`make` 和 `makefile` 之间的关系**

makefile 中的描述用于指导 make 程序如何完成工作；make 根据 makefile 中的规则执行命令，最后完成编译输出

```
+----------+
|          |  ---->  x.c        +------+
|          |                    |      |
| makefile |  ---->  y.c        | make |  ====>  目标输出
|          |                    |      |
|          |  ---->  z.c        +------+
+----------+
```

- 一个简单的 makefile 示例

```
# mf.txt

hello :
----- echo "hello makefile"
 ↓    --------------------
目标	           ↓
       实现目标所需执行的命令
```

> **注意：** 目标后面的命令需要用 **Tab** 键 **`\t`** 隔开！

- make 程序的使用示例

```bash
make -f mf.txt hello
```

> **功能说明：** 以 `hello` 关键字作为目标查找 `mf.txt` 文件，并执行 `hello` 处的命令


- make 程序的简写实例

```bash
make hello
```

> **功能说明：** 以 `hello` 关键字作为目标查找 **makefile** 或 **Makefile** 文件，并执行 `hello` 处的命令

```bash
make
```

> **功能说明：** 查找 **makefile** 或 **Makefile** 文件中**最顶层目标**，并执行**最顶层目标**的命令


**编程实验**

> *[1-make_makefile/1.0](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/1-make_makefile/1.0)*

> *makefile.txt*

```makefile
# 1-make_makefile/1.0/makefile.txt

hello :
	echo "hello makefile"
```

- 执行命令：`make -f makefile.txt hello`

```bash
☁  1.0 [master] ⚡  make -f makefile.txt hello
echo "hello makefile"
hello makefile
```

> *makefile*

```makefile
# 1-make_makefile/1.0/makefile

hello : 
	echo "hello makefile"
```

- 执行命令：`make hello`

```bash
☁  1.0 [master] ⚡  make hello
echo "hello makefile"
hello makefile
```

> *makefile*

```makefile
# 1-make_makefile/1.0/makefile

hello : 
	echo "hello makefile"

test : 
	echo "test"
```

- 执行命令：`make`

```bash
☁  1.0 [master] ⚡  make
echo "hello makefile"
hello makefile
```

- 执行命令：`make test`

```bash
☁  1.0 [master] ⚡  make test 
echo "test"
test
```

> ***[1-make_makefile/1.1](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/1-make_makefile/1.1)***

```makefile
# 1-make_makefile/1.1/makefile

hello : 
	echo "hello makefile"

test : 
	echo "test"
	pwd
	ls
```

- 执行命令：`make test`

```bash
☁  1.1 [master] ⚡  make test
echo "test"
test
pwd
/mnt/e/wkspace/project_build_tuorial/make_lesson/1-make_makefile/1.1
ls
makefile
```

**小结**

- make 只是一个功能特殊的应用程序
- make 用于根据指定的目标执行相应的命令
- makefile 用于定义目标和实现目标所需的命令
- makefile 有特定的语法规则，支持函数定义和调用


## 2. makefile 的结构

**makefile 的意义**

- makefile 用于定义源文件之间的依赖关系
- makefile 说明如何编译各个源文件并生成可执行文件

> 依赖的定义：

```
targets : prerequisites ; command1
+------+
| '\t' | command2
+------+
```

**makefile 中的元素含义**

- **targets**
  - 通常是需要生成的目标文件名
  - `make` 所需执行的命令名称
- **prerequisities**
  - 当前目标所依赖的其他目标或文件
- **command**
  - 完成目标所需要执行的命令

**规则中的注意事项**

- **targets** 可以包含多个目标
  - 使用空格对多个目标进行分隔
- **prerequisites** 可以包含多个依赖
  - 使用空格对多个依赖进行分隔
- **[Tab] 键：'\t'**
  - 每一个命令必须以 [Tab] 字符开始
  - [Tab] 字符告诉 `make` 此行是一个命令行
- **续行符：\\**
  - 可以将内容分开写到下一行，提高可读性

**一个 makefile 的依赖示例**

```makefile
all : test
	echo "make all"
test : 
	echo "make test"
```

```
+----+                      +----+
|----|----+                 |----|-----+
|         |                 |          |
|   all   |    -------->    |   test   |
+---------+                 +----------+
```

**依赖规则**

- 当目标对应的文件不存在，执行对应命令
- 当依赖在时间上比目标更新，执行对应命令
- 当依赖关系连续发生时，对比依赖链上的每一个目标

> **小技巧：** **makefile** 中可以在命令前加上 `@` 符，作用为命令无回显

> *[2-makefile_struct/2.0](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/2-makefile_struct/2.0)*

```makefile
# 2-makefile_struct/2.0/makefile

all : test
	@echo "make all"

test :
	@echo "make test"
```

- 执行命令：`make`

```bash
☁  2.0 [master] ⚡  make
make test
make all
```

- 执行命令：`make test`

```bash
☁  2.0 [master] ⚡  make test
make test
```

**第一个 make 的编译示例**

> *[2-makefile_struct/2.1](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/2-makefile_struct/2.1)*

- 目录结构

```shell
.
├── func.c
├── main.c
└── makefile
```

> *func.c*

```C
#include <stdio.h>

void foo()
{
    printf("void foo() : hello makefile\n");
}
```

> *main.c*

```C
#include <stdio.h>

extern void foo();

int main(void)
{
    foo();

    return 0;
}
```

> *makefile*

```makefile

hello.out : main.o func.o
	gcc -o hello.out main.o func.o
main.o : main.c
	gcc -o main.o -c main.c
func.o : func.c
	gcc -o func.o -c func.c
```

```
                    +----------+         +----------+
                    |          |         |          |
                  / |  main.o  |  ---->  |  main.c  |
+-------------+  /  |          |         |          |
|             | /   +----------+         +----------+
|  hello.out  |/
|             |\
|             | \   +----------+         +----------+
+-------------+  \  |          |         |          |
                  \ |  func.o  |  ---->  |  func.c  |
				    |          |         |          |
				    +----------+         +----------+
```

> **小技巧：** 工程开发中可以将最终可执行文件名和 `all` 同时作为 makefile 中的第一条规则的目标


```makefile
hello.out all: func.o main.o
	gcc -o hello.out func.o main.o
```

- 编译：`make`

```shell
☁  2.1 [master] ⚡  make
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello.out func.o main.o
```

- 运行：`./hello.out`

```shell
☁  2.1 [master] ⚡  ./hello.out 
void foo() : hello makefile
```

- 使用 make 编译 Java 示例

> *[2-makefile_struct/2.2](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/2-makefile_struct/2.2)*

- 目录结构

```shell
.
├── HelloJava.java
└── makefile
```

> *HelloJava.java*

```java
public class HelloJava {
    public static void main(String[] args) {
        System.out.println("Hello Java!");
    }
}
```

> *makefile*

```makefile
HelloJava.class all: HelloJava.java
	@javac HelloJava.java
	@java HelloJava
```

- 编译：`make`

```shell
☁  2.2 [master] ⚡  make
Hello Java!
```

**小结**

- makefile 用于定义源文件之间的依赖关系
- makefile 说明如何编译各个源文件并生成可执行文件
- makefile 中的目标之间存在连续依赖关系
- 依赖存在并且命令执行成功是目标完成的充要条件


## 3. 伪目标

> **思考：** makefile 中的目标究竟是什么？

**默认情况**

- make 认为目标对应着一个文件
- make 比较目标文件和依赖文件的新旧关系，决定是否执行命令
- make 以文件处理作为第一优先级

**下面的代码有什么意义？**

```
clean : 
	rm *.o hello.out
          |
          |
          ↓
      make clean
```

> *[3-phony/3.1](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/3-phony/3.1)*


- 目录结构

```shell
.
├── clean
├── func.c
├── main.c
└── makefile
```

> *func.c*

```C
#include <stdio.h>

void foo()
{
    printf("void foo() : hello makefile\n");
}
```

> *main.c*

```C
#include <stdio.h>

extern void foo();

int main(void)
{
    foo();

    return 0;
}
```

> *makefile*

```makefile
# 3-phony/3.1/makefile

hello.out all : func.o main.o
	gcc -o hello.out func.o main.o

func.o : func.c
	gcc -o func.o -c func.c

main.o : main.c
	gcc -o main.o -c main.c

clean :
	rm *.o hello.out
```

- 编译：`make`

```bash
☁  3.1 [master] ⚡  make
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello.out func.o main.o
```

- 清理：`make clean`

```bash
☁  3.1 [master] ⚡  make clean 
make: 'clean' is up to date.
```

> **注意：** 在默认情况下 make 认为 `clean` 目标是处理 clean 文件，因为 clean 文件是最新的，所以执行命令 `make clean` 时，输出上述信息：`'clean' is up to date`


**makefile 中的伪目标**

- 通过 `.PHONY` 关键字声明一个伪目标
- 伪目标不对应任何实际的文件
- 不管伪目标的依赖是否更新，命令总是执行

**伪目标的语法：先声明，后使用**

> **本质：** 伪目标是 make 中特殊目标 `.PHONY` 的依赖

```makefile
.PHONY : clean

# 注释

clean :
	rm *.o hello.out
```

**使用伪目标**

> *[3-phony/3.2](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/3-phony/3.2)*

- 目录结构

```shell
.
├── clean
├── func.c
├── main.c
└── makefile
```

```makefile
# 3-phony/3.2/makefile

hello.out all : func.o main.o
	gcc -o hello.out func.o main.o

func.o : func.c
	gcc -o func.o -c func.c

main.o : main.c
	gcc -o main.o -c main.c

.PHONY : clean

clean :
	rm *.o hello.out
```

- 编译：`make`

```bash
☁  3.2 [master] ⚡  make  
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello.out func.o main.o
```

- 清理：`make clean`

```bash
☁  3.2 [master] ⚡  make clean
rm *.o hello.out
```

**伪目标的妙用：规则调用（函数调用）**

```makefile
.PHONY : clean rebuild all

# other rules

rebuild : clean all

clean :
	rm *.o hello.out
```

> **原理：** 当一个目标的依赖包含伪目标时，伪目标所定义的命令总是会被执行

> *[3-phony/3.3](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/3-phony/3.3)*

- 目录结构

```shell
.
├── clean
├── func.c
├── main.c
└── makefile
```

```makefile
# 3-phony/3.3/makefile

hello.out : func.o main.o
	gcc -o hello.out func.o main.o

func.o : func.c
	gcc -o func.o -c func.c

main.o : main.c
	gcc -o main.o -c main.c

.PHONY : rebuild clean all

rebuild : clean all

all : hello.out

clean :
	rm *.o hello.out
```

- 编译：`make all`

```bash
☁  3.3 [master] ⚡  make all
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello.out func.o main.o
```

- 重新编译：`make rebuild`

```bash
☁  3.3 [master] ⚡  make rebuild 
rm *.o hello.out
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello.out func.o main.o
```

- 清理：`make clean`

```bash
☁  3.3 [master] ⚡  make clean 
rm *.o hello.out
```

**技巧：绕开 `.PHONY` 关键字定义伪目标**

```makefile
clean : FORCE
	rm *.o hello.out

FORCE:
```

> **原理：** 如果一个规则没有命令或者依赖，并且它的目标不是一个存在的文件名；在执行此规则时，目标总会被认为是最新的


> *[3-phony/3.4](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/3-phony/3.4)*


- 目录结构

```shell
.
├── clean
├── func.c
├── main.c
└── makefile
```

```makefile
# 3-phony/3.4/makefile

hello.out : func.o main.o
	gcc -o hello.out func.o main.o

func.o : func.c
	gcc -o func.o -c func.c

main.o : main.c
	gcc -o main.o -c main.c

clean : FORCE
	rm *.o hello.out
FORCE :
```

> **Tips：** 需要确保 `FORCE :` 没有依赖的目标，并且在当前目录下面没有 `FORCE` 命名的文件存在

- 编译：`make`

```bash
☁  3.4 [master] ⚡  make
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello.out func.o main.o
```

- 清理：`make clean`

```bash
☁  3.4 [master] ⚡  make clean 
rm *.o hello.out
```

**小结：**

- 默认情况下，make 认为目标对应着一个文件
- `.PHONY` 用于声明一个伪目标，伪目标不对应实际的文件
- 伪目标的本质是 make 中特殊目标 `.PHONY` 的依赖
- 使用伪目标可以模拟 “函数调用”

## 4. 变量和不同的赋值方式

- makefile 中支持程序设计语言中变量的概念
- makefile 中的变量只代表文本数据（字符串）
- makefile 中的变量名规则
  
  - 变量名可以包含字符，数字，下划线
  - 不能包含 `:`, `#`, `=` 或 ` `
  - 变量名大小写敏感

**变量的定义和使用**

```
+-----------------------+
|  CC := gcc            |
|  TARGET := hello.out  |    ---->    变量定义
+-----------------------+

(TARGET) : func.o main.o
--------
    |    $(CC) -o $(TARGET) func.o main.o
	|    -----
    |      |
	|------+
        ↓
	 变量使用
```

> *4-variable_assignment/4.1*

- 目录结构：

```shell
.
├── clean
├── func.c
├── main.c
└── makefile
```


```makefile
# 4-variable_assignment/4.1/makefile

CC := gcc

TARGET := hello.out

$(TARGET) : func.o main.o
	$(CC) -o $(TARGET) func.o main.o

func.o : func.c
	$(CC) -o func.o -c func.c

main.o : main.c
	$(CC) -o main.o -c main.c

.PHONY : rebuild clean all

rebuild : clean all

all : $(TARGET)

clean : 
	rm *.o $(TARGET)
```

- 编译：`make`

```bash
☁  4.1 [master] ⚡  make       
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello.out func.o main.o
```

**变量和不同的赋值方式**

makefile 中变量的赋值方式

- 简单赋值 `:=`
- 递归赋值 `=`
- 条件赋值 `?=`
- 追加赋值 `+=`

> 不同的赋值方式意义不同！

- 简单赋值(simple_assignment) **`:=`** 

  - 程序设计语言中的通用的赋值方式
  - 只针对当前语句的变量有效

> *[simple_assignment](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/4-variable_assignment/4.2/simple_assignment)*

```makefile

x := foo
y := $(x)b
x := new

.PHONY : test

test :
	@echo "x => $(x)"
	@echo "y => $(y)"
```

- 执行命令：`make`

```bash
☁  simple_assignment [master] ⚡  make
x => new
y => foob
```

- 递归赋值(recursive_assignment) **`=`**

  - 赋值操作可能影响多个其它变量
  - 所有与目标变量相关的其他变量都将受到影响

> *[recursive_assignment](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/4-variable_assignment/4.2/recursive_assignment)*

```makefile

x = foo
y = $(x)b
x = new

.PHONY : test

test :
	@echo "x => $(x)"
	@echo "y => $(y)"
```

- 执行命令：`make`

```bash
☁  recursive_assignment [master] ⚡  make
x => new
y => newb
```

> *[4-variable_assignment/4.2/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/4-variable_assignment/4.2/makefile)*


```makefile
# ex1
# x := foo
# y := $(x)b
# x := new

# # ex2
# x = foo
# y = $(x)b
# x = new

# ex3
a = $(b)
b = $(c)
c = hello-makefile

# ex4
# x := foo
# y := $(x)b
# x += new

.PHONY : test

test : 
	@echo "x => $(x)"
	@echo "y => $(y)"
	@echo "a => $(a)"
	@echo "b => $(b)"
	@echo "c => $(c)"
```

- 执行命令：`make`

```bash
☁  4.2 [master] ⚡  make
x => 
y => 
a => hello-makefile
b => hello-makefile
c => hello-makefile
```

- 条件赋值(conditional_assignment) **`?=`**

  - 如果变量未定义，使用赋值符号中的值定义变量
  - 如果变量已经定义，赋值无效

> *[conditional_assignment](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/4-variable_assignment/4.2/conditional_assignment)*


```makefile
x = foo
y = $(x)b
x ?= new

.PHONY : test

test :
	@echo "x => $(x)"
	@echo "y => $(y)"
```

- 执行命令：`make`

```bash
☁  conditional_assignment [master] ⚡  make
x => foo
y => foob
```

- 追加赋值(append_assignment) **`+=`**

  - 原变量值之后加上一个新值
  - 原变量值与新值之间由空格隔开

> *[append_assignment](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/4-variable_assignment/4.2/append_assignment)*

```makefile

x := foo
y := $(x)b
x += $(y)

.PHONY : test

test : 
	@echo "x => $(x)"
	@echo "y => $(y)"
```

- 执行命令：`make`

```bash
☁  append_assignment [master] ⚡  make
x => foo foob
y => foob
```

**小结：**

- makefile 中支持变量的定义和使用
- makefile 中存在四种变量的赋值方式
  - 简单赋值 `:=`
  - 递归赋值 `=`
  - 条件赋值 `?=`
  - 追加赋值 `+=`


## 5. 预定义变量的使用

**在 makefile 中存在一些预定义的变量**

- 自动变量

  - **`$@`, `$^`, `$<`**

- 特殊变量

  - **`$(MAKE)`, `$(MAKECMDGOALS)`, `$(MAKEFILE_LIST)`**
  - **`$(MAKE_VERSION)`, `$(CURDIR)`, `$(.VARIABLES)`**
  - ...

**自动变量的意义**

- `$@`

  - 当前规则中触发命令被执行的目标

- `$^`

  - 当前规则中的所有依赖

- `$<`

  - 当前规则中的第一个依赖

> 自动变量的使用示例：*[5-predefined_variable/5.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/5-predefined_variable/5.1/makefile)*

```makefile

# 5-predefined_variable/5.1/makefile

.PHONY : all first second third

all : first second third
	@echo "\$$@ => $@"
	@echo "$$^ => $^"
	@echo "$$< => $<"

first:
second:
third:
```
> **注意：**
> 
> 1. `$` 对于 makefile 有特殊含义 -- 输出时需要加上一个 `$` 进行转义
>
> 2. `$@` 对于 Bash Shell 有特殊含义 -- 输出时需要加上 `\` 进行转义


- 执行命令：`make all`

```bash
☁  5.1 [master] ⚡  make all
$@ => all
$^ => first second third
$< => first
```

> *[5-predefined_variable/5.2](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/5-predefined_variable/5.2)*

- 目录结构

```shell
.
├── clean
├── func.c
├── main.c
└── makefile
```

```makefile
# 5-predefined_variable/5.2/makefile

CC := gcc
TARGET := hello-world.out

$(TARGET) : func.o main.o
	$(CC) -o $@ $^

func.o : func.c
	$(CC) -o $@ -c $^

main.o : main.c
	$(CC) -o $@ -c $^

.PHONY : rebuild clean all

rebuild : clean all

all : $(TARGET)

clean : 
	rm *.o $(TARGET)
```

- 编译：`make`

```bash
☁  5.2 [master] ⚡  make
gcc -o func.o -c func.c
gcc -o main.o -c main.c
gcc -o hello-world.out func.o main.o
```

- 清理：`make clean`

```bash
☁  5.2 [master] ⚡  make clean 
rm *.o hello-world.out
```

**一些特殊变量的含义**

- `$(MAKE)`
  - 当前 make 解释器的文件名

- `$(MAKECMDGOALS)`
  - 命令行中指定的目标名（make 的命令行参数）

- `$(MAKEFILE_LIST)`
  - make 所需要处理的 makefile 文件列表
  - 当前 makefile 的文件名总是位于列表的最后
  - 文件名之间以空格进行分隔

> *[5-predefined_variable/5.3](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/5-predefined_variable/5.3)*

```makefile
# 5-predefined_variable/5.3/makefile

.PHONY : all out first second third

all out : first second third
	@echo "$(MAKE)"
	@echo "$(MAKECMDGOALS)"
	@echo "$(MAKEFILE_LIST)"

first :
	@echo "first"

second :
	@echo "second"

third :
	@echo "third"

test :
	@$(MAKE) first
	@$(MAKE) second
	@$(MAKE) third
```

- 执行命令：`make`

```bash
☁  5.3 [master] ⚡  make        
first
second
third
make

 makefile
```

- 执行命令：`make all`

```bash
☁  5.3 [master] ⚡  make all    
first
second
third
make
all
 makefile
```

- 执行命令：`make all out`

```bash
☁  5.3 [master] ⚡  make all out
first
second
third
make
all out
 makefile
make
all out
 makefile
```

- 执行命令：`make test`

```bash
☁  5.3 [master] ⚡  make test
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/5-predefined_variable/5.3'
first
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/5-predefined_variable/5.3'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/5-predefined_variable/5.3'
second
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/5-predefined_variable/5.3'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/5-predefined_variable/5.3'
third
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/5-predefined_variable/5.3'
```

- `$(MAKE_VERSION)`
  - 当前 make 解释器的版本

- `$(CURDIR)`
  - 当前 make 解释器的工作目录

- `$(.VARIABLES)`
  - 所有已经定义的变量名列表（预定义变量和自定义变量）


> *[5-predefined_variable/5.4](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/5-predefined_variable/5.4)*

```makefile
# 5-predefined_variable/5.4/makefile

.PHONY : test1 test2

F_Name := Yang
L_Name := Fushou

test1 :
	@echo "$(MAKE_VERSION)"
	@echo "$(CURDIR)"
	@echo "$(.VARIABLES)"

test2 : 
	@echo "$(RM)"
```

- 执行命令：`make`

```bash
☁  5.4 [master] ⚡  make     
4.2.1
/home/fyang/wkspace/project_build_tuorial/make_lesson/5-predefined_variable/5.4
VSCODE_GIT_ASKPASS_MAIN <D ?F WSLENV .SHELLFLAGS CWEAVE ?D @D @F CURDIR SHELL RM CO _ PREPROCESS.F LINK.m LINK.o OUTPUT_OPTION COMPILE.cpp MAKEFILE_LIST GNUMAKEFLAGS VSCODE_INJECTION DBUS_SESSION_BUS_ADDRESS CC CHECKOUT,v CPP LINK.cc MAKE_HOST PATH LD TEXI2DVI YACC LSCOLORS COMPILE.mod XDG_RUNTIME_DIR ARFLAGS LINK.r LINT COMPILE.f LINT.c YACC.m LINK.p ZDOTDIR YACC.y AR VSCODE_IPC_HOOK_CLI .FEATURES TANGLE LS_COLORS GET %F DISPLAY COMPILE.F CTANGLE WSL2_GUI_APPS_ENABLED .LIBPATTERNS LINK.C PWD .LOADED LINK.S PREPROCESS.r WSL_INTEROP PULSE_SERVER LINK.c WT_SESSION LINK.s HOME LOGNAME ZSH ^D MAKELEVEL VSCODE_GIT_IPC_HANDLE COMPILE.m COLORTERM MAKE SHLVL AS PREPROCESS.S COMPILE.p MAKE_VERSION USER FC .DEFAULT_GOAL NAME LESS %D F_Name WEAVE MAKE_COMMAND LINK.cpp F77 OLDPWD VSCODE_GIT_ASKPASS_EXTRA_ARGS TERM_PROGRAM .VARIABLES PC *F VSCODE_GIT_ASKPASS_NODE COMPILE.def LEX MAKEFLAGS MFLAGS *D TERM_PROGRAM_VERSION LEX.l LEX.m +D COMPILE.r MAKE_TERMOUT +F M2C L_Name WT_PROFILE_ID GIT_ASKPASS MAKEFILES COMPILE.cc <F CXX USER_ZDOTDIR COFLAGS PAGER COMPILE.C HOSTTYPE ^F COMPILE.S LINK.F SUFFIXES COMPILE.c WAYLAND_DISPLAY COMPILE.s .INCLUDE_DIRS .RECIPEPREFIX MAKEINFO MAKE_TERMERR OBJC TEX LANG TERM F77FLAGS LINK.f WSL_DISTRO_NAME
```

- 执行命令：`make test2`

```bash
☁  5.4 [master] ⚡  make test2
rm -f
```

> *[5-predefined_variable/5.5](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/5-predefined_variable/5.5)*

- 目录结构

```shell
.
├── clean
├── func.c
├── main.c
└── makefile
```

```makefile
# 5-predefined_variable/5.5/makefile

CC := g++
TARGET := hello-world.out

$(TARGET) : func.o main.o
	$(CC) -o $@ $^

func.o : func.c
	$(CC) -o $@ -c $^

main.o : main.c
	$(CC) -o $@ -c $^

.PHONY : rebuild clean all

rebuild : clean all

all : $(TARGET)

clean : 
	$(RM) *.o $(TARGET)
```

**小结：**

- makefile 提供了预定义变量供开发者使用
- 预定义变量的使用能够使得 makefile 的开发更高效
- 自动变量是 makefile 中最常见的元素
- 使用 `$(.VARIABLES)`  能够获取所有的特殊变量


## 6. 变量的高级主题（上）

**变量值的替换**

- 使用指定字符（串）替换变量值中的后缀字符串
- 语法格式：`$(var:a=b)` 或 `${var:a=b}` 
  - 替换表达式中不能有任何的空格
  - make 中支持使用 `${}` 对变量进行取值

```makefile
src := a.cc b.cc c.cc
obj := $(src:cc=o)

test:
	@echo "obj => $(obj)"
```

**变量的模式替换**

- 使用 `%` 保留变量值中的指定字符，替换其它字符
- 语法格式：`$(var:a%b=x%y)` 或 `${var:a%b=x%y}` 
  - 替换表达式中不能有任何的空格
  - make 中支持使用 `${}` 对变量进行取值

```makefile

src := a1b.c a2b.c a3b.c
obj := $(src:a%b.c=x%y.c)

test:
	@echo "obj => $(obj)"
```

**规则中的模式替换**

```
targets : target-pattern : prereq-pattern
	command1
	command2
	...
```

> **意义：** 通过 `target-pattern` 从 `targets` 中匹配子目标；再通过 `prereq-pattern` 从子目标生成依赖；进而构成完整的规则

```makefile
OBJS := func.o main.o

$(OBJS) : %.o : %.c
	gcc -o $@ -c $^
```


```makefile
func.o : func.c
	gcc -o $@ -c $^

main.o : main.c
	gcc -o $@ -c $^
```


> *[6-variable/6.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/6-variable/6.1/makefile)*

```makefile

# 6-variable/6.1/makefile

src1 := a.cc b.cc c.cc
obj1 := $(src1:cc=o)

test1 : 
	@echo "obj1 => $(obj1)"

src2 := a1b.c a22b.c a333b.c
obj2 := $(src2:a%b.c=x%y)

test2 : 
	@echo "obj2 => $(obj2)"
```

- 执行命令：`make test1`

```bash
☁  6.1 [master] ⚡  make test1
obj1 => a.o b.o c.o
```

- 执行命令：`make test2`

```bash
☁  6.1 [master] ⚡  make test2
obj2 => x1y x22y x333y
```

> *[6-variable/6.2](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/6-variable/6.2)*

- 目录结构

```shell
☁  6.2 [master] ⚡  tree
.
├── const.c
├── func.c
├── main.c
└── makefile
```

```makefile

# 6-variable/6.2/makefile

CC := g++
TARGET := hello-makefile.out
OBJS := func.o main.o const.o

$(TARGET) : $(OBJS)
	$(CC) -o $@ $^

$(OBJS) : %.o : %.c 
	$(CC) -o $@ -c $^

.PHONY : rebuild clean all

rebuild : clean all

all : $(TARGET)

clean : 
	$(RM) *.o $(TARGET)
```

- 执行命令：`make`

```bash
☁  6.2 [master] ⚡  make      
g++ -o func.o -c func.c
g++ -o main.o -c main.c
g++ -o const.o -c const.c
g++ -o hello-makefile.out func.o main.o const.o
```

**变量值的嵌套引用**

- 一个变量名之中可以包含对其它变量的引用
- 嵌套引用的本质是使用一个变量表示另外一个变量

```
x := y
y := z        ===>  a := $(y)  ===>  a := z
a := $($(x))
```

**命令行变量**

- 运行 make 时，在命令行定义变量
- 命令行变量默认覆盖 makefile 中定义的变量

```makefile
hm := hello makefile

test :
	@echo "hm => $(hm)"
```

```bash
make hm=cmd
```

- output

```bash
hm => cmd
```

**`override` 关键字**

- 用于指示 makefile 中定义的变量不能被覆盖
- 变量的定义和赋值都需要使用 `override` 关键字


```makefile
override var := test
test:
	@echo "var => $(var)"
```

```bash
make var=cmd
```

- output

```bash
hm => test
```

**`define` 关键字**

- 用于在 makefile 中定义多行变量
- 多行变量的定义从变量名开始到 `endef` 结束
- 可使用 `override` 关键字防止变量被覆盖
- `define` 定义的变量等价于使用 `=` 定义的变量

```makefile
define foo
I'm fool!
endef
```

```makefile
override define cmd
	@echo "run cmd ls ..."
	@ls
endef
```

> *[6-variable/6.3](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/6-variable/6.3/makefile)*

```makefile

# 6-variable/6.3/makefile

hm := hello makefile

override var := override-test

define foo
I'm fool!
endef

override define cmd
	@echo "run cmd ls ..."
	@ls
endef

test : 
	@echo "hm => $(hm)"
	@echo "var => $(var)"
	@echo "foo => $(foo)"
	${cmd}
```

- 执行命令：`make`

```bash
☁  6.3 [master] ⚡  make
hm => hello makefile
var => override-test
foo => I'm fool!
run cmd ls ...
makefile
```

- 执行命令：`make foo=cmd-foo`

```bash
☁  6.3 [master] ⚡  make foo=cmd-foo
hm => hello makefile
var => override-test
foo => cmd-foo
run cmd ls ...
makefile
```

**小结：**

- 变量值的替换：`$(var:a=b)` 或 `${var:a=b}`
- 变量的模式替换：`$(var:a%b=x%y)` 或 `${var:a%b=x%y}`
- makefile 支持将模式替换可以直接用于规则中
- makefile 中的变量值能够嵌套引用
- 命令行中定义的变量能够覆盖 makefile 中定义的变量
- `override` 用于指示 maekfile 中定义的变量不能被覆盖
- `define` 用于在 makefile 中定义值为多行变量

## 7. 变量的高级主题（下）

**环境变量（全局变量）**

- makefile 中能够直接使用环境变量的值
  - 定义了同名变量，环境变量将被覆盖
  - 运行 make 时指定 `-e` 选项，优先使用环境变量

为什么要在 makefile 中使用环境变量？

- 优势
  - 环境变量可以在所有 makefile 中使用

- 劣势
  - 过多的依赖环境变量会导致移植性降低

**变量在不同 makefile 之间的传递方式**

- 直接在外部定义环境变量进行传递
- 使用 `export` 定义变量进行传递（定义临时环境变量）
- 定义 make 命令行变量进行传递（推荐）

> *[7-variable/7.0/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/7-variable/7.0/makefile)*

```makefile

# 7-variable/7.0/makefile

JAVA_HOME := java home
export var := Fyang

new := fyang0906

test : 
	@echo "JAVA_HOME => $(JAVA_HOME)"
	@echo "make another file ..."
	@$(MAKE) -f makefile.2
	@$(MAKE) -f makefile.2 new:=$(new)

```

> *[7-variable/7.0/makefile.2](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/7-variable/7.0/makefile.2)*

```makefile
# 7-variable/7.0/makefile.2

test : 
	@echo "JAVA_HOME => $(JAVA_HOME)"
	@echo "var => $(var)"
	@echo "new => $(new)"
```

- 执行命令：`make`

```bash
☁  7.0 [master] ⚡  make
JAVA_HOME => java home
make another file ...
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/7-variable/7.0'
JAVA_HOME => 
var => Fyang
new => 
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/7-variable/7.0'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/7-variable/7.0'
JAVA_HOME => 
var => Fyang
new => fyang0906
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/7-variable/7.0'
```

**目标变量（局部变量）**

- 作用域只在指定目标及连带规则中
  - target: name \<assignment> value
  - target: override name \<assignament> value

> *[7-variable/7.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/7-variable/7.1/makefile)*

```makefile
var := Fyang

test : var := test-var

test : 
	@echo "test :"
	@echo "var => $(var)"
```

- 执行命令：`make`

```bash
☁  7.1 [master] ⚡  make
test :
var => test-var
```

> *[7-variable/7.2/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/7-variable/7.2/makefile)*

```makefile

# 7-variable/7.2/makefile

var := Fyang

test : var := test-var

test : another
	@echo "test :"
	@echo "var => $(var)"

another : 
	@echo "another :"
	@echo "var => $(var)"
```

- 执行命令：`make`

```bash
☁  7.2 [master] ⚡  make
another :
var => test-var
test :
var => test-var
```

- 执行命令：`make another`

```bash
☁  7.2 [master] ⚡  make another
another :
var => Fyang
```

**模式变量**

- 模式变量是目标变量的扩展
- 作用域只在符合模式的目标及连带规则中
  - pattern: name \<assignment> value
  - pattern: override name <assignment> value

> *[7-variable/7.3/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/7-variable/7.3/makefile)*


```makefile

# 7-variable/7.3/makefile

var := Fyang
new := Fushou

test : var := test-var
%e : override new := test-new

test : another
	@echo "test :"
	@echo "var => $(var)"
	@echo "new => $(new)"

another : 
	@echo "another :"
	@echo "var => $(var)"
	@echo "new => $(new)"

rule : 
	@echo "rule :"
	@echo "var => $(var)"
	@echo "new => $(new)"
```

- 执行命令：`make test rule`

```bash
☁  7.3 [master] ⚡  make test rule
another :
var => test-var
new => Fushou
test :
var => test-var
new => Fushou
rule :
var => Fyang
new => test-new
```

- 执行命令：`make test rule new:=cmd-new`

```bash
☁  7.3 [master] ⚡  make test rule new:=cmd-new
another :
var => test-var
new => cmd-new
test :
var => test-var
new => cmd-new
rule :
var => Fyang
new => test-new
```

**小结**

makefile 中的三种变量
- 全局变量：makefile 外部定义的环境变量
- 文件变量：makefile 中定义的变量
- 局部变量：指定目标的变量

## 8. 条件判断语句

**makefile 中支持条件判断语句**

- 可以根据条件的值来决定 make 的执行
- 可以比较两个不同变量或者变量和常量值

```makefile
ifxxx (arg1,arg2)
	# for ture
else
	# for false
endif
```

> 注意事项
>
> 条件判断语句只能用于控制 make 实际执行的语句；但是，不能控制规则中命令的执行过程

- 条件判断语句的语法说明
  - 常用形式
    - `ifxxx (arg1,arg2)`
  - 其他合法形式
    - `ifxxx "arg1" "arg2"`
    - `ifxxx 'arg1' 'arg2'`
    - `ifxxx "arg1" 'arg2'`
    - `ifxxx 'arg1' "arg2"`

> **小贴士：**

```
              不能使用空格
                  ↓
              _________
____ ifxxx _ (arg1,arg2) _ 
  ↖       ↗             ↗
   \    /-------------/
    \  /
 可以使用空格
```

- 条件判断关键字

| 关键字  | 功能                                                  |
| -      | -                                                    |
| `ifeq`   | 判断参数**是否相等**，相等为 `true`，否则为 `false`      |
| `ifneq`  | 判断参数**是否不相等**，不相等为 `true`，否则为 `false`   |
| `ifdef`  | 判断变量**是否有值**，有值为 `true`，否则为 `false`      |
| `ifndef` | 判断变量**是否没有值**，没有值为 `true`，否则为 `false`   |


> *[8-conditional_judgment_statement/8.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/8-conditional_judgment_statement/8.1/makefile)*

```makefile

# 8-conditional_judgment_statement/8.1/makefile

.PHONY : test

var1 := A
var2 := $(var1)
var3 :=

test : 
    ifeq ($(var1),$(var2))
		@echo "var1 == var2"
    else
		@echo "var1 != var2"
    endif
    
    ifneq ($(var2),)
		@echo "var2 is NOT empty"
    else
		@echo "var2 is empty"
    endif

    ifdef var2
		@echo "var2 is NOT empty"
    else
		@echo "var2 is empty"
    endif

    ifndef var3
		@echo "var3 is empty"
    else
		@echo "var3 is NOT empty"
    endif
```

- 执行命令：`make`

```bash
☁  8.1 [master] ⚡  make
var1 == var2
var2 is NOT empty
var2 is NOT empty
var3 is empty
```

**一些工程经验**

- 条件判断语句之前可以有空格，但不能有 **Tab** 字符 `\t`
- 在条件语句中不要使用自动变量（`$@`, `$^`, `$<`）
- 一条完整的条件语句必须位于同一个 makefile 中
- 条件判断类似 C 语言中的宏，预处理阶段有效，执行阶段无效
- make 在加载 makefile 时
  - 首先计算表达式的值（赋值方式不同，计算方式不同）
  - 根据判断语句的表达式决定执行的内容

> 下面代码的输出相同吗？

```makefile
var1 :=
var2 := $(var1)

test:
    ifdef var1
		@echo "var1 is defined"
    endif
    ifdef var2
		@echo "var2 is defined"
    endif
```

```makefile
var1 =
var2 = $(var1)

test:
    ifdef var1
		@echo "var1 is defined"
    endif
    ifdef var2
		@echo "var2 is defined"
    endif
```

> 实验示例：*[8-conditional_judgment_statement/8.2/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/8-conditional_judgment_statement/8.2/makefile)*

```makefile

# 8-conditional_judgment_statement/8.2/makefile

.PHONY : test

var1 := 
var2 := $(var1)

var3 =
var4 = $(var3)
# var3 = 3

test : 
    ifdef var1
		@echo "var1 is defined"
    else
		@echo "var1 is NOT defined"
    endif

    ifdef var2
		@echo "var2 is defined"
    else
		@echo "var2 is NOT defined"
    endif

    ifdef var3
		@echo "var3 is defined"
    else
		@echo "var3 is NOT defined"
    endif

    ifdef var4
		@echo "var4 is defined"
    else
		@echo "var4 is NOT defined"
    endif
```

- 执行命令：`make`

```bash
var1 is NOT defined
var2 is NOT defined
var3 is NOT defined
var4 is defined
```

**小结**

- 条件判断根据条件的值来决定 make 的执行
- 条件判断可以比较两个不同变量或者变量和常量值
- 条件判断在预处理阶段有效，执行阶段无效
- 条件判断不能控制规则中命令的执行过程

## 9. 函数定义及调用

**makefile 中支持函数的概念**

- make 解释器提供了一系列的函数供 makefile 调用
- 在 makefile 中支持自定义函数实现，并调用执行
- 通过 `define` 关键字实现自定义函数

**自定义函数的语法**

- 函数定义

```makefile
define func1
	@echo "My name is $(0)."
endef
```

```makefile
define func2
	@echo "My name is $(0)."
	@echo "Param => $(1)"
endef
```

- 函数调用

```makefile
test:
	$(call func1)
	$(call func2, "Hello!")
```

**深入理解自定义函数**

- 自定义函数是一个多行变量，无法直接调用
- 自定义函数是一种过程调用，没有任何的返回值
- 自定义函数用于定义命令集合，并应用于规则中

> *[9-function/9.0/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/9-function/9.0/makefile)*

```makefile

# 9-function/9.0/makefile

.PHONY : test

define func1
	@echo "My name is $(0)"
endef

define func2
	@echo "My name is $(0)"
	@echo "Param 1 => $(1)"
	@echo "Param 2 => $(2)"
endef

test :
	$(call func1)
	$(call func2, Hello)
```

- 执行命令：`make`

```bash
☁  9.0 [master] ⚡  make
My name is func1
My name is func2
Param 1 =>  Hello
Param 2 => 
```

> *[9-function/9.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/9-function/9.1/makefile)*

```makefile

# c_learning/Makefile/Makefile_lesson/9-function/9.1/makefile

.PHONY : test

define func1
	@echo "My name is $(0)"
endef

define func2
	@echo "My name is $(0)"
	@echo "Param 1 => $(1)"
	@echo "Param 2 => $(2)"
endef

var := $(call func1)
new := $(func1)

test : 
	@echo "var => $(var)"
	@echo "new => $(new)"
	$(call func1)  # echo "My name is func1"
	$(call func2, "Hello world!", fyang)
```

- 执行命令：`make`

```bash
☁  9.1 [master] ⚡  make
var =>  @echo My name is func1
new =>  @echo My name is 
My name is func1
My name is func2
Param 1 =>  Hello world!
Param 2 =>  fyang
```

**make 解释器中的预定义函数**

- make 的函数提供了处理文件名，变量和命令的函数
- 可以在需要的地方调用函数来处理指定的参数
- 函数在调用的地方被替换为处理结果

**预定义函数的调用**

```
var := $(func_name arg1,arg2,...)
---      --------  -------------
返回值      函数名      函数参数

----------------------------------

var := $(abspath ./)

test :
	@echo "var => $(var)"
```

> **问题：** 为什么自定义函数和预定义函数的调用形式完全不同？

**本质剖析**

- makefile 中不支持真正意义上的自定义函数
- 自定义函数的本质是多行变量
- 预定义的 `call` 函数在调用时将参数传递给多行变量
- 自定义函数是 `call` 函数的实参，并在 `call` 中被执行

> 函数剖析：*[9-function/9.2/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/9-function/9.2/makefile)*

```makefile

# 9-function/9.2/makefile

.PHONY : test

define func1
	@echo "My name is $(0)"
endef

func2 := @echo "My name is $(0)"

test :
	$(call func1)
	$(call func2)
```

- 执行命令：`make`

```bash
☁  9.2 [master] ⚡  make
My name is func1
My name is 
```

> *[9-function/9.3/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/9-function/9.3/makefile)*


```makefile

# 9-function/9.3/makefile

.PHONY : test

define func1
	@echo "My name is $(0)"
endef

define func2
	@echo "My name is $(0)"
endef

test :
	$(call func1)
	$(call func2)
```

- 执行命令：`make`

```bash
☁  9.3 [master] ⚡  make
My name is func1
My name is func2
```

> *[9-function/9.4/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/9-function/9.4/makefile)*


```makefile

# 9-function/9.4/makefile

.PHONY : test

define func1
	@echo "My name is $(0)"
endef

define func2
	@echo "My name is $(0)"
endef

var1 := $(call func1)
var2 := $(call func2)
var3 := $(abspath ./)
var4 := $(abspath test.cpp)

test : 
	@echo "var1 => $(var1)"
	@echo "var2 => $(var2)"
	@echo "var3 => $(var3)"
	@echo "var4 => $(var4)"
```

- 执行命令：`make`

```bash
☁  9.4 [master] ⚡  make
var1 =>         @echo My name is func1
var2 =>         @echo My name is func2
var3 => /home/fyang/wkspace/project_build_tuorial/make_lesson/9-function/9.4
var4 => /home/fyang/wkspace/project_build_tuorial/make_lesson/9-function/9.4/test.cpp
```

**小结**

- make 解释器提供了一系列的函数供 makefile 调用
- 自定义函数是一个多行变量，无法直接调用
- 自定义函数用于定义命令集合，并应用于规则中
- 预定义的 `call` 函数在调用时将参数传递给多行变量
- 自定义函数是 `call` 函数的实参，并在 `call` 中被执行

## 10. 变量与函数的综合示例

**实战需求**

- 自动生成 `target` 文件夹存放可执行文件
- 自动生成 `objs` 文件夹存放编译生成的目标文件 `*.o`
- 支持调试版本的编译选项
- 考虑代码的扩展性

**工具原料**

- `$(wildcard _pattern)`
  - 获取当前工作目录中满足 `_pattern` 的文件或目录列表

- `$(addprefix _prefix, _names)`
  - 给名字列表 `_names` 中的每一个名字增加前缀 `_prefix`

**关键技巧**

- 自动获取当前目录下的源文件列表（函数调用）
  - `SRCS := $(wildcard *.c)`


- 根据源文件列表生成目标文件列表（变量的值替换）
  - `OBJS := $(SRCS:.c=.o)`


- 对每一个目标文件列表加上路径前缀（函数调用）
  - `OBJS := $(addprefix path/, $(OBJS))`

**规则中的模式替换（目录结构）**

```
工作目录中存在 `func.c` 和 `main.c`
--------------------------------
               ↓
%.o : %.c
	gcc -o $@ -c $^
--------------------------------
               ↓
func.o : func.c
	gcc -o $@ -c $^
main.o : main.c
	gcc -o $@ -c $^
```

> **注意** 
> 
> - `%.c` 用于模式匹配当前目录下的文件
>
> - `%.o` 用于将 `%.c` 匹配成功的文件名进行模式替换，`.c` 替换为 `.o`



**编译规则的依赖**

```
                             +--------------------+
                           / | dirs(创建文件夹)     |
                          /  +--------------------+
+-----+       +--------+ /   +--------------------+
| all |  -->  | target | --> | objs(编译目标文件)   |
+-----+       +--------+ \   +--------------------+
                          \  +--------------------+
                           \ | 生成可执行文件(链接)  |
						     +--------------------+
```

> 编程示例：*[10-var_func_sample](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/10-var_func_sample)*

- 目录结构

```shell
.
├── const.c
├── func.c
├── main.c
└── makefile
```

> *10-var_func_sample/const.c*

```C
const char* g_hello = "hello makefile";
```

> *10-var_func_sample/func.c*

```C
#include <stdio.h>

extern char* g_hello;

void foo()
{
    printf("void foo() : %s\n", g_hello);
}
```

> *10-var_func_sample/main.c*

```C
#include <stdio.h>

extern void foo();

int main(void)
{
    foo();

    return 0;
}
```

> *10-var_func_sample/makefile*

```makefile

# 10-var_func_sample/makefile

CC := gcc
MKDIR := mkdir
RM := rm -rf

DIR_OBJS := objs
DIR_TARGET := target

DIRS := $(DIR_OBJS) $(DIR_TARGET)

TARGET := $(DIR_TARGET)/hello-makefile.out

# main.c const.c func.c
SRCS := $(wildcard *.c)

# main.o const.o func.o
OBJS := $(SRCS:.c=.o)

# obj/main.o objs/const.o objs/func.o
OBJS := $(addprefix $(DIR_OBJS)/, $(OBJS))


.PHONY : rebuild clean all

$(TARGET) : $(DIRS) $(OBJS)
	$(CC) -o $@ $(OBJS)
	@echo "Target File ==> $@"

$(DIRS) :
	$(MKDIR) $@

$(DIR_OBJS)/%.o : %.c
    ifeq ($(DEBUG),true)
		$(CC) -o $@ -g -c $^
    else
		$(CC) -o $@ -c $^
    endif

rebuild : clean all

all : $(TARGET)

clean :
	$(RM) $(DIRS)
```

- 执行命令：`make`

```bash
☁  10-var_func_sample [master] ⚡  make      
mkdir objs
mkdir target
gcc -o objs/const.o -c const.c
gcc -o objs/main.o -c main.c
gcc -o objs/func.o -c func.c
gcc -o target/hello-makefile.out objs/const.o objs/main.o objs/func.o
Target File ==> target/hello-makefile.out
```

> 目录结构

```
.
├── const.c
├── func.c
├── main.c
├── makefile
├── objs
│   ├── const.o
│   ├── func.o
│   └── main.o
└── target
    └── hello-makefile.out

2 directories, 8 files
```

- 编译 Debug 版本：`make DEBUG:=true`

```shell
☁  10-var_func_sample [master] ⚡  make DEBUG:=true
mkdir objs
mkdir target
gcc -o objs/const.o -g -c const.c
gcc -o objs/main.o -g -c main.c
gcc -o objs/func.o -g -c func.c
gcc -o target/hello-makefile.out objs/const.o objs/main.o objs/func.o
Target File ==> target/hello-makefile.out
```

**小结**

- 目录可以成为目标的依赖，在规则中创建目录
- 预定义函数是 `makefile` 实战不可或缺的部分
- 规则中的模式匹配可以直接针对目录中的文件
- 可以使用命令行变量编译特殊的目标版本


## 11. 自动生成依赖关系（上）

**问题**

- 目标文件 `.o` 是否只依赖于源文件 `.c` ？
- 编译器如何编译源文件和头文件？

```
               +--------+
               | test.c |
+----------+  /+--------+
|          | /
| test.out |   
|          | \ +--------+
+----------+  \| test.h |
               +--------+

```

**编译行为带来的缺陷**

- 预处理器将头文件中的代码直接插入源文件
- 编译器只能通过预处理后的源文件产生目标文件
- 因此，**规则中以源文件为依赖，命令可能无法执行**

**下面的 `makefile` 又没问题？**

```makefile
OBJS := func.o main.o

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ===> $@"

$(OBJS): %.o : %.c
	@gcc -o $@ -c $^
```

```
       +--------+
       | func.h |
       +--------+
       /        \
 +--------+  +--------+
 | main.c |  | func.c |
 +--------+  +--------+

```

> *[11-auto_generate_dependencies](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/11-auto_generate_dependencies)*

- 目录结构

```shell
.
├── func.c
├── func.h
├── main.c
└── makefile
```

> *11-auto_generate_dependencies/func.h*

```C
#ifndef FUNC_H
#define FUNC_H

#define HELLO "Hello Makefile!!!"

void foo();

#endif
```

> *11-auto_generate_dependencies/func.c*

```shell
#include <stdio.h>
#include "func.h"

void foo()
{
    printf("void foo(): %s\n", HELLO);
}
```

> *11-auto_generate_dependencies/main.c*

```shell
#include <stdio.h>
#include "func.h"

int main()
{
    foo();

    return 0;
}
```

> *11-auto_generate_dependencies/makefile*

```shell
OBJS := func.o main.o

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ===> $@"

$(OBJS): %.o : %.c
	@gcc -o $@ -c $^

.PHONY : clean

clean:
	$(RM) $(OBJS) *.out
```

- 编译：`make`

```shell
☁  11-auto_generate_dependencies [master] ⚡  ./hello.out
void foo(): Hello Makefile!!!
```

- 运行：`./hello.out`

```shell
☁  11-auto_generate_dependencies [master] ⚡  ./hello.out
void foo(): Hello Makefile!!!
```

- 修改 `func.h` 

```C
#ifndef FUNC_H
#define FUNC_H

#define HELLO "Hello World!!!"

void foo();

#endif
```

- 编译：`make`

```shell
☁  11-auto_generate_dependencies [master] ⚡  make
make: 'hello.out' is up to date.
```

- 运行：`./hello.out`

```shell
☁  11-auto_generate_dependencies [master] ⚡  ./hello.out
void foo(): Hello Makefile!!!
```

> 如上输出信息可知，修改 `func.h` 文件中的内容后，依赖该头文件的目标文件 `make` 编译时并未更新，修改 makefile 添加目标文件依赖的头文件 `func.h`

> *11-auto_generate_dependencies/makefile*

```makefile
OBJS := func.o main.o

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ===> $@"

$(OBJS): %.o : %.c func.h
	@gcc -o $@ -c $<

.PHONY : clean

clean:
	$(RM) $(OBJS) *.out
```

- 编译：`make`

```shell
☁  11-auto_generate_dependencies [master] ⚡  make
Target File ===> hello.out
```

- 运行：`./hello.out`

```shell
☁  11-auto_generate_dependencies [master] ⚡  ./hello.out
void foo(): Hello World!!!
```

**实验中解决方案的问题**

- 头文件作为依赖出现于每个目标对应的规则中
- 当头文件改动，任何源文件都将被重新编译（编译低效）
- 当项目中头文件数量巨大时， `makefile` 将很难维护

**解决方案**

- 通过命令**自动生成**对头文件的依赖
- 将生成的依赖**自动包含**进 `makefile` 中
- 当头文件改动后，**自动确认**需要重新编译的文件

**预备工作（原材料）**

- Linux 命令 `sed`
- 编译器依赖生成选项 `gcc -MM` (`gcc -M`)

**Linux 中的 `sed` 命令**

- `sed` 是一个流编辑器，用于流文本的修改（增/删/查/改）
- `sed` 可用于流文本中的字符串替换
- `sed` 的字符串替换方式为: `sed 's:src:des:g'`

```
echo "test=>abc+abc=abc" | sed 's:abc:xyz:g'
--------------------------------------------
	|
	+----→  test==>xyz+xyz=xyz
```

**`sed` 正则表达式支持**

- 在 `sed` 中可以用正则表达式匹配替换目标
- 并且可以使用匹配的目标生成替换结果


```
sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g'
       -------------- ------------
	          ↓             ↓
      正则表达式匹配目标 将匹配结果进行替换
```

**`gcc` 关键编译选项**

- 生成依赖关系
  - 获取目标的完整依赖关系
    - `gcc -M test.c`

  - 获取目标的部分依赖关系
    - `gcc -MM test.c`


> 测试示例

- 执行命令

```bash
echo "test=>abc+abc=abc"
```

- output

```bash
test=>abc+abc=abc
```

- 执行命令

```bash
echo "test=>abc+abc=abc" | sed 's:abc:xyz:g'
```

- output

```bash
test=>xyz+xyz=xyz
```

- 执行命令

```bash
echo "main.o : main.c func.c"
```

- output

```bash
main.o : main.c func.c
```


- 执行命令

```bash
 echo "main.o : main.c func.c" | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g'
```

- output

```bash
objs/main.o : main.c func.c
```

- 执行命令

```bash
echo "a/b/c/d/main.o : main.c func.c" | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g'
```

- output

```bash
objs/a/b/c/d/main.o : main.c func.c
```

- 执行命令

```bash
echo "a/b/c/d/main.o : main.c func.c" | sed 's,\(.*\)\.o[ :]*,x/y/z/\1.o : ,g'
```

- output

```bash
x/y/z/a/b/c/d/main.o : main.c func.c
```


- 执行命令

```bash
echo "a/b/c/d/main.o :::::  main.c func.c" | sed 's,\(.*\)\.o[ :]*,x/y/z/\1.o : ,g'
```

- output

```bash
x/y/z/a/b/c/d/main.o : main.c func.c
```

- 执行命令

```bash
gcc -M main.c
```

- output

```bash
main.o: main.c /usr/include/stdc-predef.h /usr/include/stdio.h \
 /usr/include/x86_64-linux-gnu/bits/libc-header-start.h \
 /usr/include/features.h /usr/include/features-time64.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/bits/timesize.h \
 /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/long-double.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/lib/gcc/x86_64-linux-gnu/11/include/stddef.h \
 /usr/lib/gcc/x86_64-linux-gnu/11/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h \
 /usr/include/x86_64-linux-gnu/bits/time64.h \
 /usr/include/x86_64-linux-gnu/bits/types/__fpos_t.h \
 /usr/include/x86_64-linux-gnu/bits/types/__mbstate_t.h \
 /usr/include/x86_64-linux-gnu/bits/types/__fpos64_t.h \
 /usr/include/x86_64-linux-gnu/bits/types/__FILE.h \
 /usr/include/x86_64-linux-gnu/bits/types/FILE.h \
 /usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/floatn.h \
 /usr/include/x86_64-linux-gnu/bits/floatn-common.h func.h
```

- 执行命令

```bash
gcc -MM main.c
```

- output

```bash
main.o: main.c func.h
```


- 执行命令

```bash
gcc -MM -E main.c
```

- output

```bash
main.o: main.c func.h
```

- 执行命令

```bash
gcc -M -E main.c | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g'
```

- output

```bash
objs/main.o : main.c /usr/include/stdc-predef.h /usr/include/stdio.h \
 /usr/include/x86_64-linux-gnu/bits/libc-header-start.h \
 /usr/include/features.h /usr/include/features-time64.h \
 /usr/include/x86_64-linux-gnu/bits/wordsize.h \
 /usr/include/x86_64-linux-gnu/bits/timesize.h \
 /usr/include/x86_64-linux-gnu/sys/cdefs.h \
 /usr/include/x86_64-linux-gnu/bits/long-double.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs.h \
 /usr/include/x86_64-linux-gnu/gnu/stubs-64.h \
 /usr/lib/gcc/x86_64-linux-gnu/11/include/stddef.h \
 /usr/lib/gcc/x86_64-linux-gnu/11/include/stdarg.h \
 /usr/include/x86_64-linux-gnu/bits/types.h \
 /usr/include/x86_64-linux-gnu/bits/typesizes.h \
 /usr/include/x86_64-linux-gnu/bits/time64.h \
 /usr/include/x86_64-linux-gnu/bits/types/__fpos_t.h \
 /usr/include/x86_64-linux-gnu/bits/types/__mbstate_t.h \
 /usr/include/x86_64-linux-gnu/bits/types/__fpos64_t.h \
 /usr/include/x86_64-linux-gnu/bits/types/__FILE.h \
 /usr/include/x86_64-linux-gnu/bits/types/FILE.h \
 /usr/include/x86_64-linux-gnu/bits/types/struct_FILE.h \
 /usr/include/x86_64-linux-gnu/bits/stdio_lim.h \
 /usr/include/x86_64-linux-gnu/bits/floatn.h \
 /usr/include/x86_64-linux-gnu/bits/floatn-common.h func.h
```

- 执行命令

```bash
gcc -MM -E main.c | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g'
```

- output

```bash
objs/main.o : main.c func.h
```

**小技巧：拆分目标的依赖**

- 将目标的完整依赖拆分为多个部分依赖

```
.PHONY: a b c

test: a b c
	@echo "$^"

----------------------------------
          <===等价===>
----------------------------------
.PHONY: a b c

test: a b
test: b c
test:
	@echo "$^"
```

## 12. 自动生成依赖关系（中）

**makefile 中的 include 关键字**

- 类似 C 语言中的 `include`
- 将其他文件的内容原封不动的搬入当前文件

```
语法：include filename
---------------------
        |
		+----> 例：include foo.make *.mk $(var)
```

**make 对 include 关键字的处理方式**

- 在当前目录搜索或指定目录搜索目标文件
  - 搜索成功：将文件内容搬入当前 makefile 中
  - 搜索失败：产生警告
    - 以文件名作为目标查找并执行对应规则
    - 当文件名对应的规则不存在时，最终产生错误

**下面的代码怎么执行？为什么？**

> *[12.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/12-auto_generate_dependencies/12.1/makefile)*

```makefile
.PHONY : all

include test.txt

all : 
	@echo "this is $@"
test.txt :
	@echo "test.txt"
	@touch test.txt
```

- 执行命令：`make all`

```shell
☁  12.1 [master] ⚡  make all
this is all
```

- 执行命令：`make all`

```shell
☁  12.1 [master] ⚡  make    
this is other
```

**makefile 中命令的执行机制**

- 规则中的每个命令默认是在一个新的进程中执行 (Shell)
- 可以通过接续符（`;`）将多个命令组合成一个命令
- 组合的命令依次在同一个进程中被执行
- `set -e` 指定发生错误后立即退出执行

**下面的代码想要实现什么功能？有没有问题？**

> *[12.2/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/12-auto_generate_dependencies/12.2/makefile)*

```makefile
.PHONY : all

all : 
	mkdir test
	cd test
	mkdir subtest
```

- 执行命令：`make`

```bash
☁  12.2 [master] ⚡  make
mkdir test
cd test
mkdir subtest
```

- 目录结构

```bash
.
├── makefile
├── subtest
└── test
```

> *[12.3/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/12-auto_generate_dependencies/12.3/makefile)*


```makefile
.PHONY : all

all : 
	set -e;\
	mkdir test;\
	cd test;\
	mkdir subtest;\
```

- 执行命令：`make`

```bash
☁  12.3 [master] ⚡  make    
set -e;\
mkdir test;\
cd test;\
mkdir subtest;\
```

- 目录结构

```bash
.
├── makefile
└── test
    └── subtest
```

**解决方案的初步思路**

- 通过 `gcc -MM` 和 `sed` 得到 `.dep`
  - 技术点：规则中命令的连续执行

- 通过 include 指令包含所有的 `.dep` 依赖文件
  - 技术点：当 `.dep` 依赖文件不存在时，使用规则自动生成

> 项目示例：*[12-auto_generate_dependencies/12.4](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/12-auto_generate_dependencies/12.4)*

- 目录结构

```shell
.
├── func.c
├── func.h
├── main.c
└── makefile
```

```makefile
# 12.4/makefile

.PHONY : all clean deps

MKDIR := mkdir
RM := rm -rf
CC := gcc

SRCS := $(wildcard *.c)
DEPS := $(SRCS:.c=.dep)

-include $(DEPS)

all :
	@echo "all"

%.dep : %.c
	@echo "Creating $@ ..."
	@set -e;\
	$(CC) -MM -E $^ | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g' > $@

clean :
	$(RM) $(DEPS)
```

- 执行命令：`make all`

```bash
☁  12.4 [master] ⚡  make all
Creating func.dep ...
Creating main.dep ...
all
```

- 目录结构

```bash
.
├── func.c
├── func.dep
├── func.h
├── main.c
├── main.dep
└── makefile

```

## 13. 自动生成依赖关系（下）

**问题：**

如何在 makefile 中组织 `.dep` 文件到指定目录？

**解决思路**

- 当 include 发现 `.dep` 文件不存在：
  - 通过规则和命令创建 deps 文件
  - 将所有 `.dep` 文件创建到 deps 文件夹
  - `.dep` 文件中记录目标文件的依赖关系

**初步代码设计**

```makefile
$(DIR_DEPS) : 
	$(MKDIR) $@

$(DIR_DEPS)/%.dep : $(DIR_DEPS) %.c
	@echo "Creating $@ ..."
	@set -e;\
	$(CC) -MM -E $(filter %.c,$^) | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g' > $@
```

> *[13-auto_generate_dependencies/13.1](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/13-auto_generate_dependencies/13.1)*

- 目录结构

```shell
.
├── func.c
├── func.h
├── main.c
└── makefile
```

> *13.1/makefile*

```makefile
.PHONY : all clean deps

MKDIR := mkdir
RM := rm -rf
CC := gcc

DIR_DEPS := deps

SRCS := $(wildcard *.c)
DEPS := $(SRCS:.c=.dep)
DEPS := $(addprefix $(DIR_DEPS)/,$(DEPS))

-include $(DEPS)

all :
	@echo "all"

$(DIR_DEPS) : 
	$(MKDIR) $@

$(DIR_DEPS)/%.dep : $(DIR_DEPS) %.c
	@echo "Creating $@ ..."
	@set -e;\
	$(CC) -MM -E $(filter %.c, $^) | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g' > $@

clean :
	$(RM) $(DIR_DEPS)
```

- 执行命令: `make all`

```bash
☁  13.1 [master] ⚡  make all
mkdir deps
Creating deps/func.dep ...
Creating deps/main.dep ...
mkdir deps
mkdir: cannot create directory ‘deps’: File exists
all
```

**问题本质分析**

- `deps` 文件夹的时间属性会因为依赖文件创建而发生改变
- make 发现 `deps` 文件夹比对应的目标更新
- 触发相应规则的重新解析和命令的执行


**优化解决方案**

> *[13-auto_generate_dependencies/13.2](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/13-auto_generate_dependencies/13.2)*

```makefile
.PHONY : all clean deps

MKDIR := mkdir
RM := rm -rf
CC := gcc

DIR_DEPS := deps

SRCS := $(wildcard *.c)
DEPS := $(SRCS:.c=.dep)
DEPS := $(addprefix $(DIR_DEPS)/,$(DEPS))

all :
	@echo "all"

ifeq ("$(MAKECMDGOALS)", "all")
-include $(DEPS)
endif

ifeq ("$(MAKECMDGOALS)", "")
-include $(DEPS)
endif

$(DIR_DEPS) : 
	$(MKDIR) $@

ifeq ("$(wildcard $(DIR_DEPS))", "")
$(DIR_DEPS)/%.dep : $(DIR_DEPS) %.c
else
$(DIR_DEPS)/%.dep : %.c
endif
	@echo "Creating $@ ..."
	@set -e;\
	$(CC) -MM -E $(filter %.c, $^) | sed 's,\(.*\)\.o[ :]*,objs/\1.o : ,g' > $@

clean :
	$(RM) $(DIR_DEPS)
```

- 执行命令: `make all`

```bash
☁  13.2 [master] ⚡  make      
mkdir deps
Creating deps/func.dep ...
Creating deps/main.dep ...
all
```

**`include` 暗黑操作一：**

- 使用减号（`-`）不但关闭了 `include` 发出的警告，同时关闭了错误；当错误发生时 make 将忽略这些错误！

> *[13.3/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/13-auto_generate_dependencies/13.3/makefile)*

```makefile
.PHONY : all

include test.txt

all : 
	@echo "this is all"
```

- 执行命令：`make all`

```bash
☁  13.3 [master] ⚡  make all
makefile:3: test.txt: No such file or directory
make: *** No rule to make target 'test.txt'.  Stop.
```

```makefile
.PHONY : all

-include test.txt

all : 
	@echo "this is all"
```

- 执行命令：`make all`

```bash
☁  13.3 [master] ⚡  make all
this is all
```

**`include` 暗黑操作二：**

> *[13.4/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/13-auto_generate_dependencies/13.4/makefile)*


```makefile
.PHONY : all

-include test.txt

all : 
	@echo "this is all"

test.txt : 
	@echo "Creating $@ ..."
	@echo "other : ; @echo "this is other" " > test.txt
```

- 执行命令：`make all`

```bash
☁  13.4 [master] ⚡  make all
Creating test.txt ...
this is all
```

- 删除 `test.txt`： `rm test.txt`

```bash
rm test.txt
```

- 执行命令：`make`

```bash
☁  13.4 [master] ⚡  make    
Creating test.txt ...
this is other
```

**`include` 暗黑操作三：**

- 如果 include 包含的文件存在，之后还会发生什么？

> *[13-auto_generate_dependencies/13.5](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/13-auto_generate_dependencies/13.5)*


```makefile

# 13-auto_generate_dependencies/13.5/makefile

.PHONY : all

-include test.txt

all : 
	@echo "this is all"

test.txt : b.txt
	@echo "Creating $@ ..."
```

**关于 `include` 的总结**

- 当前目标文件不存在
  - 以文件名查找规则，并执行

- 当前目标文件不存在，且查找到的规则中创建了目标文件
  - 将创建成功的目标文件包含进当前 makefile

- 当前目标文件存在
  - 将目标文件包含进当前 makefile
  - 以目标文件名查找是否有相应的规则
    - YES：比较规则的依赖关系，决定是否执行规则的命令
    - NO：NULL（无操作）

- 当前目标文件存在，且目标对应的规则被执行
  - 规则中的命令更新了目标文件
  - make 重新包含目标文件，替换之前包含的内容

- 目标文件未被更新
  - NULL （无操作）

## 14. 自动生成依赖关系（续）

**疯狂想法的具体实现**

```
include :                         all :
           +------+                           +------+
           | deps |                           | exes |
          /+------+                          /+------+
+-------+/                        +--------+/ +------+
| %.dep |                ====>    | $(EXE) |--| objs |
+-------+\                        +--------+\ +------+
          \+-----+                           \+------+
		   | %.c |                            |  %.o |
		   +-----+                            +------+
```

**注意事项**

当前 `.dep` 文件生成后，如果动态的改变头文件间的依赖关系，那么 make 可能无法检测到这个改变，进而做出错误的编译决策。


**解决方案**

- 将依赖文件名作为目标加入自动生成的依赖关系中
- 通过 `include` 加载依赖文件时判断是否执行规则
- 在规则执行时重新生成依赖关系文件
- 最后加载新的依赖文件

**小结**

- makefile 中可以将目标的依赖拆分写到不同的地方
- `include` 关键字能够触发相应规则的执行
- 如果规则的执行导致依赖更新，可能导致再次解释执行相应规则
- 依赖文件也需要依赖于源文件得到正确的编译决策
- 自动生成文件间的依赖关系能够提高 makefile 的移植性

## 15. make 的隐式规则（上）

**问题：**

如果同一个目标的命令拆分的写到不同地方，会发生什么？

> 下面的程序怎么执行？为什么?

> *[15-implicit_rule/15.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/15-implicit_rule/15.1/makefile)*

```makefile
.PHONY : all

all : 
	@echo "command-1"

VAR := test

all: 
	@echo "all : $(VAR)"
```

- 执行命令：`make all`

```bash
make all
```

- output

```bash
☁  15.1 [master] make all
makefile:7: warning: overriding recipe for target 'all'
makefile:4: warning: ignoring old recipe for target 'all'
makefile:12: warning: overriding recipe for target 'all'
makefile:7: warning: ignoring old recipe for target 'all'
all : test
```

> **注意：** make 在处理 makefile 中多出定义同一个目标命令时，以最新定义的命令为准，忽略之前定义的目标命令

**makefile 中出现同名目标时**

- 依赖：
  - 所有的依赖将合并在一起，成为目标的最终依赖

- 命令：
  - 当多处出现同一目标的命令时，make 发出警告
  - 所有之前定义的命令被最后定义的命令取代

**注意事项**

当使用 `include` 关键字包含其他文件时，需要确保被包含文件中的同名目标只有依赖，没有命令；否则，同名目标的命令将被覆盖！

> *[15-implicit_rule/15.2/1.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/15-implicit_rule/15.2/1.mk)*

```Makefile
all : 
	@echo "this is command from 1.mk"
```


> *[15-implicit_rule/15.2/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/15-implicit_rule/15.2/makefile)*

```Makefile
.PHONY : all

VAR := test

all : 
	@echo "all : $(VAR)"

include 1.mk
```

- 执行命令：`make all`

```shell
☁  15.2 [master] ⚡  make all
1.mk:2: warning: overriding recipe for target 'all'
makefile:6: warning: ignoring old recipe for target 'all'
this is command from 1.mk
```

**什么是隐式规则（built-in rules）？**

- make 提供了一些常用的，例行的规则实现
- 当相应目标的规则未提供时，make 尝试使用隐式规则

**下面的 makefile 能成功编译吗？为什么？**

> *[15-implicit_rule/15.3/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/15-implicit_rule/15.3/makefile)*

```makefile
SRCS := $(wildcard *.c)
OBJS := $(SRCS:.c=.o)

app.out : $(OBJS)
	$(CC) -o $@ $^
	$(RM) $^
	@echo "Target ==> $@"
```

- 执行命令：`make`

```bash
☁  15.3 [master] ⚡  make    
cc    -c -o main.o main.c
cc    -c -o func.o func.c
cc -o app.out main.o func.o
rm -f main.o func.o
Target ==> app.out
```

> 查看 make 提供的预定义变量

```makefile
all : 
	@echo "$(.VARIABLES)"
```

**初探隐式规则**

- make 提供了生成目标文件的隐式规则
- 隐式规则会使用预定义变量完成编译工作
- 改变预定义变量将部分改变隐式规则的行为
- 当存在自定义规则时，不使用隐式规则

**小结**

- 当多处出现同一目标的命令时，只有最后定义的命令有效
- make 提供了一系列的隐式规则可以使用
- 当 makefile 中未定义相关规则时，尝试使用隐式规则
- 隐式规则中可能使用 make 中的预定义变量
- 改变预定义变量可部分改变定义规则的行为

## 16. make 的隐式规则（下）

**深入理解隐式规则**

- 当 make 发现目标的依赖不存在时
  - 尝试通过依赖名逐一查找隐式规则
  - 并且通过依赖名推导可能需要的源文件

```makefile
app.out : main.o func.o
	$(CC) -o $@ $^
```

1. 发现目标依赖不存在
2. 查找隐式规则，并自动推导出：`func.o : func.c`
3. 根据文件后缀自动推导编译命令：`cc -c -o func.o func.c`

**隐式规则的副作用**

- 编译行为难以控制
  - 大量使用隐式规则可能产生意想不到的编译行为

- 编译效率低下
  - make 从隐式规则和自定义规则中选择最终使用的规则


**隐式规则链**

当依赖的目标不存在时，make 会极力组合各种隐式规则对目标进行创建，进而产生意料之外的编译行为！

> 例：
> 需要名为 `N.o` 的目标：`N.y` --> `N.c` --> `N.o`

**问题**

make 提供了多少隐式规则？如何查看隐式规则？

**查看隐式规则**

- 查看所有：`make -p`
- 查看具体规则：`make -p | grep "XXX"`


- 执行命令

```bash
make -p | grep "%.o"
```

- output

```bash
%.out:
%.o:
%: %.o
%.o: %.c
%.o: %.cc
%.o: %.C
%.o: %.cpp
%.o: %.p
%.o: %.f
%.o: %.F
%.o: %.m
%.o: %.r
%.o: %.s
%.o: %.S
%.o: %.mod
%.out: %
```

**隐式规则的禁用**

- 局部禁用
  - 在 makefile 中自定义规则
  - 在 makefile 中定义模式（如：`%.o : %.p`）

- 全局禁用
  - `make -r`

**后缀规则简介**
- 后缀规则是旧式的 “模式规则”
- 可以通过后缀描述的方式自定义规则

```makefile
.cpp.o : 
	@echo "Suffix Rule"
	g++ -o $@ -c $^
```

> 通过 `.cpp` 文件产生 `.o` 文件    ==>    例：`func.cpp -> func.o`

**双后缀规则**

- 定义一对文件后缀（依赖文件后缀和目标文件后缀）
  - 如：`.cpp.o  <==>  %.o : %.cpp`

**单后缀规则**

- 定义单个文件后缀（源文件后缀）
  - 如：`.c  <==>  % : %.c`

**关于后缀规则的注意事项**

- 后缀规则中不允许有依赖
- 后缀规则必须有命令，否则无意义
- 后缀规则将逐步被模式规则取代

**小结**

- 隐式规则可能造成意想不到的编译行为
- 在实际工程项目中尽量不使用隐式规则
- 后缀规则是一种旧式的模式规则
- 后缀规则正逐步被模式规则取代

## 17. make 中的路径搜索（上）

**问题**

在实际的工程项目中，所有的源文件和头文件都放在同一个文件夹中吗？

**常用的源码管理方式**

```
.
└── Project
    ├── Module1
    │   ├── inc
    │   └── src
    └── Module2
        ├── inc
        └── src
```
项目中的 makefile 必须能够正确的定位文件和依赖的文件，最终编译产生可执行程序。

> *[17-path_search/17.0](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/17-path_search/17.0)*

- 目录结构

```
.
├── inc
│   └── func.h
├── makefile
└── src
    ├── func.c
    └── main.c
```

> *[17-path_search/17.0/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/17-path_search/17.0/makefile)*

```makefile
OBJS := func.o main.o

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ==> $@"

$(OBJS) : %.o : %.c func.h
	@gcc $(CFLAGS) -o $@ -c $^
```

- 执行命令：`make`

```bash
☁  17.0 [master] ⚡  make      
make: *** No rule to make target 'func.c', needed by 'func.o'.  Stop.
```

**特殊的预定义变量 `VPATH` (全大写)**

- `VPATH` 变量的值用于指示 `make` 如何查找文件
- 不同文件夹可作为 `VPATH` 的值同时出现
- 文件夹的名字之间需要使用分隔符进行区分

  - 例1：`VPATH := inc src` (空格)
  - 例2：`VPATH := inc;src` (分号)
  - 例3：`VPATH := inc:src` (冒号)

**make 对于 `VPATH` 值得处理方式**

- 当前文件夹找不到需要得文件时，`VPATH` 会被使用
- make 会在 `VPATH` 指定得文件夹中依次搜索文件
- **`当多个文件夹存在同名文件时，选择第一次搜索到的文件`**

> *[17-path_search/17.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/17-path_search/17.1/makefile)*

```Makefile
OBJS := func.o main.o

VPATH := inc src

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ==> $@"

$(OBJS) : %.o : %.c func.h
	@gcc -o $@ -c $<
```

- 执行命令：`make`

```bash
☁  17.1 [master] ⚡  make
src/func.c:2:10: fatal error: func.h: No such file or directory
    2 | #include "func.h"
      |          ^~~~~~~~
compilation terminated.
make: *** [makefile:13: func.o] Error 1
```

> **注意事项**
> - `VPATH` 只能决定 make 的搜索路径，无法决定命令的搜索路径
> - 对于特定的编译命令 (`gcc`)，需要独立指定编译搜索路径

```makefile
                                gcc -I include-path
                                ...... ............
编译选项，指定gcc的头文件搜索路径   <----+         +----> 路径参数，具体需要gcc搜索的路径
```

> *[17-path_search/17.2/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/17-path_search/17.2/makefile)*

```makefile
OBJS := func.o main.o

INC := inc
SRC := src

VPATH := $(INC) $(SRC)
CFLAGS := -I $(INC)

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ==> $@"

$(OBJS) : %.o : %.c func.h
	@gcc $(CFLAGS) -o $@ -c $<

```

- 执行命令：`make`

```bash
☁  17.2 [master] ⚡  make
Target File ==> hello.out
```

- 执行命令：`./hello.out`

```bash
☁  17.2 [master] ⚡  ./hello.out
void foo(): Hello World!
```

**`VPATH` 存在的问题**

当 `inc` 文件夹中意外出现源文件（C/Cpp 文件），那么可能产生编译错误！

> *[17-path_search/17.3](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/17-path_search/17.3)*

```
.
├── inc
│   ├── func.c
│   └── func.h
├── makefile
└── src
    ├── func.c
    └── main.c
```

> *[17-path_search/17.3/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/17-path_search/17.3/makefile)*

```makefile
OBJS := func.o main.o

INC := inc
SRC := src

VPATH := $(INC) $(SRC)
CFLAGS := -I $(INC)

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ==> $@"

$(OBJS) : %.o : %.c func.h
	@gcc $(CFLAGS) -o $@ -c $<
```

- 执行命令：`make`

```bash
☁  17.3 [master] ⚡  make
Target File ==> hello.out
```

- 执行命令：`./hello.out`

```bash
☁  17.3 [master] ⚡  ./hello.out
void foo(): This file is from inc...
```

**替换方案：`vpath` 关键字（全小写）**

- 为不同类型的文件指定不同的搜索路径
- 语法：
  - 在 Directory 中搜索符合 Pattern 的规则的文件

```
vpath Pattern Directory
------------------------
           |
		   +------->  例：
		                 vpath %.h inc
						 vpath %.c src
```

> *[17-path_search/17.4](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/17-path_search/17.4)*

```
.
├── inc
│   ├── func.c
│   └── func.h
├── makefile
└── src
    ├── func.c
    └── main.c
```

> *[17-path_search/17.4/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/17-path_search/17.4/makefile)*

```makefile
OBJS := func.o main.o

INC := inc
SRC := src
CFLAGS := -I $(INC)

vpath %.h $(INC)
vpath %.c $(SRC)

hello.out : $(OBJS)
	@gcc -o $@ $^
	@echo "Target File ==> $@"

# vpath %.h

$(OBJS) : %.o : %.c func.h
	@gcc $(CFLAGS) -o $@ -c $<
```

- 执行命令：`make`

```bash
☁  17.4 [master] ⚡  make
Target File ==> hello.out
```

- 执行命令：`./hello.out`

```bash
☁  17.4 [master] ⚡  ./hello.out
void foo(): Hello World!
```

**取消搜索规则**

- 取消已经设置的某个搜索规则
  - `vpath Pattern`
  - 例： 

```makefile
vpath %.h inc  # 在 inc 中搜索 .h 文件 
vpath %.h      # 不再到 inc 中搜索 .h 文件
```

- 取消所有已经设置的规则

```makefile
vpath
```

**小结**

- `VPATH` 变量用于指示 make 如何查找文件
- meke 会在 `VPATH` 指定的文件夹中依次搜索文件
- `vpath` 关键字可以为不同类型的文件指定不同的搜索路径
- `vpath` 比 `VPATH` 更灵活易用，可动态设置/取消搜索路径

## 18. make 中的路径搜索（下）

**问题一**

当 `VPATH` 和 `vpath` 同时出现时，make 会如何处理？

```makefile
VPATH := src1
vpath %.c src2

func.o : func.c
	@echo "$^"
```

下面的项目中会选择哪一个文件进行编译？

> *[18-path_search/18.1](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/18-path_search/18.1)*

- 目录结构

```bash
.
├── inc
│   └── func.h
├── main.c
├── makefile
├── src1
│   └── func.c
└── src2
    └── func.c
```

> *[18-path_search/18.1/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/18-path_search/18.1/makefile)*


```makefile
VPATH : src1
CFLAGS := -I inc

vpath %.c src2
vpath %.h inc

app.out : func.o main.o
	@gcc -o $@ $^
	@echo "target File ==> $@"

%.o : %.c func.h
	@gcc $(CFLAGS) -o $@ -c $<
```

- 执行命令：`make`

```shell
☁  18.1 [master] ⚡  make
target File ==> app.out
```

- 执行命令：`./app.out`

```shell
☁  18.1 [master] ⚡  ./app.out
void foo(): This file is from src2 ...
```

**实验结论**

- make 首先在当前文件夹搜索需要的文件
- 如果失败：
  - make 优先在 `vpath` 指定的文件夹中搜索目标文件
  - 当 `vpath` 搜索失败时，转而搜索 `VPATH` 指定的文件夹

```
VPATH := src1 src3
                           ==>    src2 -->  src1  --> src3
vpath %.c src2
```

**问题二**

当使用 vpath 对同一个 Pattern 指定多个文件夹时， make 会如何处理？

```makefile
vpath %.c src1
vpath %.c src2

func.o : func.c
	@echo "$^"
```

- 下面的项目中会选择哪个文件进行编译？

> *[18-path_search/18.2](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/18-path_search/18.2)*

```bash
.
├── inc
│   └── func.h
├── main.c
├── makefile
├── src1
│   └── func.c
└── src2
    └── func.c
```

```makefile
CFLAGS := -I inc

vpath %.c src1
vpath %.c src2
vpath %.h inc

app.out : func.o main.o
	@gcc -o $@ $^
	@echo "Target File ==> $@"

%.o : %.c func.h
	@gcc $(CFLAGS) -o $@ -c $<
```

- 执行命令： `make`

```shell
☁  18.2 [master] ⚡  make
target File ==> app.out
```

- 执行命令：`./app.out`

```shell
☁  18.2 [master] ⚡  ./app.out
void foo(): This file is from src1 ...
```

**实验结论**

- make 首先在当前文件夹搜索需要的文件
- 如果失败：
  - make 以自上而下的顺序搜索 vpath 指定的文件夹
  - 当找到目标文件，搜索结束

```
vpath %.c src3
vpath %.c src1    ==>    src3 -> src1 -> src2
vpath %.c src2
```

**问题三**

通过 VPATH 指定搜索路径后，make 如何决定目标文件的最终位置？

> *[18-path_search/18.3](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/18-path_search/18.3)*

```
.
├── inc
│   └── func.h
├── main.c
├── makefile
└── src
    ├── app.out
    └── func.c
```

> *func.c*

```C
#include <stdio.h>
#include "func.h"

void foo()
{
    printf("void foo(): %s\n", "This file from src ...");
}
```

```makefile
VPATH := src
CFLAGS := -I inc

app.out : func.o main.o
	@gcc -o $@ $^
	@echo "target File ==> $@"

%.o : %.c inc/func.h
	@gcc $(CFLAGS) -o $@ -c $<
```

- 执行命令：`make`

```shell
☁  18.3 [master] ⚡  make
target File ==> app.out
```

- 执行命令：`./app.out`

```shell
☁  18.3 [master] ⚡  ./app.out
void foo(): This file from src ...
```

- 将 `app.out` 移动到 `src` 目录下，执行命令 `make`

```shell
☁  18.3 [master] ⚡  make
make: 'src/app.out' is up to date.
```

- 修改 `func.c`

```C
#include <stdio.h>
#include "func.h"

void foo()
{
    printf("void foo(): %s\n", "This file from changed ...");
}
```

- 执行命令：`make`

```shell
☁  18.3 [master] ⚡  make
target File ==> app.out
```

- 执行命令：`./app.out`

```shell
☁  18.3 [master] ⚡  ./app.out
void foo(): This file is changed ...
```

- 执行命令：`./src/app.out`

```shell
☁  18.3 [master] ⚡  ./src/app.out
void foo(): This file from src ...
```

**实验结论**

- 当 app.out 完全不存在：
  - make 在当前文件夹下创建 app.out
- 当 src 文件夹中存在 app.out
  - 所有目标和依赖的新旧关系不变，make 不会重新创建 app.out
  - 当依赖文件更新，make 在当前文件夹下创建 app.out

**问题**

当依赖改变时，如何使得 src 下的 app.out 被更新？

**解决方案**

- 使用 GPATH 特殊变量指定目标文件夹
- `CPATH := src`
  - 当 app.out 完全不存在
    - make 默认在当前文件夹创建 app.out
  - 当 app.out 存在于 src， 且依赖文件被更新
    - make 在 src 中创建 app.out

> *[18-path_search/18.4/makefile](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/18-path_search/18.4/makefile)*

```makefile
GPATH := src
VPATH := src
CFLAGS := -I inc

app.out : func.o main.o
	@gcc -o $@ $^
	@echo "target File ==> $@"

%.o : %.c inc/func.h
	@gcc $(CFLAGS) -o $@ -c $<
```

**工程项目中的几点建议**

- 尽量使用 `vpath` 为不同文件指定搜索路径
- 不要在源代码文件夹中生成目标文件
- 为编译得到的结果创建独立的文件夹
- 避免 `VPATH` 和 `GPATH` 特殊变量的使用

## 19. 路径搜索综合示例

**需求分析**

- 工程项目中不希望源码文件夹在编译时被改动（只读文件夹）
- 在编译时自动创建文件夹（build）用于存放编译结果
- 编译过程中能够自动搜索需要的文件
- makefile易于扩展，能够复用于相同类型的项目
- 支持调试版本的编译选项

**项目类型分析**

```shell
.
└── Project
    ├── inc      // 源码文件夹（只读）
    ├── src      // 源码文件夹（只读）
    ├── build    // 目标文件夹（编译产生）
    └── makefile // 可复用的 makefile
```

**工具原料**

- `$(wildcard $(DIR)/_pattern)`
  - 获取 `$(DIR)` 文件夹中满足 `_pattern` 的文件
- `$(notdir _names)`
  - 去除 `_names` 中每一个文件名的路径前缀
- `$(patsubst _pattern, replacement, _text)`
  - 将 `_text` 中符合 `_pattern` 的部分替换为 `replacement`

**关键技巧**

- 自动获取源文件列表（函数调用）
  - `SRC := $(wildcard src/*.c)`
- 根据源文件列表生成目标文件列表（变量的值替换）
  - `OBJS := $(SRCS:.c=.o)`
- 替换每一个目标文件的路径前缀（函数调用）
  - `OBJS := $(patsubst src/%, build/%, $(OBJS))`

**编译规则的依赖**

```
+-----+       +------------+       +-------+
| all | ----> | build(dir) | ----> | mkdir |
+-----+       +------------+       +-------+
   \            ↗     ↖  
    \         /         \
     \       /           \
      ↘     /             \
     +---------+       +-----+       +---------+
     | app.out | ----> | %.o | ----> | gcc %.c |
     +---------+       +-----+       +---------+
```

> *[19-path_search_demo](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/19-path_search_demo)*

- 目录结构

```shell
.
├── inc
│   └── func.h
├── makefile
└── src
    ├── func.c
    └── main.c
```

> *19-path_search_demo/makefile*

```makefile
.PHONY : all clean

DIR_BUILD := build
DIR_SRC := src
DIR_INC := inc

TYPE_INC := .h
TYPE_SRC := .c
TYPE_OBJ := .o

CC := gcc
LFLAGS := 
CFLAGS := -I $(DIR_INC)

ifeq ($(DEBUG), true)
CFLAGS += -g
endif

MKDIR := mkdir
RM := rm -rf

APP := $(DIR_BUILD)/app.out
HDRS := $(wildcard $(DIR_INC)/*$(TYPE_INC))
HDRS := $(notdir $(HDRS))
OBJS := $(wildcard $(DIR_SRC)/*$(TYPE_SRC))
OBJS := $(OBJS:$(TYPE_SRC)=$(TYPE_OBJ))
OBJS := $(patsubst $(DIR_SRC)/%, $(DIR_BUILD)/%, $(OBJS))

vpath %$(TYPE_INC) $(DIR_INC)
vpath %$(TYPE_SRC) $(DIR_SRC)

all : $(DIR_BUILD) $(APP)
	@echo "Target File ==> $(APP)"

$(DIR_BUILD) : 
	$(MKDIR) $@

$(APP) : $(OBJS)
	$(CC) $(LFLAGS) -o $@ $^

$(DIR_BUILD)/%$(TYPE_OBJ) : %$(TYPE_SRC) $(HDRS)
	$(CC) $(CFLAGS) -o $@ -c $<

clean :
	$(RM) $(DIR_BUILD)
```

- 执行命令编译：`make all`

```shell
☁  19-path_search_demo [master] ⚡  make all
mkdir build
gcc -I inc -o build/main.o -c src/main.c
gcc -I inc -o build/func.o -c src/func.c
gcc  -o build/app.out build/main.o build/func.o
Target File ==> build/app.out
```

- 执行命令运行程序：`./build/app.out`

```shell
☁  19-path_search_demo [master] ⚡  ./build/app.out
void foo(): Hello World!
```

- 清理：`make clean`

```shell
☁  19-path_search_demo [master] ⚡  make clean
rm -rf build
```

- 编译debug版本：`make DEBUG=true`

```shell
☁  19-path_search_demo [master] ⚡  make DEBUG=true
mkdir build
gcc -I inc -g -o build/main.o -c src/main.c
gcc -I inc -g -o build/func.o -c src/func.c
gcc  -o build/app.out build/main.o build/func.o
Target File ==> build/app.out
```

> **思考：** 如何编写项目 makefile 使其能够触发模块 makefile 的调用，并最终生成可执行程序？


## 20. 打造专业的编译环境（上）

**大型项目的目录结构（无第三方库）**

```
Project ---------------------> build
   |                            |
   |--> common                  |--> common
   |            |--> inc        |             |-> *.o
   |--> module -|--> src        |--> module --|
   |            |--> makefile   |             |-> *.dep
   |--> main                    |--> main
   |                            |
   |--> makefile                |--> app.out
                                |
                                |--> *.a
```

**项目架构设计分析**

- 项目被划分为多个不同模块
  - 每个模块的代码用一个文件夹进行管理
    - 文件夹由 `inc`, `src`, `makefile` 构成
  - 每一个模块的对外函数声明统一放置于 `common/inc` 中
    - 如：`common.h`, `xxfunc.h`

**需要打造的编译环境**

- 源码问价夹在编译时不能被改动（只读文件夹）
- 在编译时自动创建文件夹（build）用于存放编译结果
- 编译过程中能够自动生成依赖关系，自动搜索需要的文件
- 每个模块可以拥有自己独立的编译方式
- 支持调试版本的编译选项

**解决方案设计**

- 第1阶段：将每个模块中的代码编译成静态库文件

```
                      +------------+
                      | <<common>> |       +----------+
                  ↗   |  makefile  | ----> | common.a |
                 /    +------------+       +----------+
                /
               /
+-------------+       +------------+       +----------+
| <<Project>> | ----> | <<module>> | ----> | module.a |
|  makefile   |       |  makefile  |       +----------+
+-------------+       +------------+
               \
                \
                 \    +------------+       +----------+
                  ↘   |  <<main>>  | ----> |  main.a  |
                      |  makefile  |       +----------+
                      +------------+
```

- 第2阶段：将每个模块的静态库文件链接成最终可执行程序

```
                      +----------+
                  ↗   | common.a |
                 /    +----------+
                /
               /
+-------------+       +----------+       +---------+
| <<Project>> | ----> | module.a | ====> | app.out |
|  makefile   |       +----------+       +---------+
+-------------+
               \
                \
                 \    +----------+
                  ↘   |  main.a  |
                      +----------+
```

**第一阶段任务：**

- 完成可用于各个模块编译的 makefile 文件
- 每个模块的编译结果为静态库文件（`.a` 文件）

**关键的实现要点**

- 自动生成依赖关系（`gcc -MM`）
- 自动搜索需要的文件（`vpath`）
- 将目标文件打包为静态文件（`ar crs`）

**模块 makefile 中的构成**

```
                        +-------------+
                      ↗ | gcc -MM %.c |
         +---------+ /  +-------------+
         | include |/
       / | %.deps  |\ 
    1 /  +---------+ \  +-----+
     /                ↘ | sed |
+------------+          +-----+
| <<module>> |
|  makefile  |
+------------+
    \
   2 \  +-----+    +----------+    +-----+    +---------+
      \ | all | -> | module.a | -> | %.o | -> | gcc %.c |
        +-----+    +----------+    +-----+    +---------+
```

**编程实验**

- 模块编译

> *[20-env/common](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/20-env/common)*

- 目录结构

```shell
.
├── build
│   └── common
├── common
    ├── inc
    │   ├── common.h
    │   └── func.h
    ├── makefile
    └── src
        ├── common.c
        └── func.c
```

> *[20-env/common/makefile](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/20-env/common/makefile)*

```makefile
.PHONY : all

DIR_BUILD := /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build
DIR_COMMON_INC := /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/common/inc

DIR_SRC := src
DIR_INC := inc

TYPE_INC := .h
TYPE_SRC := .c
TYPE_OBJ := .o
TYPE_DEP := .dep

AR := ar
ARFLAGS := crs

CC := gcc
CFLAGS := -I$(DIR_INC) -I$(DIR_COMMON_INC)

ifeq ($(DEBUG),true)
CFLAGS += -g
endif

MODULE := $(realpath .)
MODULE := $(notdir $(MODULE))

DIR_OUTPUT := $(addprefix $(DIR_BUILD)/, $(MODULE))

OUTPUT := $(MODULE).a
OUTPUT := $(addprefix $(DIR_BUILD)/, $(OUTPUT))

SRCS := $(wildcard $(DIR_SRC)/*$(TYPE_SRC))
OBJS := $(SRCS:$(TYPE_SRC)=$(TYPE_OBJ))
OBJS := $(patsubst $(DIR_SRC)/%, $(DIR_OUTPUT)/%, $(OBJS))
DEPS := $(SRCS:$(TYPE_SRC)=$(TYPE_DEP))
DEPS := $(patsubst $(DIR_SRC)/%, $(DIR_OUTPUT)/%, $(DEPS))

vpath %$(TYPE_INC) $(DIR_INC)
vpath %$(TYPE_INC) $(DIR_COMMON_INC)
vpath %$(TYPE_SRC) $(DIR_SRC)

-include $(DEPS)

all : $(OUTPUT)
	@echo "Success! Target ==> $(OUTPUT)"

$(OUTPUT) : $(OBJS)
	$(AR) $(ARFLAGS) $@ $^

$(DIR_OUTPUT)/%$(TYPE_OBJ) : %$(TYPE_SRC)
	$(CC) $(CFLAGS) -o $@ -c $(filter %$(TYPE_SRC), $^)

$(DIR_OUTPUT)/%$(TYPE_DEP) : %$(TYPE_SRC)
	@echo "Creating $@ ..."
	@set -e; \
	$(CC) $(CFLAGS) -MM -E $(filter %$(TYPE_SRC), $^) | sed 's,\(.*\)\.o[ :]*,$(DIR_OUTPUT)/\1$(TYPE_OBJ) $@ : ,g' > $@

```

- 编译 `module` 模块：`make all`

```shell
☁  common [master] ⚡  make all
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/common.a
```

- 查看生成文件及目录结构

```makefile
.
├── build
│   ├── common
│   │   ├── common.dep
│   │   ├── common.o
│   │   ├── func.dep
│   │   └── func.o
│   └── common.a
├── common
    ├── inc
    │   ├── common.h
    │   └── func.h
    ├── makefile
    └── src
        ├── common.c
        └── func.c
```

如上输出信息，`common` 模块编译完成后，在对应的 `build/common` 目录下生成依赖文件 `*.dep` 和目标文件 `*.o`, 在 `build` 目录下生成静态库文件 `*.a`；将 `common` 模块中的 `makefile` 文件分别拷贝到 `module` 和 `main` 模块目录下，编译后可在 `build` 目录下生成 `module` 和 `main` 模块对应的依赖文件、目标文件和库文件，编译完成后完整目录结构如下：

- 编译 `module` 模块：`make all`

```shell
☁  module [master] ⚡  make all
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/module.a
```

- 编译 `main` 模块：`make all`

```shell
☁  main [master] ⚡  make all
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/20-env/build/main.a
```

- 编译后目录结构：

```shell
.
├── build
│   ├── common
│   │   ├── common.dep
│   │   ├── common.o
│   │   ├── func.dep
│   │   └── func.o
│   ├── common.a
│   ├── main
│   │   ├── main.dep
│   │   └── main.o
│   ├── main.a
│   ├── module
│   │   ├── module.dep
│   │   └── module.o
│   └── module.a
├── common
│   ├── inc
│   │   ├── common.h
│   │   └── func.h
│   ├── makefile
│   └── src
│       ├── common.c
│       └── func.c
├── main
│   ├── inc
│   │   └── define.h
│   ├── makefile
│   └── src
│       └── main.c
└── module
    ├── inc
    │   └── module.h
    ├── makefile
    └── src
        └── module.c
```

## 21. 打造专业的编译环境（中）

**第二阶段任务：**

- 完成编译整个工程的 makefile 文件
- 调用模块 makefile 编译生成静态库文件
- 编译所有模块的静态库文件，最终得到可执行程序

```
                      +----------+
                  ↗   | common.a |
                 /    +----------+
                /
               /
+-------------+       +----------+       +---------+
| <<Project>> | ----> | module.a | ====> | app.out |
|  makefile   |       +----------+       +---------+
+-------------+
               \
                \
                 \    +----------+
                  ↘   |  main.a  |
                      +----------+
```

**关键的实现要点**

- 如何自动创建 build 文件夹以及子文件夹？
- 如何进入每一个模块文件夹进行编译？
- 编译成功后如何链接所有模块静态库？

**开发中的经验假设**

项目中的各个模块在设计阶段就已经基本确定，因此，在之后的开发过程中不会频繁随意的增加或减少！！！

- 解决方案设计

1. 定义变量保存模块名列表（模块名变量）
2. 利用 Shell 中的 for 循环遍历模块名变量
3. 在 for 循环中进入模块文件夹进行编译
4. 循环结束后链接所有的模块静态库文件

**makefile 中嵌入 Shell 的 for 循环**

```makefile
# 模块名列表
MODULES := common \
           module \
           main

test :
# 1. 发生错误后立即退出执行
# 2. Shell 中的 for 循环
# 3. 循环体中打印模块名
	@set -e; \
	for dir in $(MODULES); \
	do \
		echo $$dir; \
	done
```

> **注意事项：** makefile 中嵌入 Shell 代码时，如果需要使用 Shell 变量的值，必须在变量名前加上 `$$`（例如：`$$dir`）

**工程 makefile 中的关键构成**

```
                       +--------------+
                       | compile      |
                     ↗ |--------------|
                    /  | mkdir module |
                   /   | cd module    |
                  /    | make all     |
                 /     | cd ..        |
                /      +--------------+
               /
+-------------+
| <<Project>> |
|  makefile   |
+-------------+
               \
                \
                 \
                  \    +--------------------+
                   \   |   link             |
                    ↘  +--------------------+
                       | gcc -o app.out *.a |
                       +--------------------+
```

**链接时的注意事项**

- gcc 在进行静态库链接时必须遵循严格的依赖关系
  - `gcc -o app.out x.a y.a z.a`
    - 其中的依赖关系必须为：`x.a` --> `y.a`, `y.a` --> `z.a`
    - 默认情况下遵循自左向右的依赖关系

- 如果不清楚库间的依赖，可以使用 `-Xlinker` 自动确定依赖关系
  - `gcc -o app.out -Xlinker "-("z.a y.a x.a -Xlinker "-)"`

- **工程的编译**

> **[make_lesson/21-env](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/21-env)**

- 目录结构：

```shell
.
├── common
│   ├── inc
│   │   ├── common.h
│   │   └── func.h
│   ├── makefile
│   └── src
│       ├── common.c
│       └── func.c
├── main
│   ├── inc
│   │   └── define.h
│   ├── makefile
│   └── src
│       └── main.c
├── makefile
└── module
    ├── inc
    │   └── module.h
    ├── makefile
    └── src
        └── module.c
```

> **[21-env/makefile](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/21-env/makefile)**

```makefile

.PHONY : all compile link clean rebuild

MODULES := common \
           module \
           main

MKDIR := mkdir
RM := rm -rf

CC := gcc
LFLAGS :=

DIR_PROJECT := $(realpath .)
DIR_BUILD := build
DIR_BUILD_SUB := $(addprefix $(DIR_BUILD)/, $(MODULES))
MODULE_LIB := $(addsuffix .a, $(MODULES))
MODULE_LIB := $(addprefix $(DIR_BUILD)/, $(MODULE_LIB))

APP := app.out
APP := $(addprefix $(DIR_BUILD)/, $(APP))

all : compile $(APP)
	@echo "Success!!! Target ==> $(APP)"

compile : $(DIR_BUILD) $(DIR_BUILD_SUB)
	@echo "Begin to compile ..."
	@set -e; \
	for dir in $(MODULES); \
	do \
		cd $$dir && $(MAKE) all DEBUG:=$(DEBUG) && cd ..; \
	done
	@echo "Compile Success!!!"

link $(APP) : $(MODULE_LIB)
	@echo "Begin to link ..."
	$(CC) -o $(APP) -Xlinker "-(" $^ -Xlinker "-)" $(LFLAGS)
	@echo "Link Success!!!"

$(DIR_BUILD) $(DIR_BUILD_SUB) : 
	$(MKDIR) $@

clean :
	@echo "Begin to clean ..."
	$(RM) $(DIR_BUILD)
	@echo "Clean Success!!!"

rebuild : clean all
```

- 编译整个项目：`make all`

```shell
☁  21-env [master] ⚡  make all
☁  21-env [master] ⚡  make all
Begin to compile ...
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/module'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/module'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/main'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/main'
Compile Success!!!
Begin to link ...
gcc -o build/app.out -Xlinker "-(" build/common.a build/module.a build/main.a -Xlinker "-)" 
Link Success!!!
Success!!! Target ==> build/app.out
```

- 运行：`./build/app.out`

```shell
☁  21-env [master] ⚡  ./build/app.out
Version: 1.0.0
main()::start main ...
void common() ...
module_main()::start module ...
void foo()::Hello, Fyang ...
```
- 清理：`make clean`

```shell
☁  21-env [master] ⚡  make clean
Begin to clean ...
rm -rf build
Clean Success!!!
```

- 分步编译项目：
  1. 编译：`make compile`

```shell
☁  21-env [master] ⚡  make compile
mkdir build
mkdir build/common
mkdir build/module
mkdir build/main
Begin to compile ...
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/module'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/module'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/main'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/main'
Compile Success!!!
```

  2. 链接：`make link`

```shell
☁  21-env [master] ⚡  make link
Begin to link ...
gcc -o build/app.out -Xlinker "-(" build/common.a build/module.a build/main.a -Xlinker "-)" 
Link Success!!!
```

  3. 运行：`./build/app.out`

```shell
☁  21-env [master] ⚡  ./build/app.out 
Version: 1.0.0
main()::start main ...
void common() ...
module_main()::start module ...
void foo()::Hello, Fyang ...
```

- 重新编译：`make rebuild`

```makefile
☁  21-env [master] ⚡  make rebuild
Begin to clean ...
rm -rf build
Clean Success!!!
mkdir build
mkdir build/common
mkdir build/module
mkdir build/main
Begin to compile ...
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/common.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/module'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/module.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/module'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/main'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build/main.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/main'
Compile Success!!!
Begin to link ...
gcc -o build/app.out -Xlinker "-(" build/common.a build/module.a build/main.a -Xlinker "-)" 
Link Success!!!
Success!!! Target ==> build/app.out
```

> **思考：** 当前整个项目的 makefile 是否存在潜在的问题？是否需要重构？

## 22. 打造专业的编译环境（下）

**问题一：**

- 所有模块 makefile 中使用的编译路径均为写死的绝对路径，一旦项目文件夹移动，编译必将失败！

```shell
DIR_BUILD := /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/build
DIR_COMMON_INC := /home/fyang/wkspace/project_build_tuorial/make_lesson/21-env/common/inc
```

**解决方案**

- 在工程 makefile 中获取项目的源码路径
- 根据项目源码路径：
  - 拼接得到编译文件夹的路径（`DIR_BUILD`）
  - 拼接得到全局包含路径（`DIR_COMMON_INC`）

- 通过定义命令行变量将路径传递给模块 makefile


- **修改工程 makefile：** `22-env/makefile` ；删除 `22-env/common/makefile`，`22-env/module/makefile`，`22-env/main/makefile` 中的绝对路径

> *22-env/makefile*

```makefile

.PHONY : all compile link clean rebuild

MODULES := common \
           module \
           main

MKDIR := mkdir
RM := rm -rf

CC := gcc
LFLAGS :=

DIR_PROJECT := $(realpath .)
DIR_BUILD := build
DIR_COMMON_INC : common/inc
DIR_BUILD_SUB := $(addprefix $(DIR_BUILD)/, $(MODULES))
MODULE_LIB := $(addsuffix .a, $(MODULES))
MODULE_LIB := $(addprefix $(DIR_BUILD)/, $(MODULE_LIB))

APP := app.out
APP := $(addprefix $(DIR_BUILD)/, $(APP))

all : compile $(APP)
	@echo "Success!!! Target ==> $(APP)"

compile : $(DIR_BUILD) $(DIR_BUILD_SUB)
	@echo "Begin to compile ..."
	@set -e; \
	for dir in $(MODULES); \
	do \
		cd $$dir && \
		$(MAKE) all \
			DEBUG:=$(DEBUG) && \
			DIR_BUILD:=$(addprefix $(DIR_PROJECT)/, $(DIR_BUILD)) \
			DIR_COMMON_INC:=$(addprefix $(DIR_PROJECT)/, $(DIR_COMMON_INC)) && \
		cd ..; \
	done
	@echo "Compile Success!!!"

link $(APP) : $(MODULE_LIB)
	@echo "Begin to link ..."
	$(CC) -o $(APP) -Xlinker "-(" $^ -Xlinker "-)" $(LFLAGS)
	@echo "Link Success!!!"

$(DIR_BUILD) $(DIR_BUILD_SUB) : 
	$(MKDIR) $@

clean :
	@echo "Begin to clean ..."
	$(RM) $(DIR_BUILD)
	@echo "Clean Success!!!"

rebuild : clean all
```

> *22-env/*/makefile*, 此处 makefile，`common` , `main`, `module` 模块中相同

```makefile
.PHONY : all

DIR_SRC := src
DIR_INC := inc

TYPE_INC := .h
TYPE_SRC := .c
TYPE_OBJ := .o
TYPE_DEP := .dep

AR := ar
ARFLAGS := crs

CC := gcc
CFLAGS := -I$(DIR_INC) -I$(DIR_COMMON_INC)

ifeq ($(DEBUG),true)
CFLAGS += -g
endif

MODULE := $(realpath .)
MODULE := $(notdir $(MODULE))

DIR_OUTPUT := $(addprefix $(DIR_BUILD)/, $(MODULE))

OUTPUT := $(MODULE).a
OUTPUT := $(addprefix $(DIR_BUILD)/, $(OUTPUT))

SRCS := $(wildcard $(DIR_SRC)/*$(TYPE_SRC))
OBJS := $(SRCS:$(TYPE_SRC)=$(TYPE_OBJ))
OBJS := $(patsubst $(DIR_SRC)/%, $(DIR_OUTPUT)/%, $(OBJS))
DEPS := $(SRCS:$(TYPE_SRC)=$(TYPE_DEP))
DEPS := $(patsubst $(DIR_SRC)/%, $(DIR_OUTPUT)/%, $(DEPS))

vpath %$(TYPE_INC) $(DIR_INC)
vpath %$(TYPE_INC) $(DIR_COMMON_INC)
vpath %$(TYPE_SRC) $(DIR_SRC)

-include $(DEPS)

all : $(OUTPUT)
	@echo "Success! Target ==> $(OUTPUT)"

$(OUTPUT) : $(OBJS)
	$(AR) $(ARFLAGS) $@ $^

$(DIR_OUTPUT)/%$(TYPE_OBJ) : %$(TYPE_SRC)
	$(CC) $(CFLAGS) -o $@ -c $(filter %$(TYPE_SRC), $^)

$(DIR_OUTPUT)/%$(TYPE_DEP) : %$(TYPE_SRC)
	@echo "Creating $@ ..."
	@set -e; \
	$(CC) $(CFLAGS) -MM -E $(filter %$(TYPE_SRC), $^) | sed 's,\(.*\)\.o[ :]*,$(DIR_OUTPUT)/\1$(TYPE_OBJ) $@ : ,g' > $@
```

**问题二**

- 所有模块 makefile 的内容完全相同（复制粘贴）
- 当模块 makefile 需要改动时，将涉及多处相同的修改

**解决方案**

- 将模块 makefile 拆分为两个模板文件
  - `mod-cfg.mk` ：定义可能改变的变量
  - `mod-rule.mk` ：定义相对稳定的变量和规则

- 默认情况下
  - 模块 makefile 复用模板文件实现功能（`include`）

**关键问题**

- 模块 makefile 如何知道模板文件的具体位置？
- 解决方案：
  - 通过命令行变量进行模板文件位置的传递

- **模块 makefile 的拆分**

> *22-env/makefile*

```makefile

.PHONY : all compile link clean rebuild

MODULES := common \
           module \
           main

MKDIR := mkdir
RM := rm -rf

CC := gcc
LFLAGS :=

DIR_PROJECT := $(realpath .)
DIR_BUILD := build
DIR_COMMON_INC := common/inc
DIR_BUILD_SUB := $(addprefix $(DIR_BUILD)/, $(MODULES))
MODULE_LIB := $(addsuffix .a, $(MODULES))
MODULE_LIB := $(addprefix $(DIR_BUILD)/, $(MODULE_LIB))

MOD_CFG := mod-cfg.mk
MOD_RULE := mod-rule.mk
CMD_CFG := cmd-cfg.mk

APP := app.out
APP := $(addprefix $(DIR_BUILD)/, $(APP))

all : compile $(APP)
	@echo "Success!!! Target ==> $(APP)"

compile : $(DIR_BUILD) $(DIR_BUILD_SUB)
	@echo "Begin to compile ..."
	@set -e; \
	for dir in $(MODULES); \
	do \
		cd $$dir && \
		$(MAKE) all \
				DEBUG:=$(DEBUG) \
				DIR_BUILD:=$(addprefix $(DIR_PROJECT)/, $(DIR_BUILD)) \
				DIR_COMMON_INC:=$(addprefix $(DIR_PROJECT)/, $(DIR_COMMON_INC)) \
				CMD_CFG:=$(addprefix $(DIR_PROJECT)/, $(CMD_CFG)) \
				MOD_CFG:=$(addprefix $(DIR_PROJECT)/, $(MOD_CFG)) \
				MOD_RULE:=$(addprefix $(DIR_PROJECT)/, $(MOD_RULE)) && \
		cd .. ; \
	done
	@echo "Compile Success!!!"

link $(APP) : $(MODULE_LIB)
	@echo "Begin to link ..."
	$(CC) -o $(APP) -Xlinker "-(" $^ -Xlinker "-)" $(LFLAGS)
	@echo "Link Success!!!"

$(DIR_BUILD) $(DIR_BUILD_SUB) : 
	$(MKDIR) $@

clean :
	@echo "Begin to clean ..."
	$(RM) $(DIR_BUILD)
	@echo "Clean Success!!!"

rebuild : clean all
```

> *[22-env/mod-cfg.mk](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/22-env/mod-cfg.mk)*

```makefile

DIR_SRC := src
DIR_INC := inc

TYPE_INC := .h
TYPE_SRC := .c
TYPE_OBJ := .o
TYPE_DEP := .dep

```

> *[22-env/mod-rule.mk](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/22-env/mod-rule.mk)*

```makefile


.PHONY : all

MODULE := $(realpath .)
MODULE := $(notdir $(MODULE))

DIR_OUTPUT := $(addprefix $(DIR_BUILD)/, $(MODULE))

OUTPUT := $(MODULE).a
OUTPUT := $(addprefix $(DIR_BUILD)/, $(OUTPUT))

SRCS := $(wildcard $(DIR_SRC)/*$(TYPE_SRC))
OBJS := $(SRCS:$(TYPE_SRC)=$(TYPE_OBJ))
OBJS := $(patsubst $(DIR_SRC)/%, $(DIR_OUTPUT)/%, $(OBJS))
DEPS := $(SRCS:$(TYPE_SRC)=$(TYPE_DEP))
DEPS := $(patsubst $(DIR_SRC)/%, $(DIR_OUTPUT)/%, $(DEPS))

vpath %$(TYPE_INC) $(DIR_INC)
vpath %$(TYPE_INC) $(DIR_COMMON_INC)
vpath %$(TYPE_SRC) $(DIR_SRC)

-include $(DEPS)

all : $(OUTPUT)
	@echo "Success! Target ==> $(OUTPUT)"

$(OUTPUT) : $(OBJS)
	$(AR) $(ARFLAGS) $@ $^

$(DIR_OUTPUT)/%$(TYPE_OBJ) : %$(TYPE_SRC)
	$(CC) $(CFLAGS) -o $@ -c $(filter %$(TYPE_SRC), $^)

$(DIR_OUTPUT)/%$(TYPE_DEP) : %$(TYPE_SRC)
	@echo "Creating $@ ..."
	@set -e; \
	$(CC) $(CFLAGS) -MM -E $(filter %$(TYPE_SRC), $^) | sed 's,\(.*\)\.o[ :]*,$(DIR_OUTPUT)/\1$(TYPE_OBJ) $@ : ,g' > $@


```

> *[22-env/cmd-cfg.mk](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/22-env/cmd-cfg.mk)*

```makefile


AR := ar
ARFLAGS := crs

CC := gcc
LFLAGS :=
CFLAGS := -I$(DIR_INC) -I$(DIR_COMMON_INC)

ifeq ($(DEBUG),true)
CFLAGS += -g
endif

MKDIR := mkdir
RM := rm -rf


```

> *[22-env/common/makefile](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/22-env/common/makefile)*

```makefile

include $(MOD_CFG)

# Custmization Begin
#
# DIR_SRC := src
# DIR_INC := inc
#
# TYPE_INC := .h
# TYPE_SRC := .c
# TYPE_OBJ := .o
# TYPE_DEP := .dep
#
# Custmization End

include $(CMD_CFG)

include $(MOD_RULE)

```

> `common`, `main`, `module` 模块中的 makefile 文件内容相同

**工程 makefile 的重构**

- 拆分命令变量，项目变量，以及其它变量和规则到不同文件
  - `cmd-cfg.mk`：定义命令相关的变量
  - `pro-cfg.mk`：定义项目变量以及编译路径变量等
  - `pro-rule.mk`：定义其它变量和规则
  - 最后的工程 `makefile` 通过包含拆分后的文件构成（`include`）

> *[22-env](https://gitee.com/fyang0906/project_build_tuorial/tree/master/make_lesson/22-env)*

- 目录结构

```shell
.
├── common
│   ├── inc
│   │   ├── common.h
│   │   └── func.h
│   ├── makefile
│   └── src
│       ├── common.c
│       └── func.c
├── main
│   ├── inc
│   │   └── define.h
│   ├── makefile
│   └── src
│       └── main.c
├── module
│   ├── inc
│   │   └── module.h
│   ├── makefile
│   └── src
│       └── module.c
├── makefile
├── mod-cfg.mk
├── mod-rule.mk
├── cmd-cfg.mk
├── pro-cfg.mk
└── pro-rule.mk
```

**小结**

- 大型项目的编译环境是由不同的 makefile 构成的
- 编译环境的设计需要依据项目的整体架构设计
- 整个项目的编译过程可分解为不同阶段
- 根据不同的阶段有针对性的对 makefile 进行设计
- makefile 也需要考虑复用性和维护性等基本程序特性

## 23. 模块独立编译的支持

**问题**

一般而言，不同工程师负责不同模块的开发；编译环境中如何支持模块的独立编译？

**问题背景**

- 大型项目的代码文件成千上万，完整编译时间较长
- 编写模块代码时，可通过编译检查语法错误
- 为了提高开发效率，需要支持指定模块的独立编译

示例：

```shell
>> make main
Begin to compile main
...
...
Success! Target ==> /home/make/build/main.a
```

**解决方案**

- 将模块名（module）作为目标名（伪目标）建立规则
- 目标（module）对应的依赖为 build build/module
- 规则中的命令进入对应的模块文件夹进行编译
- 编译结果存放于 build 文件夹下

**关键技术点**

- 如何获取 make 命令中指定编译的模块名？
  - 预定义变量：`$(MAKECMDGOALS)`
    - 命令行中指定的目标名（make 的命令行参数）

```makefile
$(MODULES) : $(DIR_BUILD) $(DIR_BUILD)/$(MAKECMDGOALS)
	cd $@ && \
	$(MAKE) all \
	...
```

> `$(DIR_BUILD)/$(MAKECMDGOALS)` 是否可写成 `$(DIR_BUILD)/$@` ?

- 模块的独立编译


> *23-env/pro-rule.mk*

```makefile

.PHONY : all compile link clean rebuild $(MODULES)

DIR_PROJECT := $(realpath .)
DIR_BUILD_SUB := $(addprefix $(DIR_BUILD)/, $(MODULES))
MODULE_LIB := $(addsuffix .a, $(MODULES))
MODULE_LIB := $(addprefix $(DIR_BUILD)/, $(MODULE_LIB))

APP := app.out
APP := $(addprefix $(DIR_BUILD)/, $(APP))

all : compile $(APP)
	@echo "Success!!! Target ==> $(APP)"

compile : $(DIR_BUILD) $(DIR_BUILD_SUB)
	@echo "Begin to compile ..."
	@set -e; \
	for dir in $(MODULES); \
	do \
		cd $$dir && \
		$(MAKE) all \
				DEBUG:=$(DEBUG) \
				DIR_BUILD:=$(addprefix $(DIR_PROJECT)/, $(DIR_BUILD)) \
				DIR_COMMON_INC:=$(addprefix $(DIR_PROJECT)/, $(DIR_COMMON_INC)) \
				CMD_CFG:=$(addprefix $(DIR_PROJECT)/, $(CMD_CFG)) \
				MOD_CFG:=$(addprefix $(DIR_PROJECT)/, $(MOD_CFG)) \
				MOD_RULE:=$(addprefix $(DIR_PROJECT)/, $(MOD_RULE)) && \
		cd .. ; \
	done
	@echo "Compile Success!!!"

link $(APP) : $(MODULE_LIB)
	@echo "Begin to link ..."
	$(CC) -o $(APP) -Xlinker "-(" $^ -Xlinker "-)" $(LFLAGS)
	@echo "Link Success!!!"

$(DIR_BUILD) $(DIR_BUILD_SUB) : 
	$(MKDIR) $@

clean :
	@echo "Begin to clean ..."
	$(RM) $(DIR_BUILD)
	@echo "Clean Success!!!"

rebuild : clean all

$(MODULES) : $(DIR_BUILD) $(DIR_BUILD)/$(MAKECMDGOALS)
	@echo "Begin to compile $@"
	@set -e; \
	cd $@ && \
		$(MAKE) all \
				DEBUG:=$(DEBUG) \
				DIR_BUILD:=$(addprefix $(DIR_PROJECT)/, $(DIR_BUILD)) \
				DIR_COMMON_INC:=$(addprefix $(DIR_PROJECT)/, $(DIR_COMMON_INC)) \
				CMD_CFG:=$(addprefix $(DIR_PROJECT)/, $(CMD_CFG)) \
				MOD_CFG:=$(addprefix $(DIR_PROJECT)/, $(MOD_CFG)) \
				MOD_RULE:=$(addprefix $(DIR_PROJECT)/, $(MOD_RULE)) && \
	cd .. ; \
```

- 编译 `common` 模块：`make common`

```shell
☁  23-env [master] ⚡  make common 
mkdir build
mkdir build/common
Begin to compile common
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/common'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/common.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/common'
```

- 编译 `module` 模块：`make module`

```shell
☁  23-env [master] ⚡  make module
mkdir build/module
Begin to compile module
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/module'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/build/module.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/23-env/module'
```

- 编译 `main` 模块：`make main`

```shell
☁  23-modules_compile [master] ⚡  make main
mkdir build
mkdir build/main
Begin to compile main
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/main'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/common/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/build/main.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/23-modules_compile/main'
```

- 链接生成可执行文件：`make link`

```shell
☁  23-env [master] ⚡  make link
Begin to link ...
gcc -o build/app.out -Xlinker "-(" build/common.a build/module.a build/main.a -Xlinker "-)" 
Link Success!!!
```

**makefile 中的代码复用**

- 当不同规则中的命令大量重复时，可考虑自定义函数
- makefile 中的自定义函数是代码复用的一种方式

```makefile
define func
	@echo "My name is $(0)"
	@echo "Param => $(1)"
endef
```

```makefile
rule1 :
	$(call func, p1)

rule2 :
	$(call func, p2)
```

**思路**

- 将编译模块的命令集作为自定义函数的具体实现
- 函数参数为模块名，函数调用后编译参数指定的模块
- 在不同的规则中调用该函数

```makefile
define makemodule
	cd $(1) && \
	$(MAKE) all \
	... \
	cd ..;
endef
```

- makefile 中的代码复用自定义函数

> *[23-modules_compile/pro-rule.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/23-modules_compile/pro-rule.mk)*

```makefile

.PHONY : all compile link clean rebuild $(MODULES)

DIR_PROJECT := $(realpath .)
DIR_BUILD_SUB := $(addprefix $(DIR_BUILD)/, $(MODULES))
MODULE_LIB := $(addsuffix .a, $(MODULES))
MODULE_LIB := $(addprefix $(DIR_BUILD)/, $(MODULE_LIB))

APP := app.out
APP := $(addprefix $(DIR_BUILD)/, $(APP))

define makemodule
	cd ${1} && \
	$(MAKE) all \
		DEBUG:=$(DEBUG) \
		DIR_BUILD:=$(addprefix $(DIR_PROJECT)/, $(DIR_BUILD)) \
		DIR_COMMON_INC:=$(addprefix $(DIR_PROJECT)/, $(DIR_COMMON_INC)) \
		CMD_CFG:=$(addprefix $(DIR_PROJECT)/, $(CMD_CFG)) \
		MOD_CFG:=$(addprefix $(DIR_PROJECT)/, $(MOD_CFG)) \
		MOD_RULE:=$(addprefix $(DIR_PROJECT)/, $(MOD_RULE)) && \
	cd .. ;
endef

all : compile $(APP)
	@echo "Success!!! Target ==> $(APP)"

compile : $(DIR_BUILD) $(DIR_BUILD_SUB)
	@echo "Begin to compile ..."
	@set -e; \
	for dir in $(MODULES); \
	do \
		$(call makemodule, $$dir) \
	done
	@echo "Compile Success!!!"

link $(APP) : $(MODULE_LIB)
	@echo "Begin to link ..."
	$(CC) -o $(APP) -Xlinker "-(" $^ -Xlinker "-)" $(LFLAGS)
	@echo "Link Success!!!"

$(DIR_BUILD) $(DIR_BUILD_SUB) : 
	$(MKDIR) $@

clean :
	@echo "Begin to clean ..."
	$(RM) $(DIR_BUILD)
	@echo "Clean Success!!!"

rebuild : clean all

$(MODULES) : $(DIR_BUILD) $(DIR_BUILD)/$(MAKECMDGOALS)
	@echo "Begin to compile $@"
	@set -e; \
	$(call makemodule, $@)
```

**小结**

- 编写模块代码时可通过模块独立编译快速检查语法错误
- 自动变量只能在规则的命令中使用，不能在依赖中使用
- makefile 中的自定义函数是代码复用的一种方式
- 当不同规则中的命令大量重复时，可考虑自定义函数

## 24. 第三方库的使用支持

**问题**

需要使用第三方库文件时，编译环境中的 makefile 如何修改？

**经验假设**

- 第三方库通过函数调用的方式提供库中的功能
- 库文件发布时都附带了声明库函数原型的头文件
- 编译阶段使用头文件，链接阶段使用库文件

```
Project
   |
   |--> common
   |
   |--> module
   |
   |--> main
   |                   |-> dlib.h
   |          |-> inc -|
   |          |        |-> slib.h
   |--> libs -|
              |        |-> dlib.so
              |-> lib -|
                       |-> slib.a
```

**第三方库的编译阶段支持**

- 定义变量 `DIR_LIBS_INC` 用于指示头文件的存储位置
  - `DIR_LIBS_INC := $(DIR_PROJECT)/libs/inc`

- 使用 `DIR_LIBS_INC` 提示 make 头文件的存储位置
  - `vpath %$(TYPE_INC) $(DIR_LIBS_INC)`

- 使用 `DIR_LIBS_INC` 提示编译器头文件的存储位置
  - `CFLAGS += -I$(DIR_LIBS_INC)`

- **第一阶段：编译支持**

- 在 main.c 中调用第三方库：

> *[24-3rd_party_lib/main/src/main.c](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/24-3rd_party_lib/main/src/main.c)*


```C
#include <stdio.h>
#include "define.h"
#include "slib.h"
#include "dlib.h"

int main()
{
    printf("Version: %s\n", VERSION);
    printf("main()::start main ...\n");
    
    common();
    module_main();

    printf("Dynamic Lib: %s\n", dlib_name());
    printf("2 + 3 = %d\n", add(2, 3));

    printf("Static Lib: %s\n", slib_name());
    printf("4 * 5 = %d\n", multi(4, 5));
}
```

> *[24-3rd_party_lib/pro-cfg.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/24-3rd_party_lib/pro-cfg.mk)*


```makefile
# ...
DIR_LIBS_INC := libs/inc
# ...
```

> *[24-3rd_party_lib/pro-rule.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/24-3rd_party_lib/pro-rule.mk)*


```makefile
# ...
define makemodule
	cd ${1} && \
	$(MAKE) all \
		DEBUG:=$(DEBUG) \
		DIR_BUILD:=$(addprefix $(DIR_PROJECT)/, $(DIR_BUILD)) \
		DIR_COMMON_INC:=$(addprefix $(DIR_PROJECT)/, $(DIR_COMMON_INC)) \
		DIR_LIBS_INC:=$(addprefix $(DIR_PROJECT)/, $(DIR_LIBS_INC)) \
		CMD_CFG:=$(addprefix $(DIR_PROJECT)/, $(CMD_CFG)) \
		MOD_CFG:=$(addprefix $(DIR_PROJECT)/, $(MOD_CFG)) \
		MOD_RULE:=$(addprefix $(DIR_PROJECT)/, $(MOD_RULE)) && \
	cd .. ;
endef
# ...

```

> *[24-3rd_party_lib/cmd-cfg.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/24-3rd_party_lib/cmd-cfg.mk)*


```makefile

# ...
CFLAGS := -I$(DIR_INC) -I$(DIR_COMMON_INC) -I$(DIR_LIBS_INC)
# ...

```

> *[24-3rd_party_lib/cmd-rule.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/24-3rd_party_lib/cmd-rule.mk)*


```makefile

# ...
vpath %$(TYPE_INC) $(DIR_INC)
vpath %$(TYPE_INC) $(DIR_COMMON_INC)
vpath %$(TYPE_INC) $(DIR_LIBS_INC)
vpath %$(TYPE_SRC) $(DIR_SRC)
#...

```

**注意事项**

- 定义 `DIR_LIBS_LIB := libs/lib`（第三方库所在路径）
- 链接时不会直接链接 `DIR_LIBS_LIB` 中的库文件
- 需要先将库文件拷贝到 `DIR_BUILD` 文件夹
- 必须考虑拷贝后的库文件和原始库文件的新旧关系

```makefile
$(DIR_BUILD)/% : $(DIR_LIBS)/%
	$(CP) $^ $@
```

**第三方库的链接阶段支持**

- 定义变量 `EXTERNAL_LIB` 用于保存第三方库列表
- 目标 `link` 需要依赖于第三方库列表

```makefile
link $(APP) : $(MODULE_LIB) $(EXTERNAL_LIB)
	@echo "Begin to link ..."
	$(CC) -o $(APP) -Xlinker "-(" $^ -Xlinker "-)" $(LFLAGS)
	@echo "Link Success!!!"
```

> **Tips:** `$(EXTERNAL_LIB)` 必须作为最后一个依赖出现（why?）

> *[24-3rd_party_lib/pro-cfg.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/24-3rd_party_lib/pro-cfg.mk)*

```makefile
# ...
DIR_LIBS_LIB := libs/lib
# ...
```

> *[24-3rd_party_lib/pro-rule.mk](https://gitee.com/fyang0906/project_build_tuorial/blob/master/make_lesson/24-3rd_party_lib/pro-rule.mk)*

```makefile
# ...
EXTERNAL_LIB := $(wildcard $(DIR_LIBS_LIB)/*)
EXTERNAL_LIB := $(patsubst $(DIR_LIBS_LIB)/%, $(DIR_BUILD)/%, $(EXTERNAL_LIB))
# ...
# ...
link $(APP) : $(MODULE_LIB) $(EXTERNAL_LIB)
	@echo "Begin to link ..."
	$(CC) -o $(APP) -Xlinker "-(" $^ -Xlinker "-)" $(LFLAGS)
	@echo "Link Success!!!"

$(DIR_BUILD)/% : $(DIR_LIBS_LIB)/%
	$(CP) $^ $@
# ...
```

- 编译链接生成可执行文件：`make all`

```shell
☁  24-3rd_party_lib [master] ⚡  make all
mkdir build
mkdir build/common
mkdir build/module
mkdir build/main
Begin to compile ...
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/module'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/module'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/main'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/main'
Compile Success!!!
cp libs/lib/slib.a build/slib.a
cp libs/lib/dlib.so build/dlib.so
Begin to link ...
gcc -o build/app.out -Xlinker "-(" build/common.a build/module.a build/main.a build/slib.a build/dlib.so -Xlinker "-)" 
Link Success!!!
Success!!! Target ==> build/app.out
```

- 运行：`build/app.out`

```shell
☁  24-3rd_party_lib [master] ⚡  ./build/app.out
Version: 1.0.0
main()::start main ...
void common() ...
module_main()::start module ...
void foo()::Hello, Fyang ...
Dynamic Lib: dlib
2 + 3 = 5
Static Lib: slib
4 * 5 = 20
```

- 清理：`make clean`

```shell
☁  24-3rd_party_lib [master] ⚡  make clean
Begin to clean ...
rm -rf build
Clean Success!!!
```

- **单个模块分步编译：回归测试**

- 编译 `main` 模块：`make main`

```shell
☁  24-3rd_party_lib [master] ⚡  make main
mkdir build
mkdir build/main
Begin to compile main
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/main'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/main'
```

- 链接：`make link`

```shell
☁  24-3rd_party_lib [master] ⚡  make link
make: *** No rule to make target 'build/common.a', needed by 'link'.  Stop.
```

- 编译 `common` 模块：`make common`

```shell
☁  24-3rd_party_lib [master] ⚡  make common
mkdir build/common
Begin to compile common
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common'
```

- 链接：`make link`

```shell
☁  24-3rd_party_lib [master] ⚡  make link
make: *** No rule to make target 'build/module.a', needed by 'link'.  Stop.
```

- 编译 `module` 模块：`make module`

```shell
☁  24-3rd_party_lib [master] ⚡  make module
mkdir build/module
Begin to compile module
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/module'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/module'
```

- 链接：`make link`

```shell
☁  24-3rd_party_lib [master] ⚡  make link
cp libs/lib/slib.a build/slib.a
cp libs/lib/dlib.so build/dlib.so
Begin to link ...
gcc -o build/app.out -Xlinker "-(" build/common.a build/module.a build/main.a build/slib.a build/dlib.so -Xlinker "-)" 
Link Success!!!
```

- 运行：`build/app.out`

```shell
☁  24-3rd_party_lib [master] ⚡  ./build/app.out
Version: 1.0.0
main()::start main ...
void common() ...
module_main()::start module ...
void foo()::Hello, Fyang ...
Dynamic Lib: dlib
2 + 3 = 5
Static Lib: slib
4 * 5 = 20
```

**移除第三方库测试**

- 修改 `main.c` 移除第三方库调用；并移除 `libs` 目录

```C
#include <stdio.h>
#include "define.h"
// #include "slib.h"
// #include "dlib.h"

int main()
{
    printf("Version: %s\n", VERSION);
    printf("main()::start main ...\n");
    
    common();
    module_main();

    // printf("Dynamic Lib: %s\n", dlib_name());
    // printf("2 + 3 = %d\n", add(2, 3));

    // printf("Static Lib: %s\n", slib_name());
    // printf("4 * 5 = %d\n", multi(4, 5));
}
```

- 清理：`make clean`

```shell
☁  24-3rd_party_lib [master] ⚡  make clean
Begin to clean ...
rm -rf build
Clean Success!!!
```

- 编译链接生成可执行文件：`make all`

```shell
☁  24-3rd_party_lib [master] ⚡  make all
mkdir build
mkdir build/common
mkdir build/module
mkdir build/main
Begin to compile ...
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.dep ...
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.o -c src/common.c
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.o -c src/func.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/common.o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common/func.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/common.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/module'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.o -c src/module.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module/module.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/module.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/module'
make[1]: Entering directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/main'
Creating /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.dep ...
gcc -Iinc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/common/inc -I/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/libs/inc -o /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.o -c src/main.c
ar crs /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main.a /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main/main.o
Success! Target ==> /home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/build/main.a
make[1]: Leaving directory '/home/fyang/wkspace/project_build_tuorial/make_lesson/24-3rd_party_lib/main'
Compile Success!!!
Begin to link ...
gcc -o build/app.out -Xlinker "-(" build/common.a build/module.a build/main.a -Xlinker "-)" 
Link Success!!!
Success!!! Target ==> build/app.out
```

- 运行：`build/app.out`

```shell
☁  24-3rd_party_lib [master] ⚡  ./build/app.out
Version: 1.0.0
main()::start main ...
void common() ...
module_main()::start module ...
void foo()::Hello, Fyang ...
```

**小结**

- 编译环境必须支持第三方库的使用（静态库或动态库）
- 工程开发中一般会使用特殊的文件夹存放第三方库
- 第三方库所附带的头文件用于声明库函数（编译阶段需要）
- 在链接阶段先将库文件拷贝到 build 文件夹，在进行链接

