# Project-Build-Tutorial

**CMake 学习笔记**

- **[CMake-Best-Practices](https://gitee.com/fyang0906/cmake-tutorial/tree/master/cmake_best_practices)**

  - [参考资料]：[CMake-Best-Practices](https://github.com/xiaoweiChen/CMake-Best-Practices)
  - [示例代码]：[CMake-Best-Practices](https://github.com/PacktPublishing/CMake-Best-Practices)

- **[CMake_Code](https://gitee.com/fyang0906/cmake-tutorial/tree/master/cmake_code)**

**Makefile 学习笔记**

- [makefile](https://gitee.com/fyang0906/project_build_tuorial/tree/master/makefile)

