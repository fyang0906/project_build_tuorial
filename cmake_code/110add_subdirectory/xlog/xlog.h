#ifndef XLOG_H
#define XLOG_H

// __declspec(dllexport) 导出XLog类的函数到lib文件中
// xlog库文件调用 dllexport
// test_log 调用 dllimport

#ifndef _WIN32  // Linux mac unix Android
    #define XCPP_API
#else           // Windows
    #ifdef xlog_STATIC  // 静态库
        #define XCPP_API
    #else               // 动态库
        #ifdef xlog_EXPORTS
            #define XCPP_API __declspec(dllexport) // 库项目调用
        #else
            #define XCPP_API __declspec(dllimport) // 调用库项目调用
        #endif
    #endif
#endif

class XCPP_API XLog
{
public:
    XLog();
};

#endif