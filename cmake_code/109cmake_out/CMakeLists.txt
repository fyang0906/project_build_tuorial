# 109cmake_out/CMakeLists.txt

cmake_minimum_required(VERSION 3.21)
project(xlog)
include_directories("xlog")

# CMakeLists.txt 路径
message("CMAKE_CURRENT_LIST_DIR = ${CMAKE_CURRENT_LIST_DIR}")
# .so 库输出路径 默认路径在 -B build目录下

# 当前路径 109cmake_out/
# set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/../lib")
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/lib")

# 执行程序和dll 动态pdb调试文件 输出路径
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/bin")

# 归档输出路径 Windows静态库: .lib 动态库lib地址文件 静态库pdb调试文件  Linux静态库 .a
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/lib")

# 添加xlog库编译  项目自带预处理变量 xlog_EXPORTS
# add_library(xlog SHARED xlog/xlog.cpp)

set(BUILD_SHARED_LIBS OFF)  # 静态库
# cmake 传递变量给 c++
add_definitions(-Dxlog_STATIC) # 默认值 1
add_library(xlog xlog/xlog.cpp)

# 执行文件
add_executable(test_xlog test_xlog/test_xlog.cpp)

# 链接库
target_link_libraries(test_xlog xlog)