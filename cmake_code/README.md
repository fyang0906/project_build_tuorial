# 1. CMake 快速入门

> 执行程序和动态库的构建

## 1.1 CMake 基本概念

**CMake 是什么？**

- CMake 是用于构建、测试和软件打包的开源跨平台工具

**为什么用 CMake？**

- 为什么我需要一个好的构建系统
  - 你想避免硬编码路径
  - 你需要在多台计算机上构建一个包
  - 你想使用CI（持续集成）
  - 你需要支持不同的操作系统
  - 你想支持多个编译器
  - 你想使用IDE，但不是所有情况都使用IDE
  - 你想描述你的程序的逻辑结构，而不是标志和命令
  - 你想使用库
  - 您想使用其他工具来帮助你编写代码moc ProtoBuf
  - 你想使用单元测试

- 持续集成
  - 每次集成都通过自动化的制造（包括提交、发布、自动化测试）来验证，准确地发现集成错误
  - 快速处理错误，每完成一点更新，就集成到主干，可以快速发现错误，定位错误也比较容易
  - 各种不同的更新主干，如果不经常集成，会导致集成的成本变大
  - 让产品可以快速地通过，同时保持关键测试合格
  - 自动化测试，只要有一个测试用例不通过就不能集成
  - 集成并不能删除发现的错误，而是让它们很容易和改正

- CMake 特性
  - 自动搜索可能需要的程序、库和头文件的能力
  - 独立的构建目录，可以安全清理
  - 创建复杂的自定义命令：如 qt moc uic
  - 配置时选择可选组件的能力
  - 从简单的文本文件（CMakeLists.txt）自动生成工作区和项目的能力
  - 在静态和共享构建之间轻松切换的能力
  - 在大多数平台上自动生成文件依赖项并支持并行构建
- 每个IDE 都支持CMake（ CMake 支持几乎所有IDE）
- 使用CMake 的软件包比任何其他系统都多

**CMake 工作原理**

![cmake1_1.png](./image/cmake1_1.png)

## 1.2 CMake 安装

- Linux（ubuntu 22.04 LTS）

```shell
sudo apt install cmake
```
- 查看 cmake 版本信息，验证是否安装成功

![cmake1_2.png](./image/cmake1_2.png)

- Windows

  - 前置要求：安装 [Visual Studio](https://visualstudio.microsoft.com/zh-hans/downloads/)

> [https://cmake.org/download/](https://cmake.org/download/)

  - 设置环境变量：windows系统属性 --> 高级 --> 环境变量 --> 设置Path

![cmake1_3.png](./image/cmake1_3.png)

![cmake1_4.png](./image/cmake1_4.png)

## 1.3 第一个 CMake 示例

> *[101first_cmake](https://gitee.com/fyang0906/cmake-tutorial/tree/master/cmake_code/101first_cmake)*

> 前置准备

- 准备测试的c++程序文件first_cmake.cpp

```C++
#include <iostream>

using namespace std;

int main(int argc, char *argv[])
{
    cout << "first CMake Test" << endl;

    return 0;
}
```

- 在源码的同目录下编写第一个 CMakeLists.txt

```CMake
# CMakeList.txt cmake文件名大小写不敏感

# 指定cmake最低版本
cmake_minimum_required(VERSION 3.21)

# 构建项目名称
project(first_cmake)

#构建执行程序
add_executable(first_cmake first_cmake.cpp)

```

**Windows 平台编译**

> CMake ==> VS项目 ==> cl编译

**生成项目文件**

- 生成VS项目

源码目录下执行cmake命令：`cmake -S . -B win_build`

![cmake1_5.png](./image/cmake1_5.png)

- 生成nmake项目

1. 运行vs 控制台编译工具x64 Native Tools Command Prompt for VS
2022 Current

2. 进入源码目录执行命令: `cmake -S . -B nmake_build -G "NMake Makefiles"`

![cmake1_6.png](./image/cmake1_6.png)


> *Tips:* 上述cmake命令中: `-S .` 为指定源文件路径（也就是 CMakeLists.txt 文件所在的路径；`-B xxx` 为cmake编译生成的临时文件和项目文件，指定为 `build/win_build/nmake_build` (用户可自定义目录名)，将生成的文件放在独立的目录方便查看和清理。

**编译构建**

- 使用VS编译

进入 win_build 目录打开解决方案

![cmake1_7.png](./image/cmake1_7.png)

- cmake 命令编译

![cmake1_8.png](./image/cmake1_8.png)


- nmake 编译

![cmake1_9.png](./image/cmake1_9.png)

**Linux（Ubuntu）平台编译**

> 前置准备

- 安装 gcc 编译工具

```shell
sudo apt install g++
```
```shell
sudo apt install make
```

- 如果需要用到Ninja

```shell
sudo apt install ninja-build
```

**生成项目文件**

- 生成 makefile

```shell
cmake -S . -B build
```

- 生成Ninja项目

```shell
cmake -S . -B ninja_build -G "Ninja"
```

> *指定项目工具：* 在linux主要有两种，一种是生成 make 的 `makefile`；另一种是生成 Ninja 的 `build.ninja`。

## 1.4 CMake 构建静态库与动态库

- 前置准备

> *[102cmake_lib](https://gitee.com/fyang0906/cmake-tutorial/tree/master/cmake_code/102cmake_lib)*

```shell
.
├── CMakeLists.txt
├── test_xlog
│   ├── CMakeLists.txt
│   └── test_xlog.cpp
└── xlog
    ├── CMakeLists.txt
    ├── xlog.cpp
    └── xlog.h
```

**动态库和静态库概念(xlog)**

- 静态库：
  - windows: xlog.lib
  - linux: libxlog.a

- 动态库：
  - windows: xlog.lib（函数地址索引） xlog.dll（函数二进制代码）
  - linux: libxlog.so

- 头文件作用
  - 函数名称和参数类型（用于索引查找函数地址）
  - 不引用，可以自己直接声明函数
  - 知道名字可以调用系统api 查找函数

**cmake 编译静态库**

> *[./xlog/CMakeLists.txt](https://gitee.com/fyang0906/cmake-tutorial/blob/master/cmake_code/102cmake_lib/xlog/CMakeLists.txt)*

```CMake
cmake_minimum_required(VERSION 3.21)

project(xlog)

add_library(xlog STATIC xlog.cpp xlog.h)
```

- 在 `102cmake_lib/xlog` 目录下编译生成静态库

```shell
$ cmake -S . -B build
-- The C compiler identification is GNU 11.4.0
-- The CXX compiler identification is GNU 11.4.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/fyang/wkspace/cmake-tutorial/cmake_code/102cmake_lib/xlog/build
$ cmake --build build
[ 50%] Building CXX object CMakeFiles/xlog.dir/xlog.cpp.o
[100%] Linking CXX static library libxlog.a
[100%] Built target xlog
```

**cmake 链接静态库**

> *[./test_xlog/CMakeLists.txt](https://gitee.com/fyang0906/cmake-tutorial/blob/master/cmake_code/102cmake_lib/test_xlog/CMakeLists.txt)*


```CMake
# CMakeLists.txt test_xlog 102

cmake_minimum_required(VERSION 3.21)

project(test_xlog)

# 指定头文件查找路径
include_directories("../xlog")

# 指定库查找路径  windows下自动找  ../xlog/build/Debug  ../xlog/build/Release
link_directories("../xlog/build")

add_executable(test_xlog test_xlog.cpp)

# 指定加载的库
target_link_libraries(test_xlog xlog)
```

- 在 `102cmake_lib/test_xlog` 目录下编译链接静态库

```shell
$ cmake -S . -B build
-- The C compiler identification is GNU 11.4.0
-- The CXX compiler identification is GNU 11.4.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/fyang/wkspace/cmake-tutorial/cmake_code/102cmake_lib/test_xlog/build
$ cmake --build build
[ 50%] Building CXX object CMakeFiles/test_xlog.dir/test_xlog.cpp.o
[100%] Linking CXX executable test_xlog
[100%] Built target test_xlog
```

**cmake 动态库编译链接**

> *[CMakeLists.txt](https://gitee.com/fyang0906/cmake-tutorial/blob/master/cmake_code/102cmake_lib/CMakeLists.txt)*

```CMake
# CMakeLists.txt test_xlog xlog 102

cmake_minimum_required(VERSION 3.21)

project(xlog)

include_directories("xlog")

# 添加xlog库编译 项目自带预处理变量 xlog_EXPORTS
add_library(xlog SHARED xlog/xlog.cpp)

add_executable(test_xlog test_xlog/test_xlog.cpp xlog)

target_link_libraries(test_xlog xlog)
```

- 在 `102cmake_lib` 目录下编译链接动态库

```shell
$ cmake -S . -B build
-- The C compiler identification is GNU 11.4.0
-- The CXX compiler identification is GNU 11.4.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: /home/fyang/wkspace/cmake-tutorial/cmake_code/102cmake_lib/build
$ cmake --build build
[ 25%] Building CXX object CMakeFiles/xlog.dir/xlog/xlog.cpp.o
[ 50%] Linking CXX shared library libxlog.so
[ 50%] Built target xlog
[ 75%] Building CXX object CMakeFiles/test_xlog.dir/test_xlog/test_xlog.cpp.o
[100%] Linking CXX executable test_xlog
[100%] Built target test_xlog
```

> cmake 自动给库项目添加 `库名称_EXPORTS` 预处理变量

- `xlog_EXPORTS`
  - `__declspec(dllexport)`
  - `__declspec(dllimport) `

> *[xlog.h](https://gitee.com/fyang0906/cmake-tutorial/blob/master/cmake_code/102cmake_lib/xlog/xlog.h)*

```C++
#ifndef XLOG_H
#define XLOG_H

// __declspec(dllexport) 导出XLog类的函数到lib文件中
// xlog库文件调用 dllexport
// test_log 调用 dllimport

#ifndef _WIN32
    #define XCPP_API
#else
    #ifdef xlog_EXPORTS
        #define XCPP_API __declspec(dllexport) // 库项目调用
    #else
        #define XCPP_API __declspec(dllimport) // 调用库项目调用
    #endif
#endif

class XCPP_API XLog
{
public:
    XLog();
};

#endif
```

# 2. CMake 常用功能

## 2.1 CMake 注释

**括号注释**

- #[[第一行注释；第二行注释]] message("参数1\n" #[[中间的注释]] "参数2")
- VERSION 3.0 之前的 CMake 版本不支持括号注释

**行注释**

- 行注释，一直运行到行尾

```CMake
# 103message/CMakeLists.txt

#[[
message(arg1 arg2 arg3)
]]

cmake_minimum_required(VERSION 3.21)

project(message)

# 2.2 message基础使用
message("参数1")  ## 测试message
message("参数p1" "参数p2" #[[注释在message中]] "p3" 123 测试)
```

```shell
$ cmake -S . -B build
-- The C compiler identification is GNU 11.4.0
-- The CXX compiler identification is GNU 11.4.0
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Check for working C compiler: /usr/bin/cc - skipped
-- Detecting C compile features
-- Detecting C compile features - done
-- Detecting CXX compiler ABI info
-- Detecting CXX compiler ABI info - done
-- Check for working CXX compiler: /usr/bin/c++ - skipped
-- Detecting CXX compile features
-- Detecting CXX compile features - done
参数1
参数p1参数p2p3123测试
-- Configuring done
-- Generating done
-- Build files have been written to: /home/fyang/wkspace/cmake-tutorial/cmake_code/103message/build
```

## 2.2 CMake message 详解

**message 基础使用**

- `message(arg1 arg2 arg3)`

**message 高级使用-指定日志级别**

- `message([<mode>] "message text" ...)`
- `--log-level = <ERROR|WARNING|NOTICE|STATUS|VERBOSE|DEBUG|TRACE>`
- `1` 标准输出`stdout`；`2` 错误输出`stderr`
- 日志级别：
  - `FATAL_ERROR`
    - 停止cmake 运行和生成 --> printed to stderr
  - `SEND_ERROR`
    - cmake 继续运行，生成跳过 --> printed to stderr
  - `WARNING`
    - printed to stderr
  - `(none) or NOTICE`
    - printed to stderr
  - `STATUS`
    - 项目用户可能感兴趣的信息
  - `VERBOSE`
    - 针对项目用户的详细信息
  - `DEBUG`
    - 项目本身的开发人员使用的信息
  - `TRACE`
    - 非常低级实现细节的细粒度消息

> *[103message](https://gitee.com/fyang0906/project_build_tuorial/tree/master/cmake_code/103message)*

```CMake
# 103message/CMakeLists.txt

#[[
message(arg1 arg2 arg3)
]]

cmake_minimum_required(VERSION 3.21)

project(message)

# 2.2 message基础使用
message("参数1")  ## 测试message
message("参数p1" "参数p2" #[[注释在message中]] "p3" 123 测试)

# 2.3 message高级使用-指定日志级别

# FATAL_ERROR: 进程退出，生成退出  stderr
# message(FATAL_ERROR "TEST FATAL_ERROR")

# SEND_ERROR: 进程继续，生成退出 不会生成 add_executable add_library
# 打印代码路径和行号  stderr
message(SEND_ERROR "TEST SEND_ERROR")
add_executable(test_message test_message.cpp)
message("after TATAL_ERROR")

# WARNING: 打印代码路径和行号  stderr
message(WARNING "TEST WARNING")

# NOTICE: 等同于 none也就是不加 message("TEST NOTICE")  stderr
message("TEST none")
message(NOTICE "TEST NOTICE")

# STATUS: 加前缀 -- 用户可能感兴趣 stdout
message(STATUS "TEST STATUS")

# VERBOSE: 加前缀 -- 默认不显示 用户需要的详细信息 stdout
message(VERBOSE "TEST VERBOSE")

# 设置日志显示级别
# cmake -S . -B build --log-level=VERBOSE

# 标准输出重定向到文件 log.txt
# cmake -S . -B build --log-level=VERBOSE > log.txt

# 标准错误输出重定向到标准输出 
# cmake -S . -B build --log-level=VERBOSE > log.txt 2>&1

# DEBUG 加前缀 --
# cmake -S . -B build --log-level=DEBUG
message(DEBUG "TEST DEBUG")

# TRACE 加前缀 --
# cmake -S . -B build --log-level=TRACE
message(TRACE "TEST TRACE")


# 2.4 message Reporting checks查找库日志

#[[
CHEAK_START 开始记录将要执行检查的消息
CHECK_PASS  记录检查的成功结果
CHECK_FAIL  记录不成功的检查结果
]]

message("============================ 2.4 ============================")
# 开始查找
message(CHECK_START "Find xcpp")

# 查找库xcpp的代码
# message消息缩进
set(CMAKE_MESSAGE_INDENT "--")


# 嵌套查找
message(CHECK_START "Find xlog")

# 查找xlog的代码
message(CHECK_PASS "Success")

# 查找xthreadpool的代码
message(CHECK_START "Find xthreadpool")
message(CHECK_FAIL "Not found")

# 取消message消息缩进
set(CMAKE_MESSAGE_INDENT "")
#结束查找 查找失败
message(CHECK_FAIL "Not found")
```



